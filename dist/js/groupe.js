"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Carte =
/*#__PURE__*/
function () {
  function Carte() {
    _classCallCheck(this, Carte);

    this.type = '';
    this.race = '';
    this.niveau = '';
    this.rarete = '';
    this.attaque = 0;
    this.vie = 0;
  }

  _createClass(Carte, [{
    key: "randomCarte",
    value: function randomCarte() {}
  }, {
    key: "setCarte",
    value: function setCarte(arrayCaracs) {
      // { "type": 1, "niveau": 2, "race": 3, "rarete": 3, "attaque": 3, "vie": 3 }
      if (arrayCaracs.type != undefined) {
        this.type = arrayCaracs.type;
      }

      if (arrayCaracs.niveau != undefined) {
        this.niveau = arrayCaracs.niveau;
      }

      if (arrayCaracs.race != undefined) {
        this.race = arrayCaracs.race;
      }

      if (arrayCaracs.rarete != undefined) {
        this.rarete = arrayCaracs.rarete;
      }

      if (arrayCaracs.attaque != undefined) {
        this.attaque = arrayCaracs.attaque;
      }

      if (arrayCaracs.vie != undefined) {
        this.vie = arrayCaracs.vie;
      }
    }
  }]);

  return Carte;
}();