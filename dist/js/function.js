"use strict";

var hero = {
  init: function init() {
    this.nom = '';
    this.pays = '';
    this.metier = '';
    this.trait1 = '';
    this.trait2 = ''; // VITAL

    this.hp = 0;
    this.mp = 0;
    this.equipement = {
      arme: {
        nom: '',
        degat: ''
      },
      armure: {
        nom: '',
        protection: ''
      }
    };
    this.argent = 0; // CARACS

    this.carac = {};
    this.carac.physique = 0;
    this.carac.manuel = 0;
    this.carac.mental = 0;
    this.carac.social = 0; // TALENTS

    this.talent = {};
    this.talent.combat = 0;
    this.talent.athletisme = 0;
    this.talent.force = 0;
    this.talent.resistance = 0;
    this.talent.distance = 0;
    this.talent.precision = 0;
    this.talent.larcin = 0;
    this.talent.acrobatie = 0;
    this.talent.perception = 0;
    this.talent.magie = 0;
    this.talent.volonte = 0;
    this.talent.deduction = 0;
    this.talent.enquete = 0;
    this.talent.baratin = 0;
    this.talent.persuasion = 0;
    this.talent.representation = 0; // SPES

    this.spe = {};
    this.spe.boxe = false;
    this.spe.epee = false;
    this.spe.dague = false;
    this.spe.saut = false;
    this.spe.course = false;
    this.spe.equitation = false;
    this.spe.intimidation = false;
    this.spe.soulever = false; // tirer / pousser...

    this.spe.natation = false;
    this.spe.poison = false; // alcool, fatigue...

    this.spe.arc = false;
    this.spe.lancer = false;
    this.spe.soin = false;
    this.spe.art = false;
    this.spe.artisanat = false;
    this.spe.jeux = false;
    this.spe.investigation = false;
    this.spe.survie = false;
    this.spe.alchimie = false;
    this.spe.magieElem = false;
    this.spe.meditation = false;
    this.spe.empathie = false;
    this.spe.seduction = false;
    this.spe.marchandage = false;
    this.spe.diplomatie = false;
    this.spe.eloquence = false;
    this.spe.etiquette = false;
    this.spe.comedie = false;
    this.spe.danse = false;
    this.spe.musique = false; //CONNAISSANCES

    this.con = {};
    this.con.strategie = false;
    this.con.guildeCombattant = false;
    this.con.argotGuerrier = false;
    this.con.argotChasseur = false;
    this.con.argotMage = false;
    this.con.argotMarchand = false;
    this.con.armeLegendaire = false;
    this.con.animauxMer = false;
    this.con.animauxMontagne = false;
    this.con.animauxLegendaire = false;
    this.con.monstreMortVivant = false;
    this.con.poison = false;
    this.con.drogue = false;
    this.con.gastronomie = false;
    this.con.herboristerie = false;
    this.con.lectureEcriture = false;
    this.con.lectureAncien = false;
    this.con.glyphe = false;
    this.con.histoire = false;
    this.con.heraldique = false;
  },
  setMetier: function setMetier(metier) {
    this.carac.physique = metier.physique;
    this.carac.manuel = metier.manuel;
    this.carac.mental = metier.mental;
    this.carac.social = metier.social;
    this.talent.combat = metier.combat;
    this.talent.athletisme = metier.athletisme;
    this.talent.force = metier.force;
    this.talent.resistance = metier.resistance;
    this.talent.distance = metier.distance;
    this.talent.precision = metier.precision;
    this.talent.larcin = metier.larcin;
    this.talent.acrobatie = metier.acrobatie;
    this.talent.perception = metier.perception;
    this.talent.magie = metier.magie;
    this.talent.volonte = metier.volonte;
    this.talent.deduction = metier.deduction;
    this.talent.enquete = metier.enquete;
    this.talent.baratin = metier.baratin;
    this.talent.persuasion = metier.persuasion;
    this.talent.representation = metier.representation;
  }
};