"use strict";

function getadmin() {
  var player = 'joueur1'; // COMMENT

  if (joueur_saved != null) {
    selectedplayer = joueur_saved[player];
    selectperso = persos[player];
    selectlieux = lieux[player];
    selectcreature = creatures[player];
    selectpersonnages = personnages[player];
    var nb_obj = Object.keys(joueur_saved).length;

    if (nb_obj > 0) {
      for (var keys in joueur_saved) {
        $('.selectplayer').append('<option value="' + keys + '">' + joueur_saved[keys].nom + '</option>');
      }
    } else {
      $('.getplayer').html('Aucun joueur enregistré');
    }

    if ($('.selectplayer').length > 0) {
      updateFields();
    }

    $('.save').off().on('click', function () {
      selectedplayer.avantage = $('.avantage').val();
      selectedplayer.defaut = $('.defaut').val();
      selectedplayer.numero = $('.numero').val();
      selectedplayer.sexe = $('.sexe').val();
      selectedplayer.place = $('.place').val();
      selectedplayer.magie = $('.magie').val();
      selectedplayer.energie = $('.energie').val();
      saveGame();
      $('.msg').html('saved');
    });
    $('.selectplayer').on('change', function () {
      player = $(this).val();
      selectedplayer = joueur_saved[player];
      selectperso = persos[player];
      selectlieux = lieux[player];
      selectcreature = creatures[player];
      selectpersonnages = personnages[player];
      updateFields();
    });
  }

  function updateFields() {
    $('.selectplayer').val(player);
    $('.avantage').val(selectedplayer.avantage);
    $('.defaut').val(selectedplayer.defaut);
    $('.numero').val(selectedplayer.numero);
    $('.sexe').val(selectedplayer.sexe);
    $('.place').val(selectedplayer.place);
    $('.magie').val(selectedplayer.magie);
    $('.energie').val(selectedplayer.energie);
  }

  var my_url = document.URL;

  if (my_url == 'http://localhost:3000/') {
    var array_name = ['Clem', 'ffghfgh', 'Helene', 'Mathilde', 'Naomi', 'Océane', 'test'];

    for (var i = 0; i < array_name.length; i++) {
      $('.exportlst').append("<option value='" + array_name[i] + "' >" + array_name[i] + "</option>");
    }
  } else {
    $.post("export/export.php", {
      func: "liste"
    }, function (data) {
      $('.exportlst').html(data);

      if (data == '') {
        $('.exportlst, .import').remove();
      }
    });
  }

  $('.import').off().on('click', function () {
    var qui = $('.exportlst').val();
    var mycreaturesI, mypersosI, myjoueurI, mylieuxI, mypersonnagesI, mysortsI;
    var passe = true;

    if (joueur_saved != undefined) {
      if (joueur_saved.joueur1 != undefined && qui == joueur_saved.joueur1.nom) {
        passe = false;
      }

      if (joueur_saved.joueur2 != undefined && qui == joueur_saved.joueur2.nom) {
        passe = false;
      }

      if (joueur_saved.joueur3 != undefined && qui == joueur_saved.joueur3.nom) {
        passe = false;
      }
    }

    if (passe) {
      $.getJSON("export/Export_crea_" + qui + ".json", function (data) {
        mycreaturesI = data;
        $.getJSON("export/Export_perso_" + qui + ".json", function (data) {
          mypersosI = data;
          $.getJSON("export/Export_player_" + qui + ".json", function (data) {
            myjoueurI = data;
            $.getJSON("export/Export_lieux_" + qui + ".json", function (data) {
              mylieuxI = data;
              $.getJSON("export/Export_personnages_" + qui + ".json", function (data) {
                mypersonnagesI = data;
                $.getJSON("export/Export_sort_" + qui + ".json", function (data) {
                  mysortsI = data;

                  if (joueur_saved.joueur1 == null) {
                    joueur_saved.joueur1 = jQuery.parseJSON(myjoueurI);
                    persos.joueur1 = jQuery.parseJSON(mypersosI);
                    lieux.joueur1 = jQuery.parseJSON(mylieuxI);
                    creatures.joueur1 = jQuery.parseJSON(mycreaturesI);
                    personnages.joueur1 = jQuery.parseJSON(mypersonnagesI);
                    sorts.joueur1 = jQuery.parseJSON(mysortsI);
                    $('.import').text('Joueur 1 ajouté.');
                  } else if (joueur_saved.joueur2 == null) {
                    joueur_saved.joueur2 = jQuery.parseJSON(myjoueurI);
                    persos.joueur2 = jQuery.parseJSON(mypersosI);
                    lieux.joueur2 = jQuery.parseJSON(mylieuxI);
                    creatures.joueur2 = jQuery.parseJSON(mycreaturesI);
                    personnages.joueur2 = jQuery.parseJSON(mypersonnagesI);
                    sorts.joueur2 = jQuery.parseJSON(mysortsI);
                    $('.import').text('Joueur 2 ajouté.');
                  } else if (joueur_saved.joueur3 == null) {
                    joueur_saved.joueur3 = jQuery.parseJSON(myjoueurI);
                    persos.joueur3 = jQuery.parseJSON(mypersosI);
                    lieux.joueur3 = jQuery.parseJSON(mylieuxI);
                    creatures.joueur3 = jQuery.parseJSON(mycreaturesI);
                    personnages.joueur3 = jQuery.parseJSON(mypersonnagesI);
                    sorts.joueur3 = jQuery.parseJSON(mysortsI);
                    $('.import').text('Joueur 3 ajouté.');
                  } else {
                    $('.import').text('Vous avez déjà trois profils, merci d\'en supprimer un.');
                  }

                  localStorage.setItem("joueurs", JSON.stringify(joueur_saved));
                  localStorage.setItem("persos", JSON.stringify(persos));
                  localStorage.setItem("lieux", JSON.stringify(lieux));
                  localStorage.setItem("creatures", JSON.stringify(creatures));
                  localStorage.setItem("personnages", JSON.stringify(personnages));
                  localStorage.setItem("sorts", JSON.stringify(sorts));
                });
              });
            });
          });
        });
      });
    } else {
      $('.import').text('Vous avez déjà une partie en cours pour le personnage : ' + qui);
    }
  });
}
"use strict";

var groupe_chap2 = [];
var aveck = false;
var rem = false;
var nao = false;
var mparti = "";
var forteresseguy = "";
var forteresseguyS = "F";
var miguelname = false;
var numgroupe = 0;
var levydis = false;
var impcapture = false;
var impmort = false;
var grpsansfg = false;
var decouvert = false; // MAC LUMY + LASSENI + REM + TOI + KONAKEY 26 --> 1
// MAC LUMY + LASSENI + NAO + TOI + KONAKEY 26 --> 2
// MAC LUMY + LASSENI + NAO + TOI  27 --> 3
// MAC LUMY + LASSENI + REM + TOI  27 --> 4
// MAC LUMY + LASSENI + NAO + TOI + KANNAN 29 --> 5
// MAC LUMY + LASSENI + REM + TOI + KANNAN 29 --> 6
// MAC LUMY + LASSENI + REM + TOI YMIR + KANNAN 32 ou 30 --> 7
// MAC LUMY + LASSENI + REM + TOI YMIR 32 ou 30 --> 8
// TOI YMIR 33 --> 9 // n'existe plus au 50 - REPOS
// TOI YMIR + KANNAN 33 --> 10 // n'existe plus au 50 - REPOS

function goActionChapter2() {
  getInfoChap2();
  getGroupeMission();
  getMagesGroupe();
  mparti = selectpersonnages.groupe.mission[1];

  if (decouvert) {
    $('.decouvert').show();
    $('.pas-decouvert').remove();
  } else {
    $('.decouvert').remove();
    $('.pas-decouvert').show();
  }

  if (rem) {
    $('.rem').show();
    $('.pas-rem').remove();
  } else {
    $('.rem').remove();
    $('.pas-rem').show();
  }

  if (grpsansfg) {
    $('.grpsansfg').show(); // vous connaissez Konakey mais elle est pas dans le groupe

    $('.grpsansfg-2').remove();
  } else {
    $('.grpsansfg').remove();
    $('.grpsansfg-2').show();
  }

  if (levydis) {
    $('.iflevy').remove();
    $('.if-pas-levy').show();
  } else {
    $('.iflevy').show();
    $('.if-pas-levy').remove();
  }

  if (impcapture) {
    $('.impcapture').show();
  } else {
    $('.impcapture').remove();
  }

  if (nao) {
    $('.nao').show();
    $('.pas-nao').remove();
  } else {
    $('.nao').remove();
    $('.pas-nao').show();
  }

  if (numgroupe != 0) {
    $('.numgroupe').not('.numgroupe-' + numgroupe).remove();
  }

  if (aveck) {
    $('.aveck').show();
    $('.pas-aveck').remove();
    $('.aveck-nom').html(mparti);
  } else {
    $('.aveck').remove();
    $('.pas-aveck').show();
  }

  if (forteresseguy == 'Panacle') {
    forteresseguyS = 'G';
  }

  console.log(forteresseguy);

  if (forteresseguy == "Konakey") {
    console.log(forteresseguy);
    $('.pas-konakey').remove();
    $('.fort-nom').html(forteresseguy);
  } else {
    $('.konakey').remove();
    $('.fort-nom').html(forteresseguy);
  }

  if (forteresseguyS == 'G') {
    $('.sfg-g').show();
    $('.sfg-f').remove();
  } else {
    $('.sfg-f').show();
    $('.sfg-g').remove();
  }

  if (selectedplayer.fin1 > 0) {
    $('.fin').not('.fin-' + selectedplayer.fin1).remove();
  }

  if (forteresseguy == selectpersonnages.groupe.mission[1]) {
    $('.aveck2').show();
    $('.pas-aveck2').remove();
  } else {
    $('.aveck2').remove();
    $('.pas-aveck2').show();
  }

  $('.finchapitre').off().on('click', function () {
    // finchapitre3
    $('.messagefin').fadeIn(500);
  });

  if (selectedplayer.numero == 1) {
    var x = document.getElementById("myAudio");
    x.play();
    setTimeout(function () {
      $('.newchapter, .newchapter2').fadeOut(2000);
      $('.choix2, .resume').fadeIn(2000);
    }, 10000);

    if (selectedplayer.fin1 > 0) {
      $('.fin').not('.fin-' + selectedplayer.fin1).remove();
    }

    $('.maplace').html(selectedplayer.place);

    if (selectedplayer.grp1) {
      $('.sipasgrp1-1').remove();

      if (selectedplayer.fin1 == 1 || selectedplayer.fin1 == 2 || selectedplayer.fin1 == 3) {
        forteresseguy = selectpersonnages.groupe.mission[1];
      } else {
        forteresseguy = "Konakey";
      }
    } else {
      $('.sigrp1-1').remove();
      forteresseguy = "Konakey";
    }

    saveGame();
  }

  if (selectedplayer.numero == 2) {
    if (selectedplayer.grp1) {
      $('.sipasgrp1-1').remove();
    } else {
      $('.sigrp1-1').remove();
    }

    groupe_chap2.push('Lasseni');
    groupe_chap2.push('Mac Lumy');
    selectpersonnages.groupe.chap2 = groupe_chap2;
    saveGame();
  }

  if (selectedplayer.numero == 3) {
    // Rajout de celui qui s'est échappé avec toi
    rem = true;
    nao = false;

    if (selectedplayer.grp1) {
      $('.sipasgrp1-1').remove();
      aveck = true;
      miguelname = true;
      groupe_chap2.push(selectpersonnages.groupe.mission[1]);
    } else {
      $('.sigrp1-1').remove();
    }

    selectpersonnages.groupe.chap2 = groupe_chap2;
    saveGame();
  }

  if (selectedplayer.numero == 6) {
    insertNew(selectperso, 'Heldegarde');
    insertNew(selectperso, 'Fandrean');
    saveGame();
  }

  if (selectedplayer.numero == 9) {
    if (selectedplayer.grp1) {
      $('.sipasgrp1-1').remove();
    } else {
      $('.sigrp1-1').remove();
    }

    groupe_chap2.push('Lasseni');
    groupe_chap2.push('Mac Lumy');
    selectpersonnages.groupe.chap2 = groupe_chap2;
    saveGame();
  }

  if (selectedplayer.numero == 10) {
    insertNew(selectperso, 'Mullihane');
    insertNew(selectperso, 'Pasha');
    insertNew(selectperso, 'Andaelor');
    insertNew(selectperso, 'Fandrean');
    insertNew(selectperso, 'Rem'); //insertNew(selectlieux, 'Darmen');

    saveGame();
  }

  if (selectedplayer.numero == 10) {
    insertNew(selectperso, 'Heldegarde');
    saveGame();
  }

  if (selectedplayer.numero == 15) {
    groupe_chap2.push('Rem');
    rem = true;
    nao = false;
    selectpersonnages.groupe.chap2 = groupe_chap2;
    saveGame();
  }

  if (selectedplayer.numero == 17) {
    insertNew(selectperso, 'Nao');
    groupe_chap2.push('Nao');
    nao = true;
    rem = false;
    selectpersonnages.groupe.chap2 = groupe_chap2;
    saveGame();
  }

  if (selectedplayer.numero == 18) {
    insertNew(selectperso, 'Usaki');
    saveGame();
  }

  if (selectedplayer.numero == 20) {
    selectperso.Usaki.d2 = true;
    saveGame();
  }

  if (selectedplayer.numero == 23) {
    if (selectedplayer.fin1 != 6) {
      $('.fin.fin-6').remove();
    } else {
      $('.fin.fin-autre').remove();
    }
  }

  if (selectedplayer.numero == 24) {
    if (forteresseguy == "Konakey") {
      insertNew(selectperso, 'Konakey');
    }
  }

  if (selectedplayer.numero == 26) {
    groupe_chap2.push('Konakey');
    selectpersonnages.groupe.chap2 = groupe_chap2;

    if (nao) {
      numgroupe = 2;
    } else {
      numgroupe = 1;
    }

    saveGame();
  }

  if (selectedplayer.numero == 27) {
    grpsansfg = true;

    if (nao) {
      numgroupe = 3;
    } else {
      numgroupe = 4;
    }
  }

  if (selectedplayer.numero == 29) {
    insertNew(selectperso, 'Usaki');

    if (nao) {
      numgroupe = 5;
    } else {
      numgroupe = 6;
    }

    miguelname = true;
    groupe_chap2.push(forteresseguy);
    selectpersonnages.groupe.chap2 = groupe_chap2;
    saveGame();
  }

  if (selectedplayer.numero == 30 || selectedplayer.numero == 32) {
    if (aveck) {
      numgroupe = 7;
    } else {
      numgroupe = 8;
    }

    groupe_chap2.push('Mac Lumy');
    groupe_chap2.push('Lasseni');
    groupe_chap2.push('Rem');
    selectpersonnages.groupe.chap2 = groupe_chap2;
    saveGame();
  }

  if (selectedplayer.numero == 33) {
    if (aveck) {
      numgroupe = 10;
    } else {
      numgroupe = 9;
    }
  }

  if (selectedplayer.numero == 41 || selectedplayer.numero == 42 || selectedplayer.numero == 43 || selectedplayer.numero == 47 || selectedplayer.numero == 48) {
    if (selectedplayer.fin1 != 6) {
      $('.fin.fin-6').remove();
    } else {
      $('.fin.fin-autre').remove();
    }
  }

  if (selectedplayer.numero == 45) {
    if (numgroupe == 10) {
      numgroupe = 7;
    } else if (numgroupe == 9) {
      numgroupe = 8;
    }
  }

  if (selectedplayer.numero == 48) {
    if (nao) {
      numgroupe = 3;
    } else {
      numgroupe = 4;
    }
  }

  if (selectedplayer.numero == 50) {
    if (numgroupe == 1 || numgroupe == 2) {
      selectperso.Konakey.d2 = true;
    }

    if (numgroupe == 1 || numgroupe == 2 || numgroupe == 5 || numgroupe == 6) {
      miguelname = true;
    }

    if (selectedplayer.fin1 != 6) {
      $('.fin.fin-6').remove();
    } else {
      $('.fin.fin-autre').remove();
    }
  }

  if (selectedplayer.numero == 51) {
    insertNew(selectperso, 'Imp');
    selectedplayer.energie = "noire2";
    saveGame();
  }

  if (selectedplayer.numero == 55) {
    insertNew(selectsorts, 'blizzard');
    saveGame();
  }

  if (selectedplayer.numero == 60) {
    insertNew(selectsorts, 'pilier de pierre');
    saveGame();
  }

  if (selectedplayer.numero == 61) {
    insertNew(selectsorts, 'choc psy');
    saveGame();
  }

  if (selectedplayer.numero == 67) {
    insertNew(selectperso, 'Imp');
    saveGame();
  }

  if (selectedplayer.numero == 69) {
    decouvert = true;
    saveGame();
  }

  if (selectedplayer.numero == 73 || selectedplayer.numero == 72 || selectedplayer.numero == 71 || selectedplayer.numero == 68) {
    levydis = true;
    saveGame();
  }

  if (selectedplayer.numero == 80) {
    insertNew(selectcreature, 'porcussin');
    saveGame();
  }

  if (selectedplayer.numero == 83) {
    impcapture = true;
    selectperso.Imp.d2 = true;
    saveGame();
  }

  if (selectedplayer.numero == 85) {
    if (forteresseguy == "Konakey") {
      $('.kannan').remove();
    } else {
      $('.pas-kannan').remove();
    }
  }

  if (selectedplayer.numero == 88) {
    selectperso.Imp.d3 = true;
    impmort = true;
    saveGame();
  }

  if (selectedplayer.numero == 89) {
    $('.kannan-nom').html(mparti);

    if (selectedplayer.grp1) {
      $('.kannan').show();
    } else {
      $('.kannan').remove();
    }
  }

  if (selectedplayer.numero == 90) {
    forteresseguy = "Konakey";
    saveGame();
  }

  if (selectedplayer.numero == 103) {
    selectperso.Usaki.d2 = true;
    saveGame();
  }

  if (selectedplayer.numero == 152) {
    var x1 = document.getElementById("myAudio");
    x1.play();
    selectedplayer.fin2 = 1;
    saveGame();
    $('.finchapitre').off().on('click', function () {
      gotoChap3();
    });
  }

  if (selectedplayer.numero == 153) {
    var x2 = document.getElementById("myAudio");
    x2.play();
    selectedplayer.fin2 = 2;
    saveGame();
    $('.finchapitre').off().on('click', function () {
      gotoChap3();
    });
  }

  if (selectedplayer.numero == 154) {
    var x3 = document.getElementById("myAudio");
    x3.play();
    selectedplayer.fin2 = 3;
    saveGame();
    $('.finchapitre').off().on('click', function () {
      gotoChap3();
    });
  }

  if (selectedplayer.numero == 155) {
    var x4 = document.getElementById("myAudio");
    x4.play();
    selectedplayer.fin2 = 4;
    saveGame();
    $('.finchapitre').off().on('click', function () {
      gotoChap3();
    });
  }

  saveInfoChap2();
}

function gotoChap3() {
  // finchapitre3
  selectedplayer.chapitrenum = 'chapitre3';
  selectedplayer.chapitre = 'Chap.3 - La bataille de la Lune';
  selectedplayer.numero = 1;
  saveGame();
  $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
    binding();
    $("body, .num, main, html").scrollTop(0);
    $(window).scrollTop(0);
    $(document).scrollTop(0);
  });
}

function getInfoChap2() {
  aveck = selectedplayer.aveck;
  rem = selectedplayer.rem;
  nao = selectedplayer.nao;
  mparti = selectedplayer.mparti;
  forteresseguy = selectedplayer.forteresseguy;
  forteresseguyS = selectedplayer.forteresseguyS;
  miguelname = selectedplayer.miguelname;
  numgroupe = selectedplayer.numgroupe;
  levydis = selectedplayer.levydis;
  impcapture = selectedplayer.impcapture;
  grpsansfg = selectedplayer.grpsansfg;
  impmort = selectedplayer.impmort;
  decouvert = selectedplayer.decouvert;
}

function saveInfoChap2() {
  selectedplayer.impmort = impmort;
  selectedplayer.grpsansfg = grpsansfg;
  selectedplayer.impcapture = impcapture;
  selectedplayer.levydis = levydis;
  selectedplayer.numgroupe = numgroupe;
  selectedplayer.aveck = aveck;
  selectedplayer.rem = rem;
  selectedplayer.nao = nao;
  selectedplayer.mparti = mparti;
  selectedplayer.forteresseguy = forteresseguy;
  selectedplayer.forteresseguyS = forteresseguyS;
  selectedplayer.miguelname = miguelname;
  selectedplayer.decouvert = decouvert;
  saveGame();
}
"use strict";

var groupe_chap3 = [];
var vote = [];
var chefazano = 'humain';
var tourdetruite = 'non';
var persosmort = [];
var epilogue = [];
var quiquidonc;

function goActionChapter3() {
  getInfoChap2();

  if (selectedplayer.numero > 1) {
    getInfoChap3();
  }

  getGroupeMission();
  getMagesGroupe();

  if (selectedplayer.numero == 1) {
    var x = document.getElementById("myAudio");
    x.play();
    setTimeout(function () {
      $('.newchapter, .newchapter3').fadeOut(2000);
      $('.choix2, .resume').fadeIn(2000);
    }, 10000);
  }

  if (selectedplayer.fin1 > 0) {
    $('.fin1').not('.fin1-' + selectedplayer.fin1).remove();
  }

  if (selectedplayer.fin2 > 0) {
    $('.fin').not('.fin-' + selectedplayer.fin2).remove();
  }

  if (selectedplayer.numero == 249) {
    selectedplayer.fin3 = 1;
    saveGame();
  }

  if (selectedplayer.numero == 258) {
    selectedplayer.fin3 = 2;
    saveGame();
  }

  if (selectedplayer.numero == 288) {
    selectedplayer.fin3 = 3;
    saveGame();
  }

  if (selectedplayer.numero == 312) {
    selectedplayer.fin3 = 4;
    saveGame();
  }

  if (selectedplayer.numero == 315) {
    selectedplayer.fin3 = 5;
    saveGame();
  }

  if (selectedplayer.numero == 335) {
    selectedplayer.fin3 = 6;
    saveGame();
  }

  if (selectedplayer.numero == 338) {
    selectedplayer.fin3 = 7;
    saveGame();
  }

  if (selectedplayer.numero == 345) {
    selectedplayer.fin3 = 8;
    saveGame();
  }

  if (selectedplayer.numero == 360) {
    selectedplayer.fin3 = 9;
    saveGame();
  }

  if (selectedplayer.numero == 362) {
    selectedplayer.fin3 = 10;
    saveGame();
  }

  if (selectedplayer.numero == 364) {
    selectedplayer.fin3 = 11;
    saveGame();
  }

  if (selectedplayer.numero == 366) {
    selectedplayer.fin3 = 12;
    saveGame();
  }

  if (selectedplayer.numero == 371) {
    selectedplayer.fin3 = 13;
    saveGame();
  }

  if (selectedplayer.numero == 376) {
    selectedplayer.fin3 = 14;
    saveGame();
  }

  if (selectedplayer.numero == 378) {
    selectedplayer.fin3 = 15;
    saveGame();
  }

  if (selectedplayer.numero == 388) {
    selectedplayer.fin3 = 16;
    saveGame();
  }

  if (selectedplayer.numero == 390) {
    selectedplayer.fin3 = 17;
    saveGame();
  }

  if (selectedplayer.numero == 392) {
    selectedplayer.fin3 = 18;
    saveGame();
  }

  if (selectedplayer.numero == 395) {
    selectedplayer.fin3 = 19;
    saveGame();
  }

  if (selectedplayer.numero == 397) {
    selectedplayer.fin3 = 20;
    saveGame();
  }

  if (selectedplayer.numero == 400) {
    selectedplayer.fin3 = 21;
    saveGame();
  }

  if (selectedplayer.numero == 402) {
    selectedplayer.fin3 = 22;
    saveGame();
  }

  if (selectedplayer.numero == 409) {
    selectedplayer.fin3 = 23;
    saveGame();
  }

  if (selectedplayer.numero == 421) {
    selectedplayer.fin3 = 24;
    saveGame();
  }

  if (selectedplayer.numero == 430) {
    selectedplayer.fin3 = 25;
    saveGame();
  }

  if (selectedplayer.numero == 456) {
    selectedplayer.fin3 = 26;
    saveGame();
  }

  if (selectedplayer.numero == 469) {
    selectedplayer.fin3 = 27;
    saveGame();
  }

  if (selectedplayer.numero == 478) {
    selectedplayer.fin3 = 28;
    saveGame();
  }

  if (selectedplayer.numero == 15 || selectedplayer.numero == 59 || selectedplayer.numero == 65 || selectedplayer.numero == 92 || selectedplayer.numero == 109 || selectedplayer.numero == 110 || selectedplayer.numero == 120 || selectedplayer.numero == 135 || selectedplayer.numero == 143 || selectedplayer.numero == 152 || selectedplayer.numero == 177 || selectedplayer.numero == 190 || selectedplayer.numero == 199 || selectedplayer.numero == 200 || selectedplayer.numero == 202) {
    $('.redochap3').on('click', function () {
      resetgame3();
    });
  }

  var listemort = persosmort.join('-');

  if (listemort.search('Méonie') > -1) {
    $('.méonie-morte').show();
    $('.méonie-vivante').hide();
  } else {
    $('.méonie-morte').hide();
    $('.méonie-vivante').show();
  }

  if (listemort.search('Mullihane') > -1) {
    $('.mullihane-morte').show();
    $('.mullihane-vivante').hide();
  } else {
    $('.mullihane-morte').hide();
    $('.mullihane-vivante').show();
  }

  if (listemort.search('Kannan') > -1) {
    $('.kannan-morte').show();
    $('.kannan-vivante').hide();
  } else {
    $('.kannan-morte').hide();
    $('.kannan-vivante').show();
  }

  if (listemort.search('Espéride') > -1) {
    $('.espéride-morte').show();
    $('.espéride-vivante').hide();
  } else {
    $('.espéride-morte').hide();
    $('.espéride-vivante').show();
  }

  if (listemort.search('Haziko') > -1) {
    $('.haziko-morte').show();
    $('.haziko-vivante').hide();
  } else {
    $('.haziko-morte').hide();
    $('.haziko-vivante').show();
  }

  if (listemort.search('Mac Lumy') > -1) {
    $('.mac-mort').show();
    $('.mac-vivant').hide();
  } else {
    $('.mac-mort').hide();
    $('.mac-vivant').show();
  }

  if (listemort.search('Haziko') > -1) {
    $('.haziko-morte').show();
    $('.haziko-vivante').hide();
  } else {
    $('.haziko-morte').hide();
    $('.haziko-vivante').show();
  }

  if (listemort.search('Panacle') > -1) {
    $('.panacle-mort').show();
    $('.panacle-vivant').hide();
  } else {
    $('.panacle-mort').hide();
    $('.panacle-vivant').show();
  }

  if (listemort.search('Anselm') > -1) {
    $('.anselm-mort').show();
    $('.anselm-vivant').hide();
  } else {
    $('.anselm-mort').hide();
    $('.anselm-vivant').show();
  }

  if (listemort.search('Lasseni') > -1) {
    $('.lasseni-morte').show();
    $('.lasseni-vivante').hide();
  } else {
    $('.lasseni-morte').hide();
    $('.lasseni-vivante').show();
  }

  if (selectedplayer.grp1) {
    quiquidonc = selectpersonnages.groupe.mission[1];

    if (quiquidonc != "" && quiquidonc != undefined) {} else {
      quiquidonc = "Kannan";
    }

    $('.partiavecusaki').html('quiquidonc');
  }

  if (rem) {
    $('.naorem').html('Rem');
  } else {
    $('.naorem').html('Nao');
  }

  if (selectedplayer.numero == 1) {
    insertNew(selectperso, 'Méonie');
    insertNew(selectperso, 'Arak');
    saveGame();
  }

  if (selectedplayer.numero == 24) {
    tourdetruite = 'oui';
  }

  if (selectedplayer.numero == 52) {
    // seulement Académie et Lac peuvent voter
    $('.vote1-1').on('click', function () {
      vote[0] = '1';
    }); // tout le monde peut voter

    $('.vote1-2').on('click', function () {
      vote[0] = '2';
    });
  }

  if (selectedplayer.numero == 53) {
    // no limites
    $('.vote2-1').on('click', function () {
      vote[1] = '1';
    }); // pas de mages renégats

    $('.vote2-2').on('click', function () {
      vote[1] = '2';
    }); // pas de juniors

    $('.vote3-2').on('click', function () {
      vote[1] = '3';
    });
  }

  if (selectedplayer.numero == 54) {
    // 4 mages
    $('.vote3-1').on('click', function () {
      vote[2] = '1';
    }); // 6mages

    $('.vote3-2').on('click', function () {
      vote[2] = '2';
    });
  }

  if (selectedplayer.numero == 55) {
    if (parseInt(vote[2]) == 1) {
      $('.vote3-2').remove();
    } else {
      $('.vote3-1').remove();
    }
  }

  if (selectedplayer.numero == 72) {
    chefazano = 'mixte';
    epilogue.push('Village commun');
  }

  if (selectedplayer.numero == 94) {
    persosmort.push('Haziko');
    persosmort.push('Panacle');
    persosmort.push('Anselm');
    persosmort.push('MacLumy');
    persosmort.push('Kannan');
    persosmort.push('Espéride');
    epilogue.push('La catastrophe');
  }

  if (selectedplayer.numero == 96) {
    epilogue.push('Guerre des villages');
  }

  if (selectedplayer.numero == 97) {
    epilogue.push('Village blessé');
  }

  if (selectedplayer.numero == 98) {
    epilogue.push('Village dans la forteresse');
  }

  if (selectedplayer.numero == 108) {
    persosmort.push('Espéride2');
    epilogue.push('Espéride morte');
  }

  if (selectedplayer.numero == 113) {
    if (quiquidonc.toLowerCase() == "haziko") {
      $('.haziko').hide();
    }

    if (quiquidonc.toLowerCase() == "panacle") {
      $('.panacle').hide();
    }

    if (quiquidonc.toLowerCase() == "anselm") {
      $('.anselm').hide();
    }
  }

  if (selectedplayer.numero == 128) {
    epilogue.push('Déception légendaire');
  }

  if (selectedplayer.numero == 131) {
    epilogue.push('Déception légendaire');
  }

  if (selectedplayer.numero == 144) {
    persosmort.push('Mullihane');
    persosmort.push('Kannan2');
  }

  if (selectedplayer.numero == 145) {
    persosmort.push('Méonie');
  }

  if (selectedplayer.numero == 166) {
    persosmort.push('MacLumy2');
    persosmort.push('Kannan3');
    persosmort.push('Panacle2');
    persosmort.push('Mullihane2');
  }

  if (selectedplayer.numero == 168) {
    epilogue.push('Mort triste de Méonie');
    persosmort.push('Méonie2');
  }

  if (selectedplayer.numero == 176) {
    persosmort.push('Mullihane3');
    persosmort.push('MacLumy3');
    persosmort.push('Kannan4');
    persosmort.push('Panacle3');
    persosmort.push('Haziko2');
    persosmort.push('Anselm2');
    persosmort.push('Espéride3');
  }

  if (selectedplayer.numero == 183) {
    epilogue.push('Usaki bras en moins');
  }

  if (selectedplayer.numero == 198) {
    epilogue.push('Ymir bras en moins');
  }

  if (selectedplayer.numero == 203) {
    persosmort.push('MacLumy4');
  }

  if (selectedplayer.numero == 205) {
    epilogue.push('Lasseni et Fandrean résistance');
  }

  if (selectedplayer.numero == 208) {
    persosmort.push('Mullihane4');
    persosmort.push('Méonie3');
    epilogue.push('Kannan aveugle');
  }

  if (selectedplayer.numero == 209) {
    epilogue.push('Méonie jambes coupées');
    epilogue.push('Kannan aveugle');
  }

  if (selectedplayer.numero == 210) {
    epilogue.push('Méonie jambes coupées');
    epilogue.push('Kannan aveugle');
  }

  if (selectedplayer.numero == 219) {
    epilogue.push('Usaki bras en moins');
  }

  if (selectedplayer.numero == 232) {
    epilogue.push('Méonie sauvée par ton pouvoir');
  }

  if (selectedplayer.numero == 233) {
    epilogue.push('Méonie morte');
  }

  if (selectedplayer.numero == 236) {
    persosmort.push('Soyos2');
    persosmort.push('Andaelor');
    persosmort.push('Pasha');
  }

  if (selectedplayer.numero == 237) {
    persosmort.push('Soyos2');
    persosmort.push('Andaelor');
    persosmort.push('Pasha');
  }

  if (selectedplayer.numero == 239) {
    epilogue.push('Tour détruite');
  }

  if (selectedplayer.numero == 242) {
    persosmort.push('MacLumy5');
  }

  if (selectedplayer.numero == 244) {
    persosmort.push('Panacle4');
  }

  if (selectedplayer.numero == 246) {
    persosmort.push('Espéride4');
    persosmort.push('Panacle4');
    persosmort.push('Anselm3');
    persosmort.push('Kannan5');
  }

  if (selectedplayer.numero == 250) {
    persosmort.push('Espéride5');
    persosmort.push('Panacle5');
    persosmort.push('Anselm4');
    persosmort.push('Kannan6');
  }

  if (selectedplayer.numero == 251) {
    persosmort.push('Espéride6');
  }

  if (selectedplayer.numero == 263) {
    persosmort.push('Pasha2');
    persosmort.push('Soyos3');
    persosmort.push('Andaelor2');
  }

  if (selectedplayer.numero == 266) {
    persosmort.push('Pasha3');
    persosmort.push('Andaelor3');
  }

  if (selectedplayer.numero == 272) {
    persosmort.push('Pasha4');
  }

  if (selectedplayer.numero == 274) {
    persosmort.push('Pasha2');
    persosmort.push('Soyos3');
  }

  if (selectedplayer.numero == 276) {
    epilogue.push('Tu es aveugle');
  }

  if (selectedplayer.numero == 277) {
    persosmort.push('Méonie');

    if (rem) {
      persosmort.push('Rem');
    } else {
      persosmort.push('Nao');
    }
  }

  if (selectedplayer.numero == 280) {//persosmort.push('Pasha');
  }

  if (selectedplayer.numero == 283) {
    persosmort.push('Espéride7');
  }

  if (selectedplayer.numero == 285) {
    persosmort.push('Usaki');
    persosmort.push('Ymir');
    persosmort.push('Zaïra');
  }

  if (selectedplayer.numero == 293) {
    persosmort.push('Zaïra2');
  }

  if (selectedplayer.numero == 297) {
    persosmort.push('Pasha5');
  }

  if (selectedplayer.numero == 311) {
    persosmort.push('Ymir2');
    persosmort.push('Zaïra2');
  }

  if (selectedplayer.numero == 317) {
    persosmort.push('Soyos3');
    persosmort.push('Andaelor2');
  }

  if (selectedplayer.numero == 321) {
    persosmort.push('Pasha');
  }

  if (selectedplayer.numero == 322) {
    persosmort.push('Pasha6');
  }

  if (selectedplayer.numero == 324) {
    persosmort.push('Pasha');
    persosmort.push('Usaki2');
  }

  if (selectedplayer.numero == 325) {
    persosmort.push('Usaki3');
  }

  if (selectedplayer.numero == 331) {
    persosmort.push('Méonie5');
  }

  if (selectedplayer.numero == 334) {
    persosmort.push('Usaki4');
  }

  if (selectedplayer.numero == 340) {
    persosmort.push('Méonie5');
  }

  if (selectedplayer.numero == 348) {
    persosmort.push('MacLumy6');
  }

  if (selectedplayer.numero == 391) {
    persosmort.push('Panacle6');
    persosmort.push('Haziko3');
    persosmort.push('Kannan7');
    persosmort.push('Anselm5');
    persosmort.push('Espéride8');
    persosmort.push('Usaki5');
    persosmort.push('Méonie6');

    if (rem) {
      persosmort.push('Rem2');
    } else {
      persosmort.push('Nao2');
    }
  }

  if (selectedplayer.numero == 392) {
    persosmort.push('Lasseni');
    persosmort.push('Fandrean');
  }

  if (selectedplayer.numero == 393) {
    persosmort.push('Esteban');
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 396) {
    persosmort.push('Usaki');
  }

  if (selectedplayer.numero == 398) {
    if (rem) {
      persosmort.push('Rem3');
    } else {
      persosmort.push('Nao3');
    }
  }

  if (selectedplayer.numero == 402) {
    persosmort.push('Méonie7');
  }

  if (selectedplayer.numero == 404) {
    if (rem) {
      persosmort.push('Rem2');
    } else {
      persosmort.push('Nao2');
    }
  }

  if (selectedplayer.numero == 410) {
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 411) {
    persosmort.push('Usaki6');
  }

  if (selectedplayer.numero == 412) {
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 417) {
    if (rem) {
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }

  if (selectedplayer.numero == 420) {
    persosmort.push('Ymir2');
    persosmort.push('Usaki7');

    if (rem) {
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }

  if (selectedplayer.numero == 420) {
    persosmort.push('Ymir2');
    persosmort.push('Zaïra2');
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 429) {
    if (rem) {
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }

  if (selectedplayer.numero == 433) {
    persosmort.push('Ymir2');
    persosmort.push('Zaïra2');
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 436) {
    if (rem) {
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }

  if (selectedplayer.numero == 438) {
    persosmort.push('Pasha7');
  }

  if (selectedplayer.numero == 443) {
    if (rem) {
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }

  if (selectedplayer.numero == 446) {
    persosmort.push('Ymir2');
    persosmort.push('Usaki7');

    if (rem) {
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }

  if (selectedplayer.numero == 452) {
    persosmort.push('Andaelor2');
    persosmort.push('Soyos3');
  }

  if (selectedplayer.numero == 455) {
    persosmort.push('Usaki7');
  }

  if (selectedplayer.numero == 456) {
    if (rem) {
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }

    persosmort.push('Usaki7');
  }

  if (selectedplayer.numero == 461) {
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 465) {
    if (rem) {
      persosmort.push('Rem5');
    } else {
      persosmort.push('Nao5');
    }

    persosmort.push('Usaki8');
  }

  if (selectedplayer.numero == 467) {
    persosmort.push('Usaki9');
  }

  if (selectedplayer.numero == 470) {
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 471) {
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 473) {
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 475) {
    persosmort.push('Méonie6');
  }

  if (selectedplayer.numero == 477) {
    if (rem) {
      persosmort.push('Rem5');
    } else {
      persosmort.push('Nao5');
    }

    persosmort.push('Usaki7');
  }

  if (selectedplayer.numero == 479) {
    persosmort.push('Méonie6');
    persosmort.push('Haziko3');
  }

  saveInfoChap3();
}

function getInfoChap3() {
  chefazano = JSON.parse(selectedplayer.chefazano);
  vote = JSON.parse(selectedplayer.vote);
  tourdetruite = JSON.parse(selectedplayer.tourdetruite);
  persosmort = JSON.parse(selectedplayer.persosmort);
  epilogue = JSON.parse(selectedplayer.epilogue);
}

function saveInfoChap3() {
  selectedplayer.tourdetruite = JSON.stringify(tourdetruite);
  selectedplayer.vote = JSON.stringify(vote);
  selectedplayer.chefazano = JSON.stringify(chefazano);
  selectedplayer.persosmort = JSON.stringify(persosmort);
  selectedplayer.epilogue = JSON.stringify(epilogue);
  saveGame();
}

function resetgame3() {}
"use strict";

var heros = 0; // 1 = Armin - 2 = Yasha

var pouvoir = 0; // 1 = CROTALE / CRAPAUD - 2 = FENNEC / LIEVRE

function goActionLivre2_1() {
  // AFFICHAGE EN FONCTION DU HEROS
  if (heros == 1) {
    $('.h-1').show();
    $('.h-2').remove();
  } else {
    $('.h-1').remove();
    $('.h-2').show();
  } // AFFICHAGE EN FONCTION DU POUVOIR


  if (pouvoir == 1) {
    $('.p-1').show();
    $('.p-2').remove();
  } else {
    $('.p-1').remove();
    $('.p-2').show();
  } // CHOIX DU HEROS


  if (selectedplayer.numero == 1) {
    $('.ch').on('click', function () {
      if ($(this).hasClass('heros-1')) {
        heros = 1;
        selectedplayer.heros = 1;
      } else {
        heros = 2;
        selectedplayer.heros = 2;
      }
    });
    saveGame();
  } // CHOIX DU POUVOIR


  if (selectedplayer.numero == 2) {
    $('.ch').on('click', function () {
      if ($(this).hasClass('pouvoir-1')) {
        pouvoir = 1;
        selectedplayer.pouvoir = 1;
      } else {
        pouvoir = 2;
        selectedplayer.pouvoir = 2;
      }
    });
    saveGame();
  }

  if (selectedplayer.numero == 7) {
    selectedplayer.depart = 1; // Insulte Marez

    saveGame();
  }

  if (selectedplayer.numero == 9) {
    selectedplayer.depart = 2; // cliente raciste

    saveGame();
  }
}

function getInfoChap2_1() {
  heros = selectedplayer.heros;
  pouvoir = selectedplayer.pouvoir;
  depart = selectedplayer.depart;
}

function saveInfoChap2_1() {
  selectedplayer.depart = depart;
  selectedplayer.heros = heros;
  selectedplayer.pouvoir = pouvoir;
  saveGame();
}
"use strict";

function getMagesGroupe() {
  var mage1 = selectpersonnages.groupe.mission[1];
  var mage2 = selectpersonnages.groupe.mission[2];
  var mage3 = selectpersonnages.groupe.mission[3];

  if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "vent") {
    $('.mage-vent').html(mage1);

    if (getfillegarcon(mage1)) {
      $('.mage-vent-garcon').remove();
    } else {
      $('.mage-vent-fille').remove();
    }
  } else if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "vent") {
    $('.mage-vent').html(mage2);

    if (getfillegarcon(mage2)) {
      $('.mage-vent-garcon').remove();
    } else {
      $('.mage-vent-fille').remove();
    }
  } else if (selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "vent") {
    $('.mage-vent').html(mage3);

    if (getfillegarcon(mage3)) {
      $('.mage-vent-garcon').remove();
    } else {
      $('.mage-vent-fille').remove();
    }
  }

  if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "eau") {
    $('.mage-eau').html(mage1);

    if (getfillegarcon(mage1)) {
      $('.mage-eau-garcon').remove();
    } else {
      $('.mage-eau-fille').remove();
    }
  } else if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "eau") {
    $('.mage-eau').html(mage2);

    if (getfillegarcon(mage2)) {
      $('.mage-eau-garcon').remove();
    } else {
      $('.mage-eau-fille').remove();
    }
  } else if (selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "eau") {
    $('.mage-eau').html(mage3);

    if (getfillegarcon(mage3)) {
      $('.mage-eau-garcon').remove();
    } else {
      $('.mage-eau-fille').remove();
    }
  }

  if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "feu") {
    $('.mage-feu').html(mage1);

    if (getfillegarcon(mage1)) {
      $('.mage-feu-garcon').remove();
    } else {
      $('.mage-feu-fille').remove();
    }
  } else if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "feu") {
    $('.mage-feu').html(mage2);

    if (getfillegarcon(mage2)) {
      $('.mage-feu-garcon').remove();
    } else {
      $('.mage-feu-fille').remove();
    }
  } else if (selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "feu") {
    $('.mage-feu').html(mage3);

    if (getfillegarcon(mage3)) {
      $('.mage-feu-garcon').remove();
    } else {
      $('.mage-feu-fille').remove();
    }
  }

  if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "terre") {
    $('.mage-terre').html(mage1);

    if (getfillegarcon(mage1)) {
      $('.mage-terre-garcon').remove();
    } else {
      $('.mage-terre-fille').remove();
    }
  } else if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "terre") {
    $('.mage-terre').html(mage2);

    if (getfillegarcon(mage2)) {
      $('.mage-terre-garcon').remove();
    } else {
      $('.mage-terre-fille').remove();
    }
  } else if (selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "terre") {
    $('.mage-terre').html(mage3);

    if (getfillegarcon(mage3)) {
      $('.mage-terre-garcon').remove();
    } else {
      $('.mage-terre-fille').remove();
    }
  }
}

function getfillegarcon(nom) {
  var fille = false;

  if (nom.toLowerCase() == 'kannan' || nom.toLowerCase() == 'haziko' || nom.toLowerCase() == 'jayne' || nom.toLowerCase() == 'lasseni') {
    fille = true;
  }

  return fille;
}

function getGroupeMission() {
  var nom1 = selectpersonnages.groupe.mission[1];
  var nom2 = selectpersonnages.groupe.mission[2];
  var nom3 = selectpersonnages.groupe.mission[3]; //console.log(nom3);

  $('.grp1-1').html(nom1);
  $('.grp1-2').html(nom2);
  $('.grp1-3').html(nom3);

  if (nom1.toLowerCase() == 'kannan' || nom1.toLowerCase() == 'haziko' || nom1.toLowerCase() == 'jayne' || nom1.toLowerCase() == 'lasseni') {
    $('.grp1-1-garcon').remove();
  } else {
    $('.grp1-1-fille').remove();
  }

  if (nom2.toLowerCase() == 'kannan' || nom2.toLowerCase() == 'haziko' || nom2.toLowerCase() == 'jayne' || nom2.toLowerCase() == 'lasseni') {
    $('.grp1-2-garcon').remove();
  } else {
    $('.grp1-2-fille').remove();
  }

  if (nom3.toLowerCase() == 'kannan' || nom3.toLowerCase() == 'haziko' || nom3.toLowerCase() == 'jayne' || nom3.toLowerCase() == 'lasseni') {
    $('.grp1-3-garcon').remove();
  } else {
    $('.grp1-3-fille').remove();
  }
}

function getJayne() {
  var nom = "";
  var namee = "";

  if (selectpersonnages.jayn != null) {
    nom = 'Jayn';
  } else {
    nom = 'Jayne';
    namee = "e";
  }

  $(".nom-jayne-jayn").html(nom);
  $(".e-jayne-jayn").html(namee);
}

function nomautre() {
  var nomautre = '';

  if (selectpersonnages.anselm.magie == selectedplayer.magie) {
    nomautre = 'Anselm';
  } else if (selectpersonnages.haziko.magie == selectedplayer.magie) {
    nomautre = 'Haziko';
  } else if (selectpersonnages.kannan.magie == selectedplayer.magie) {
    nomautre = 'Kannan';
  } else if (selectpersonnages.lasseni.magie == selectedplayer.magie) {
    nomautre = 'Lasseni';
  } else if (selectpersonnages.maclumy.magie == selectedplayer.magie) {
    nomautre = 'Mac Lumy';
  } else if (selectpersonnages.panacle.magie == selectedplayer.magie) {
    nomautre = 'Panacle';
  } else {
    if (selectedplayer.sexe == 'fille') {
      nomautre = 'Jayn';
    } else {
      nomautre = 'Jayne';
    }
  }

  selectpersonnages.compagnon = nomautre;
  saveGame();
}

function insertNew(obj, entry, debloque) {
  if (obj[entry] == null) obj[entry] = {
    "debloque": false
  };

  if (debloque == undefined || debloque == null || debloque == '') {
    obj[entry].debloque = true;
  } else {
    obj[entry] = debloque;
  }
}
"use strict";

function initNew() {
  trad(lang);
  $('.livres').on('click', function () {
    if (!$(this).hasClass('locked')) {
      $('.livres').not($(this)).hide();
      $('.chapitre[data-livre="' + $(this).attr('data-livre') + '"]').show(); // console.log(selectedplayer);

      if (selectedplayer != null) {
        if (selectedplayer.livrenum == "") {
          $('.continue, .retour').hide();
          $('.new').show();
        } else if ($(this).attr('data-livre') == selectedplayer.livrenum) {
          $('.new, .retour').hide();
          $('.continue').show();
        } else {
          $('.new, .continue').hide();
          $('.retour').show();
        } // if((selectedplayer.livrenum) == ($(this).attr('data-livre'))){


        if (selectedplayer.chapitrenum == "chapitre1") {
          $('.titre__chapitre').removeClass('selected done');
          $('.chapitre-1 [data-chap="chapitre1"]').addClass('selected');
        }

        if (selectedplayer.chapitrenum == "chapitre2") {
          $('.titre__chapitre').removeClass('selected done');
          $('.chapitre-1 [data-chap="chapitre1"]').addClass('done');
          $('.chapitre-1 [data-chap="chapitre2"]').addClass('selected');
        }

        if (selectedplayer.chapitrenum == "chapitre3") {
          $('.titre__chapitre').removeClass('selected done');
          $('.chapitre-1 [data-chap="chapitre1"]').addClass('done');
          $('.chapitre-1 [data-chap="chapitre2"]').addClass('done');
          $('.chapitre-1 [data-chap="chapitre3"]').addClass('selected');
        }

        if (selectedplayer.chapitrenum == "chapitre21") {
          $('.titre__chapitre').removeClass('selected done');
          $('.chapitre-1 [data-chap="chapitre1"]').addClass('done');
          $('.chapitre-1 [data-chap="chapitre2"]').addClass('done');
          $('.chapitre-1 [data-chap="chapitre3"]').addClass('done');
          $('.chapitre-2 [data-chap="chapitre1"]').addClass('selected');
        } // }

      }
    }
  });
  $('.continue').on('click', function () {
    $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
      binding();
    });
  });
  $('.retour').on('click', function () {
    $('.livres').show();
    $('.chapitre').hide();
  });
  $('.new').on('click', function () {
    selectedplayer.livrenum = $(this).parent().attr('data-livre');
    selectedplayer.livre = $(this).parent().parent().find('.livres .titre').html();
    selectedplayer.chapitrenum = $(this).parent().find('.titre__chapitre.selected').attr('data-chap');
    selectedplayer.chapitre = $(this).parent().find('.titre__chapitre.selected').html();
    selectedplayer.numero = 1;
    saveGame();
    $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
      binding();
    });
  });

  if (selectedplayer == null) {
    if (joueur_saved != null && joueur_saved.joueur1 != null) {
      $('.nom').html(joueur_saved.joueur1.nom);
      selectedplayer = joueur_saved.joueur1;

      if (selectedplayer.chapitrenum == 'chapitre2') {
        $('.livres[data-livre="livre1"]').addClass('chapitre2');
        $('.titre__chapitre').removeClass('selected');
        $('.titre__chapitre[data-chap="chapitre2"]').addClass('selected');

        for (var keys in joueur_saved) {
          if (selectedplayer.nom == joueur_saved[keys].nom) {
            selectperso = persos[keys];
            selectlieux = lieux[keys];
            selectcreature = creatures[keys];
            selectpersonnages = personnages[keys];
            selectsorts = sorts[keys];
          }
        }
      }
    } else {
      $('.nom').html('Créer un personnage d\'abord');
      $('.new').hide();
    }
  } else {
    $('.nom').html(selectedplayer.nom);

    if (selectedplayer.chapitrenum == 'chapitre2') {
      $('.livres[data-livre="livre1"]').addClass('chapitre2');
      $('.titre__chapitre').removeClass('selected');
      $('.titre__chapitre[data-chap="chapitre2"]').addClass('selected');

      for (var keyss in joueur_saved) {
        if (selectedplayer.nom == joueur_saved[keyss].nom) {
          selectperso = persos[keyss];
          selectlieux = lieux[keyss];
          selectcreature = creatures[keyss];
          selectpersonnages = personnages[keyss];
          selectsorts = sorts[keyss];
        }
      }
    }
  }

  if (selectedplayer != null) {
    if (parseInt(selectedplayer.fin3) > 0 && selectedplayer.livrenum == "livre1") {
      selectedplayer.livrenum = "livre2";
      selectedplayer.chapitrenum = "chapitre21";
      selectedplayer.livre = "Le monde dévasté";
      selectedplayer.chapitre = "Chap.1 - Soleil brûlant";
      selectedplayer.numero = 1;
      saveGame();
    }

    if (selectedplayer.livrenum == "livre2") {
      $('.livres[data-livre="livre2"]').removeClass('locked');
    }
  }
}
"use strict";

function getinfo() {
  $('#listeinfo button').off().on('click', function () {
    var url = $(this).attr('data-info');
    $("main").load(url, function () {
      if (url == "template/infos/persos/index.html") getpersos();
      if (url == "template/infos/lieux/index.html") getlieux();
      if (url == "template/infos/creatures/index.html") getcreatures();
      if (url == "template/infos/carte/index.html") getcarte();
    });
  });
  $('.Export').off().on('click', function () {
    saveonline();
  });
}

function getcarte() {
  var multi = 5;
  $('.plussize').off().on('click', function () {
    multi++;

    if (multi > 9) {
      multi = 9;
    }

    $('.infoss-map img').width(multi * 100 + "%");
  });
  $('.moinssize').off().on('click', function () {
    multi--;

    if (multi < 1) {
      multi = 1;
    }

    $('.infoss-map img').width(multi * 100 + "%");
  });
}

function saveonline() {
  var myperso = JSON.stringify(selectperso);
  var mylieux = JSON.stringify(selectlieux);
  var mycreatures = JSON.stringify(selectcreature);
  var mypersonnages = JSON.stringify(selectpersonnages);
  var mysorts = JSON.stringify(selectsorts);
  var myplayer = JSON.stringify(selectedplayer);
  $.post("export/export.php", {
    func: "export",
    dataperso: myperso,
    datalieux: mylieux,
    datacreature: mycreatures,
    datapersonnages: mypersonnages,
    datasort: mysorts,
    dataplayer: myplayer,
    nom: selectedplayer.nom
  }, function (data) {
    $('.Export').text('SAUVEGARDE FAITE');
  });
}

function getpersos() {
  $('#infoss label').on('click', function () {
    $(this).toggleClass('open');
    $(this).parent().find('.contenu').slideToggle(200);
  });

  if (selectperso != null && selectperso != undefined) {
    for (var key in selectperso) {
      $('[data-perso="' + key + '"]').removeClass('hidden');
    }

    if (selectperso.Espéride && selectperso.Espéride.debloque) {
      if (selectperso.Espéride.triche) {
        $('[data-perso="Espéride"] .phrase2').removeClass('hidden');
      }
    }

    if (selectperso.Lasseni && selectperso.Lasseni.debloque) {
      $('[data-perso="Lasseni"] .place').html(selectpersonnages.lasseni.place);
      $('[data-perso="Lasseni"] .magie').html(selectpersonnages.lasseni.magie);
    }

    if (selectperso.Panacle && selectperso.Panacle.debloque) {
      $('[data-perso="Panacle"] .place').html(selectpersonnages.panacle.place);
      $('[data-perso="Panacle"] .magie').html(selectpersonnages.panacle.magie);
    }

    if (selectperso.Haziko && selectperso.Haziko.debloque) {
      $('[data-perso="Haziko"] .place').html(selectpersonnages.haziko.place);
      $('[data-perso="Haziko"] .magie').html(selectpersonnages.haziko.magie);
    }

    if (selectperso.Anselm && selectperso.Anselm.debloque) {
      $('[data-perso="Anselm"] .place').html(selectpersonnages.anselm.place);
      $('[data-perso="Anselm"] .magie').html(selectpersonnages.anselm.magie);
    }

    if (selectperso.MacLumy && selectperso.MacLumy.debloque) {
      $('[data-perso="MacLumy"] .place').html(selectpersonnages.macLumy.place);
      $('[data-perso="MacLumy"] .magie').html(selectpersonnages.macLumy.magie);

      if (selectperso.MacLumy.debloque2) {
        $('[data-perso="MacLumy"] .phrase2').removeClass('hidden');
      }
    }

    if (selectperso.Kannan && selectperso.Kannan.debloque) {
      $('[data-perso="Kannan"] .place').html(selectpersonnages.kannan.place);
      $('[data-perso="Kannan"] .magie').html(selectpersonnages.kannan.magie);

      if (selectperso.Kannan.debloque2) {
        $('[data-perso="Kannan"] .phrase2').removeClass('hidden');
      }
    }

    if (selectperso.Jayne && selectperso.Jayne.debloque) {
      $('[data-perso="Jayne"] .place').html(selectpersonnages.jayne.place);
      $('[data-perso="Jayne"] .magie').html(selectpersonnages.jayne.magie);
    }

    if (selectperso.Jayn && selectperso.Jayn.debloque) {
      $('[data-perso="Jayn"] .place').html(selectpersonnages.jayn.place);
      $('[data-perso="Jayn"] .magie').html(selectpersonnages.jayn.magie);
    }

    if (selectperso.Modrak && selectperso.Modrak.debloque2) {
      $('[data-perso="Modrak"] .phrase2').removeClass('hidden');
    }

    if (selectperso.Modrak && selectperso.Modrak.debloque3) {
      $('[data-perso="Modrak"] .phrase3').removeClass('hidden');
    }

    if (selectperso.Modrak && selectperso.Modrak.debloque4) {
      $('[data-perso="Modrak"] .phrase4').removeClass('hidden');
    }

    if (selectperso.Modrak && selectperso.Modrak.debloque5) {
      $('[data-perso="Modrak"] .phrase5').removeClass('hidden');
    }

    if (selectperso.Estrion && selectperso.Estrion.debloque2) {
      $('[data-perso="Estrion"] .phrase2').removeClass('hidden');
    }

    if (selectperso.Estrion && selectperso.Estrion.debloque3) {
      $('[data-perso="Estrion"] .phrase3').removeClass('hidden');
    }

    if (selectperso.Estrion && selectperso.Estrion.debloque4) {
      $('[data-perso="Estrion"] .phrase4').removeClass('hidden');
    }

    if (selectperso.Terrence && selectperso.Terrence.debloque2) {
      $('[data-perso="Terrence"] .phrase2').removeClass('hidden');
    }

    if (selectperso.Terrence && selectperso.Terrence.debloque3) {
      $('[data-perso="Terrence"] .phrase3').removeClass('hidden');
    }

    if (selectperso.Terrence && selectperso.Terrence.debloque4) {
      $('[data-perso="Terrence"] .phrase4').removeClass('hidden');
    }

    if (selectperso.Konakey && selectperso.Konakey.d2) {
      $('[data-perso="Konakey"] .phrase2').removeClass('hidden');
    }

    if (selectperso.Imp && selectperso.Imp.d2) {
      $('[data-perso="Imp"] .phrase2').removeClass('hidden');
    }

    if (selectperso.Imp && selectperso.Imp.d3) {
      $('[data-perso="Imp"] .phrase3').removeClass('hidden');
    }
  }

  if (persosmort != null && persosmort != undefined) {
    for (var key2 in persosmort) {
      $('[data-persomort="' + key2 + '"]').removeClass('hidden');
    }
  }
}

function getlieux() {
  $('#infoss label').on('click', function () {
    $(this).toggleClass('open');
    $(this).parent().find('.contenu').slideToggle(200);
  });

  if (selectlieux != null && selectlieux != undefined) {
    for (var key in selectlieux) {
      $('[data-lieux="' + key + '"]').removeClass('hidden');
    }
  }
}

function getcreatures() {
  $('#infoss label').on('click', function () {
    $(this).toggleClass('open');
    $(this).parent().find('.contenu').slideToggle(200);
  });
  console.log(selectcreature);

  if (selectcreature != null && selectcreature != undefined) {
    for (var key in selectcreature) {
      $('[data-creature="' + key + '"]').removeClass('hidden');
    }
  }
}
"use strict";

var current_modif_joueur = 0;

function initperso() {
  trad(lang);
  var joueur_new = []; // SEXE

  $('.img-fille, .img-garcon').on('click', function () {
    $('.popin').attr('data-sexe', $(this).attr('data-img')).show();
    $('.wrapperpopin .oui').off().on('click', function () {
      joueur_new.sexe = $('.popin').attr('data-sexe');
      $('.popin').removeAttr('data-sexe').hide();
      $('.part1').hide();
      $('.part2').show();
    });
    $('.wrapperpopin .non').off().on('click', function () {
      $('.popin').removeAttr('data-sexe').hide();
    });
  }); // NOM

  $('#validnom').off().on('click', function () {
    $('.popin .plus').html('Vous vous appelez donc bien: ' + $('#monnom').val() + ' ?').show();
    $('.popin').attr('data-nom', $('#monnom').val()).show();
    $('.wrapperpopin .oui').off().on('click', function () {
      joueur_new.nom = $('.popin').attr('data-nom');
      $('.popin').removeAttr('data-nom').hide();
      $('.part2').hide();
      $('.part3').show();
    });
    $('.wrapperpopin .non').off().on('click', function () {
      $('.popin').removeAttr('data-nom').hide();
    });
  }); // AVANTAGE

  $('.part3 button').off().on('click', function () {
    $('.popin').attr('data-av', $(this).attr('data-value')).show();
    $('.popin .plus').html($('.part3 p[data-value="' + $(this).attr('data-value') + '"]')).show();
    $('.wrapperpopin .oui').off().on('click', function () {
      joueur_new.avantage = $('.popin').attr('data-av');
      $('.popin').removeAttr('data-av').hide();
      $('.part3').hide();
      $('.part4').show();

      if (joueur_new.avantage == "studieux") {
        $('.tricheur').hide();
      }

      if (joueur_new.avantage == "courageux") {
        $('.peureux').hide();
      }

      if (joueur_new.avantage == "sportif") {
        $('.cache-sportif').show();
      }
    });
    $('.wrapperpopin .non').off().on('click', function () {
      $('.popin').removeAttr('data-av').hide();
      $('.popin .plus').html('');
    });
  }); // DEFAUT

  $('.part4 button').off().on('click', function () {
    $('.popin').attr('data-def', $(this).attr('data-value')).show();
    $('.popin .plus').html($('.part4 p[data-value="' + $(this).attr('data-value') + '"]')).show();
    $('.wrapperpopin .oui').off().on('click', function () {
      joueur_new.defaut = $('.popin').attr('data-def');
      $('.popin').removeAttr('data-def').hide();
      $('.part4').hide();

      if (typeof Storage !== "undefined") {
        var auj = new Date();

        if (joueur_saved == null) {
          joueur_saved = jQuery.parseJSON('{ "joueur1":{ "date": "' + auj + '"} }');
          joueur_saved.joueur1.nom = joueur_new.nom;
          joueur_saved.joueur1.sexe = joueur_new.sexe;
          joueur_saved.joueur1.avantage = joueur_new.avantage;
          joueur_saved.joueur1.defaut = joueur_new.defaut;
          joueur_saved.joueur1.livre = '';
          joueur_saved.joueur1.livrenum = '';
          joueur_saved.joueur1.chapitre = '';
          joueur_saved.joueur1.chapitrenum = '';
          joueur_saved.joueur1.numero = 0;
          joueur_saved.joueur1.objet = {};
          localStorage.setItem("joueurs", JSON.stringify(joueur_saved));
        } else {
          var nb_obj = Object.keys(joueur_saved).length;

          if (joueur_saved['joueur' + (nb_obj + 1)] == null) {
            joueur_saved['joueur' + (nb_obj + 1)] = {};
            joueur_saved['joueur' + (nb_obj + 1)].date = auj;
            joueur_saved['joueur' + (nb_obj + 1)].nom = joueur_new.nom;
            joueur_saved['joueur' + (nb_obj + 1)].sexe = joueur_new.sexe;
            joueur_saved['joueur' + (nb_obj + 1)].avantage = joueur_new.avantage;
            joueur_saved['joueur' + (nb_obj + 1)].defaut = joueur_new.defaut;
            joueur_saved['joueur' + (nb_obj + 1)].livre = '';
            joueur_saved['joueur' + (nb_obj + 1)].livrenum = '';
            joueur_saved['joueur' + (nb_obj + 1)].chapitre = '';
            joueur_saved['joueur' + (nb_obj + 1)].chapitrenum = '';
            joueur_saved['joueur' + (nb_obj + 1)].numero = 0;
            joueur_saved['joueur' + (nb_obj + 1)].objet = {};
            localStorage.setItem("joueurs", JSON.stringify(joueur_saved));
          }
        }
      }

      $("main").load("template/creer/affichage.html", function () {
        getPerso();
      });
    });
    $('.wrapperpopin .non').off().on('click', function () {
      $('.popin').removeAttr('data-def').hide();
      $('.popin .plus').html('');
    });
  });
}

function getPerso() {
  trad(lang);
  var html = "";
  var nb_obj;
  var my_width;

  if (joueur_saved != null) {
    // AFFICHAGE DES PERSOS ENREGISTRES
    nb_obj = Object.keys(joueur_saved).length;

    for (var keys in joueur_saved) {
      html += '<div class="fiche fiche-' + joueur_saved[keys].sexe + ' fiche-' + joueur_saved[keys].livrenum + ' fiche-' + joueur_saved[keys].chapitrenum + '" data-id="' + keys + '">';
      html += '<div class="nom">' + joueur_saved[keys].nom + '</div>';
      html += '<p class="livre">' + joueur_saved[keys].livre + '</p>';
      html += '<p class="chapitre">' + joueur_saved[keys].chapitre + '</p>';
      html += '<button class="continuerPartie">Continuer</button>';
      html += '</div>';
    } // AJUSTEMENT DU CAROUSSEL


    my_width = $('.allpersos').width() + 40;
    $('.allpersos').width(my_width * nb_obj);

    if (nb_obj == 1) {
      $('.previous_perso, .next_perso').addClass('disabled');
    } else if (nb_obj > 1) {
      $('.previous_perso').addClass('disabled');
    } else {
      $('.select_perso, .previous_perso, .next_perso').hide();
    }

    var current_pos = 1;
    $('.previous_perso').on('click', function () {
      if (!$(this).hasClass('disabled')) {
        current_pos--;

        if (current_pos == 1) {
          $(this).addClass('disabled');
        }

        $('.next_perso').removeClass('disabled');
        $('.allpersos').animate({
          'margin-left': '+=' + my_width + 'px'
        }, 500, function () {});
      }
    });
    $('.next_perso').on('click', function () {
      if (!$(this).hasClass('disabled')) {
        current_pos++;

        if (current_pos == nb_obj) {
          $(this).addClass('disabled');
        }

        $('.previous_perso').removeClass('disabled');
        $('.allpersos').animate({
          'margin-left': '-=' + my_width + 'px'
        }, 500, function () {});
      }
    }); // BIND DE LA SELECTION DE PERSO

    $('.select_perso').on('click', function () {
      current_modif_joueur = current_pos;
      $("main").load("template/creer/modifier.html", function () {
        modifperso();
      });
    });
  } else {
    $('.select_perso, .previous_perso, .next_perso').hide();
  }

  $('.allpersos').html(html);
  $('.allpersos .fiche').width(my_width * nb_obj / nb_obj); // BIND CREATION NEW PERSO

  $('#creer').on('click', function () {
    $("main").load("template/creer/perso.html", function () {
      initperso();
    });
  }); // CLICK SUR LA FICHE - bouton continuer

  $('.fiche').on('click', function () {
    var joueur_temp = $(this).attr('data-id');
    selectedplayer = joueur_saved["" + joueur_temp];
    selectperso = persos["" + joueur_temp];
    selectlieux = lieux["" + joueur_temp];
    selectcreature = creatures["" + joueur_temp];
    selectpersonnages = personnages["" + joueur_temp];
    selectsorts = sorts["" + joueur_temp];

    if (selectedplayer.livre.length == 0) {
      $('footer .livre').trigger('click');
    } else {
      $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
        binding();
      });
    }
  });
}

function modifperso() {
  var mymod;
  mymod = joueur_saved["joueur" + current_modif_joueur];
  $('.modif_nom input').val(mymod.nom);

  if (mymod.sexe == 'fille') {
    $('.portrait').addClass('fille');
  } else {
    $('.portrait').addClass('garcon');
  }

  $('.infos_av').html('<b>Avantage</b><br />' + mymod.avantage);
  $('.infos_def').html('<b>Défaut</b><br />' + mymod.defaut);
  $('.infos_livre').html('<b>Livre</b><br />' + mymod.livre);
  $('.infos_chapitre').html('<b>Chapitre</b><br />' + mymod.chapitre);
  $('.energie').html('<b>Energie</b> : ' + mymod.energie);
  $('.magie').html('<b>Magie favorite</b> : ' + mymod.magie);
  var niveau = 'Première année';

  if (mymod.chapitrenum == 2) {
    niveau = 'Troisième année';
  }

  $('.niveaumagie').html('<b>Niveau de magie</b> : ' + niveau);
}
"use strict";

var traduction;
var lang = 'fr';

function trad(lang) {
  $.getJSON("lang/" + lang + ".json", function (data) {
    traduction = data;
    traduire();
  });
  $('body').addClass('lang-' + lang);
}

function traduire() {
  $('[data-trad]').each(function () {
    var temp_trad = $(this).data('trad');
    $(this).html(traduction[temp_trad]);
  });
}
"use strict";

var joueur;
var joueur_saved;
var selectedplayer;
var persos;
var selectperso;
var lieux;
var selectlieux;
var creatures;
var selectcreature;
var personnages;
var selectpersonnages;
var sorts;
var selectsorts;
var mage1, mage2, mage3;
var unautre;
var index, index2;
var lesnoms;
var nb;
var nbcap;
var nbm;
var nomm;
var nomsall;
jQuery(document).ready(function ($) {
  // RECUP SAVED ?
  if (typeof Storage !== "undefined") {
    joueur_saved = jQuery.parseJSON(localStorage.getItem("joueurs"));
    persos = jQuery.parseJSON(localStorage.getItem("persos"));
    lieux = jQuery.parseJSON(localStorage.getItem("lieux"));
    creatures = jQuery.parseJSON(localStorage.getItem("creatures"));
    personnages = jQuery.parseJSON(localStorage.getItem("personnages"));
    sorts = jQuery.parseJSON(localStorage.getItem("sorts"));
  } // console.log(joueur_saved);


  if (joueur_saved != null) {} else {
    joueur_saved = jQuery.parseJSON('{}');
  }

  if (persos == null) {
    persos = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }

  if (lieux == null) {
    lieux = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }

  if (creatures == null) {
    creatures = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }

  if (personnages == null) {
    personnages = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }

  if (sorts == null) {
    sorts = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }

  trad(lang);
  $('.radiobt').parent().on('click', function () {
    $(this).find('.radiobt').toggleClass('checked');
  });
});
var groupes_mission = [];
var places_temp = [1, 2, 4, 5, 6, 7, 8];
var choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "vent"];

function saveGame() {
  //console.log(selectedplayer);
  if (selectedplayer.nom == joueur_saved.joueur1.nom) {
    joueur_saved.joueur1 = selectedplayer;
    persos.joueur1 = selectperso;
    lieux.joueur1 = selectlieux;
    creatures.joueur1 = selectcreature;
    personnages.joueur1 = selectpersonnages;
    sorts.joueur1 = selectsorts;
  } else if (selectedplayer.nom == joueur_saved.joueur2.nom) {
    joueur_saved.joueur2 = selectedplayer;
    persos.joueur2 = selectperso;
    lieux.joueur2 = selectlieux;
    creatures.joueur2 = selectcreature;
    personnages.joueur2 = selectpersonnages;
    sorts.joueur2 = selectsorts;
  } else if (selectedplayer.nom == joueur_saved.joueur3.nom) {
    joueur_saved.joueur3 = selectedplayer;
    persos.joueur3 = selectperso;
    lieux.joueur3 = selectlieux;
    creatures.joueur3 = selectcreature;
    personnages.joueur3 = selectpersonnages;
    sorts.joueur3 = selectsorts;
  }

  localStorage.setItem("joueurs", JSON.stringify(joueur_saved));
  localStorage.setItem("persos", JSON.stringify(persos));
  localStorage.setItem("lieux", JSON.stringify(lieux));
  localStorage.setItem("creatures", JSON.stringify(creatures));
  localStorage.setItem("personnages", JSON.stringify(personnages));
  localStorage.setItem("sorts", JSON.stringify(sorts));
  saveonline();
}

function binding() {
  if (selectedplayer.sexe == 'fille') {
    $('.sexe.sexe-garcon, .sg').remove();
  } else {
    $('.sexe.sexe-fille, .sf').remove();
  }

  if (selectedplayer.energie != null) {
    if (selectedplayer.energie == 'verte') {
      $('.magie-noire').remove();
      $('.magie-noire2').remove();
    } else {
      if (selectedplayer.energie == 'noire') {
        $('.magie-verte').remove();
        $('.magie-noire2').remove();
      } else {
        $('.magie-verte').remove();
      }
    }
  }

  $('.av-courageux, .av-populaire, .av-riche, .av-sportif, .av-studieux').hide();

  if (selectedplayer.avantage == 'courageux') {
    $('.av-courageux').show();
  }

  if (selectedplayer.avantage == 'populaire') {
    $('.av-populaire').show();
  }

  if (selectedplayer.avantage == 'riche') {
    $('.av-riche').show();
  }

  if (selectedplayer.avantage == 'sportif') {
    $('.av-sportif').show();
  }

  if (selectedplayer.avantage == 'studieux') {
    $('.av-studieux').show();
  } else {
    if (selectedplayer.chapitrenum != "chapitre1") {
      $('.av-studieux').remove();
    }
  }

  $('.def-bagarreur, .def-naif, .def-parfait, .def-peureux, .def-tricheur').hide();

  if (selectedplayer.defaut == 'bagarreur') {
    $('.def-bagarreur').show();
  }

  if (selectedplayer.defaut == 'naif') {
    $('.def-naif').show();
  }

  if (selectedplayer.defaut == 'parfait') {
    $('.def-parfait').show();
  }

  if (selectedplayer.defaut == 'peureux') {
    $('.def-peureux').show();
  }

  if (selectedplayer.defaut == 'tricheur') {
    $('.def-tricheur').show();
  }

  $('.magieMType').html(selectedplayer.magie);
  $('.magie-terre, .magie-feu, .magie-eau, .magie-vent').hide();

  if (selectedplayer.magie == 'terre') {
    $('.magie-terre').show();
  }

  if (selectedplayer.magie == 'feu') {
    $('.magie-feu').show();
  }

  if (selectedplayer.magie == 'eau') {
    $('.magie-eau').show();
  }

  if (selectedplayer.magie == 'vent') {
    $('.magie-vent').show();
  }

  if (selectedplayer.magie == 'terre') {
    $('.magie-pas-terre').hide();
  }

  if (selectedplayer.magie == 'feu') {
    $('.magie-pas-feu').hide();
  }

  if (selectedplayer.magie == 'eau') {
    $('.magie-pas-eau').hide();
  }

  if (selectedplayer.magie == 'vent') {
    $('.magie-pas-vent').hide();
  }

  if (selectedplayer.avantage == 'courageux') {
    $('.av-pas-courageux').remove();
  }

  if (selectedplayer.avantage == 'populaire') {
    $('.av-pas-populaire').remove();
  }

  if (selectedplayer.avantage == 'riche') {
    $('.av-pas-riche').remove();
  }

  if (selectedplayer.avantage == 'sportif') {
    $('.av-pas-sportif').remove();
  }

  if (selectedplayer.avantage == 'studieux') {
    $('.av-pas-studieux').remove();
  }

  if (selectedplayer.defaut == 'bagarreur') {
    $('.def-pas-bagarreur').remove();
  }

  if (selectedplayer.defaut == 'naif') {
    $('.def-pas-naif').remove();
  }

  if (selectedplayer.defaut == 'parfait') {
    $('.def-pas-parfait').remove();
  }

  if (selectedplayer.defaut == 'peureux') {
    $('.def-pas-peureux').remove();
  }

  if (selectedplayer.defaut == 'tricheur') {
    $('.def-pas-tricheur').remove();
  }

  $('.tonnom').html(selectedplayer.nom);
  $('.ch').off().on('click', function () {
    var num_temp = parseInt($(this).attr('data-go'));
    selectedplayer.numero = num_temp;
    saveGame();
    $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
      binding();
      $("body, .num, main, html").scrollTop(0);
      $(window).scrollTop(0);
      $(document).scrollTop(0);
    });
  }); // LIVRE 2

  if (selectedplayer.livrenum == 'livre2') {
    if (selectedplayer.chapitrenum == 'chapitre21') {
      goActionLivre2_1();
    } else {
      goActionLivre2_2();
    }
  } else if (selectedplayer.chapitrenum == 'chapitre1') {
    if (selectedplayer.numero > 125) {
      getGroupeMission();
      getMagesGroupe();
    }

    if (selectedplayer.numero == 1) {
      insertNew(selectperso, 'Ymir');
      insertNew(selectperso, 'Zaïra');
      insertNew(selectlieux, 'montsnoirs');
      insertNew(selectlieux, 'terresnord');
      insertNew(selectcreature, 'soufflardent');
      insertNew(selectcreature, 'worg');
      insertNew(selectcreature, 'pistre');
      insertNew(selectcreature, 'barlin');
      saveGame();
    }

    if (selectedplayer.numero == 2) {
      insertNew(selectlieux, 'Sarambe');
      insertNew(selectperso, 'Espéride');

      if (selectedplayer.avantage == 'riche') {
        insertNew(selectperso, 'Médeya');
      }

      if (selectedplayer.avantage == 'studieux') {
        insertNew(selectperso, 'Eran');
      }

      if (selectedplayer.avantage == 'populaire') {
        insertNew(selectperso, 'Annette');
        insertNew(selectperso, 'Rodolphe');
      }

      saveGame();
    }

    if (selectedplayer.numero == 4) {
      selectedplayer.magieforce = 'vent';
      selectperso.Espéride.triche = true;
      saveGame();
    }

    if (selectedplayer.numero == 5 || selectedplayer.numero == 6 || selectedplayer.numero == 7 || selectedplayer.numero == 8) {
      selectedplayer.debloqueinfo = 'vrai';
      saveGame();
    }

    if (selectedplayer.numero == 5) {
      selectedplayer.place = '1';
      saveGame();
    }

    if (selectedplayer.numero == 6) {
      selectedplayer.place = '4';
      saveGame();
    }

    if (selectedplayer.numero == 7) {
      selectedplayer.place = '3';
      saveGame();
    }

    if (selectedplayer.numero == 8) {
      selectedplayer.place = '9';
      selectedplayer.tricheur = 'vrai';
      saveGame();
    }

    if (selectedplayer.numero == 9) {
      if (selectedplayer.avantage == 'riche') {
        insertNew(selectperso, 'Romond');
      }

      insertNew(selectlieux, 'Azalys');
      insertNew(selectlieux, 'Déluine');
      saveGame();
    }

    if (selectedplayer.numero == 11) {
      selectedplayer.bataille = 'vrai';
      saveGame();
    }

    if (selectedplayer.numero == 12) {
      selectedplayer.trahison = 'vrai';
      saveGame();
    }

    if (selectedplayer.numero == 13) {
      selectedplayer.choix = 'vrai';
      saveGame();
    }

    if (selectedplayer.numero == 14) {
      selectedplayer.malediction = 'vrai';
      saveGame();
    }

    if (selectedplayer.numero == 15) {
      selectedplayer.voile = 'vrai';
      saveGame();
    }

    if (selectedplayer.numero == 16) {
      if (selectedplayer.tricheur == 'vrai') {
        $('.pas-tricheur').hide();
      } else {
        $('.tricheur').hide();
      }

      if (selectedplayer.place == '1') {
        $('.place-3, .place-4, .place-9').hide();
      } else if (selectedplayer.place == '3') {
        $('.place-1, .place-4, .place-9').hide();
      } else if (selectedplayer.place == '4') {
        $('.place-1, .place-3, .place-9').hide();
      } else if (selectedplayer.place == '9') {
        $('.place-1, .place-3, .place-4').hide();
      }

      insertNew(selectperso, 'Dioné');
      insertNew(selectperso, 'Soyos');
      var groupes_chambre = [];

      if (selectedplayer.sexe == "fille") {
        groupes_chambre = [selectedplayer.nom, 'Kannan', 'Lasseni', 'Haziko', 'Mac Lumy', 'Jayn', 'Anselm', 'Panacle'];
      } else {
        groupes_chambre = [selectedplayer.nom, 'Mac Lumy', 'Lasseni', 'Haziko', 'Kannan', 'Jayne', 'Anselm', 'Panacle'];
      }

      if (selectpersonnages.groupe == null) {
        selectpersonnages.groupe = {
          "chambre": groupes_chambre
        };
      }

      saveGame();
    }

    if (selectedplayer.numero == 21) {
      selectedplayer.magie = 'vent';
      choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "terre"];

      if (selectedplayer.sexe == "fille") {
        groupes_mission = [selectedplayer.nom, 'Kannan', 'Mac Lumy', 'Anselm', 'Lasseni', 'Jayn', 'Haziko', 'Panacle'];
      } else {
        groupes_mission = [selectedplayer.nom, 'Kannan', 'Haziko', 'Anselm', 'Lasseni', 'Jayne', 'Mac Lumy', 'Panacle'];
      }

      selectpersonnages.groupe.mission = groupes_mission;

      if (selectedplayer.magieforce == "vent") {
        places_temp = [1, 2, 3, 5, 6, 7, 8];
      } else {
        if (selectedplayer.place == '1') {
          places_temp = [2, 3, 4, 5, 6, 7, 8];
        } else if (selectedplayer.place == '3') {
          places_temp = [1, 2, 4, 5, 6, 7, 8];
        } else if (selectedplayer.place == '4') {
          places_temp = [1, 2, 3, 5, 6, 7, 8];
        }
      }

      insertNew(selectpersonnages, 'lasseni', {
        "place": places_temp[0]
      });
      selectpersonnages.lasseni.magie = choix_temp[0];
      insertNew(selectpersonnages, 'maclumy', {
        "place": places_temp[1]
      });
      selectpersonnages.maclumy.magie = choix_temp[1];
      insertNew(selectpersonnages, 'haziko', {
        "place": places_temp[2]
      });
      selectpersonnages.haziko.magie = choix_temp[2];
      insertNew(selectpersonnages, 'anselm', {
        "place": places_temp[3]
      });
      selectpersonnages.anselm.magie = choix_temp[3];
      insertNew(selectpersonnages, 'kannan', {
        "place": places_temp[4]
      });
      selectpersonnages.kannan.magie = choix_temp[4];
      insertNew(selectpersonnages, 'panacle', {
        "place": places_temp[5]
      });
      selectpersonnages.panacle.magie = choix_temp[5];

      if (selectedplayer.sexe == 'fille') {
        insertNew(selectpersonnages, 'jayn', {
          "place": places_temp[6]
        });
        selectpersonnages.jayn.magie = choix_temp[6];
      } else {
        insertNew(selectpersonnages, 'jayne', {
          "place": places_temp[6]
        });
        selectpersonnages.jayne.magie = choix_temp[6];
      }

      $('[data-chambre="0"]').html(selectpersonnages.groupe.chambre[0]);
      $('[data-chambre="1"]').html(selectpersonnages.groupe.chambre[1]);
      $('[data-chambre="2"]').html(selectpersonnages.groupe.chambre[2]);
      $('[data-chambre="3"]').html(selectpersonnages.groupe.chambre[3]);
      $('[data-chambre="4"]').html(selectpersonnages.groupe.chambre[4]);
      $('[data-chambre="5"]').html(selectpersonnages.groupe.chambre[5]);
      $('[data-chambre="6"]').html(selectpersonnages.groupe.chambre[6]);
      $('[data-chambre="7"]').html(selectpersonnages.groupe.chambre[7]);
      saveGame();
    }

    if (selectedplayer.numero == 22) {
      selectedplayer.magie = 'eau';

      if (selectedplayer.place == '1') {
        places_temp = [2, 3, 4, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "feu", "terre", "vent", "vent", "terre"];
      } else if (selectedplayer.place == '3') {
        places_temp = [1, 2, 4, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "feu", "terre", "vent", "vent", "terre"];
      } else if (selectedplayer.place == '4') {
        places_temp = [1, 2, 3, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "feu", "terre", "vent", "vent", "terre"];
      }

      if (selectedplayer.sexe == "fille") {
        groupes_mission = [selectedplayer.nom, 'Kannan', 'Mac Lumy', 'Anselm', 'Lasseni', 'Jayn', 'Haziko', 'Panacle'];
      } else {
        groupes_mission = [selectedplayer.nom, 'Kannan', 'Haziko', 'Anselm', 'Lasseni', 'Jayne', 'Mac Lumy', 'Panacle'];
      }

      selectpersonnages.groupe.mission = groupes_mission;
      insertNew(selectpersonnages, 'lasseni', {
        "place": places_temp[0]
      });
      selectpersonnages.lasseni.magie = choix_temp[0];
      insertNew(selectpersonnages, 'maclumy', {
        "place": places_temp[1]
      });
      selectpersonnages.maclumy.magie = choix_temp[1];
      insertNew(selectpersonnages, 'haziko', {
        "place": places_temp[2]
      });
      selectpersonnages.haziko.magie = choix_temp[2];
      insertNew(selectpersonnages, 'anselm', {
        "place": places_temp[3]
      });
      selectpersonnages.anselm.magie = choix_temp[3];
      insertNew(selectpersonnages, 'kannan', {
        "place": places_temp[4]
      });
      selectpersonnages.kannan.magie = choix_temp[4];
      insertNew(selectpersonnages, 'panacle', {
        "place": places_temp[5]
      });
      selectpersonnages.panacle.magie = choix_temp[5];

      if (selectedplayer.sexe == 'fille') {
        insertNew(selectpersonnages, 'jayn', {
          "place": places_temp[6]
        });
        selectpersonnages.jayn.magie = choix_temp[6];
      } else {
        insertNew(selectpersonnages, 'jayne', {
          "place": places_temp[6]
        });
        selectpersonnages.jayne.magie = choix_temp[6];
      }

      $('[data-chambre="0"]').html(selectpersonnages.groupe.chambre[0]);
      $('[data-chambre="1"]').html(selectpersonnages.groupe.chambre[1]);
      $('[data-chambre="2"]').html(selectpersonnages.groupe.chambre[2]);
      $('[data-chambre="3"]').html(selectpersonnages.groupe.chambre[3]);
      $('[data-chambre="4"]').html(selectpersonnages.groupe.chambre[4]);
      $('[data-chambre="5"]').html(selectpersonnages.groupe.chambre[5]);
      $('[data-chambre="6"]').html(selectpersonnages.groupe.chambre[6]);
      $('[data-chambre="7"]').html(selectpersonnages.groupe.chambre[7]);
      saveGame();
    }

    if (selectedplayer.numero == 23) {
      selectedplayer.magie = 'feu';

      if (selectedplayer.place == '1') {
        places_temp = [2, 3, 4, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "vent", "terre", "eau", "vent", "terre"];
      } else if (selectedplayer.place == '3') {
        places_temp = [1, 2, 4, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "vent", "terre", "eau", "vent", "terre"];
      } else if (selectedplayer.place == '4') {
        places_temp = [1, 2, 3, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "vent", "terre", "eau", "vent", "terre"];
      }

      if (selectedplayer.sexe == "fille") {
        groupes_mission = [selectedplayer.nom, 'Panacle', 'Kannan', 'Jayn', 'Lasseni', 'Anselm', 'Mac Lumy', 'Haziko'];
      } else {
        groupes_mission = [selectedplayer.nom, 'Kannan', 'Haziko', 'Anselm', 'Lasseni', 'Jayne', 'Mac Lumy', 'Panacle'];
      }

      selectpersonnages.groupe.mission = groupes_mission;
      insertNew(selectpersonnages, 'lasseni', {
        "place": places_temp[0]
      });
      selectpersonnages.lasseni.magie = choix_temp[0];
      insertNew(selectpersonnages, 'maclumy', {
        "place": places_temp[1]
      });
      selectpersonnages.maclumy.magie = choix_temp[1];
      insertNew(selectpersonnages, 'haziko', {
        "place": places_temp[2]
      });
      selectpersonnages.haziko.magie = choix_temp[2];
      insertNew(selectpersonnages, 'anselm', {
        "place": places_temp[3]
      });
      selectpersonnages.anselm.magie = choix_temp[3];
      insertNew(selectpersonnages, 'kannan', {
        "place": places_temp[4]
      });
      selectpersonnages.kannan.magie = choix_temp[4];
      insertNew(selectpersonnages, 'panacle', {
        "place": places_temp[5]
      });
      selectpersonnages.panacle.magie = choix_temp[5];

      if (selectedplayer.sexe == 'fille') {
        insertNew(selectpersonnages, 'jayn', {
          "place": places_temp[6]
        });
        selectpersonnages.jayn.magie = choix_temp[6];
      } else {
        insertNew(selectpersonnages, 'jayne', {
          "place": places_temp[6]
        });
        selectpersonnages.jayne.magie = choix_temp[6];
      }

      $('[data-chambre="0"]').html(selectpersonnages.groupe.chambre[0]);
      $('[data-chambre="1"]').html(selectpersonnages.groupe.chambre[1]);
      $('[data-chambre="2"]').html(selectpersonnages.groupe.chambre[2]);
      $('[data-chambre="3"]').html(selectpersonnages.groupe.chambre[3]);
      $('[data-chambre="4"]').html(selectpersonnages.groupe.chambre[4]);
      $('[data-chambre="5"]').html(selectpersonnages.groupe.chambre[5]);
      $('[data-chambre="6"]').html(selectpersonnages.groupe.chambre[6]);
      $('[data-chambre="7"]').html(selectpersonnages.groupe.chambre[7]);
      saveGame();
    }

    if (selectedplayer.numero == 24) {
      selectedplayer.magie = 'terre';

      if (selectedplayer.place == '1') {
        places_temp = [2, 3, 4, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "vent"];
      } else if (selectedplayer.place == '3') {
        places_temp = [1, 2, 4, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "vent"];
      } else if (selectedplayer.place == '4') {
        places_temp = [1, 2, 3, 5, 6, 7, 8];
        choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "vent"];
      }

      if (selectedplayer.sexe == "fille") {
        groupes_mission = [selectedplayer.nom, 'Panacle', 'Mac Lumy', 'Kannan', 'Lasseni', 'Jayn', 'Haziko', 'Anselm'];
      } else {
        groupes_mission = [selectedplayer.nom, 'Panacle', 'Haziko', 'Kannan', 'Lasseni', 'Jayne', 'Mac Lumy', 'Anselm'];
      }

      selectpersonnages.groupe.mission = groupes_mission;
      insertNew(selectpersonnages, 'lasseni', {
        "place": places_temp[0]
      });
      selectpersonnages.lasseni.magie = choix_temp[0];
      insertNew(selectpersonnages, 'maclumy', {
        "place": places_temp[1]
      });
      selectpersonnages.maclumy.magie = choix_temp[1];
      insertNew(selectpersonnages, 'haziko', {
        "place": places_temp[2]
      });
      selectpersonnages.haziko.magie = choix_temp[2];
      insertNew(selectpersonnages, 'anselm', {
        "place": places_temp[3]
      });
      selectpersonnages.anselm.magie = choix_temp[3];
      insertNew(selectpersonnages, 'kannan', {
        "place": places_temp[4]
      });
      selectpersonnages.kannan.magie = choix_temp[4];
      insertNew(selectpersonnages, 'panacle', {
        "place": places_temp[5]
      });
      selectpersonnages.panacle.magie = choix_temp[5];

      if (selectedplayer.sexe == 'fille') {
        insertNew(selectpersonnages, 'jayn', {
          "place": places_temp[6]
        });
        selectpersonnages.jayn.magie = choix_temp[6];
      } else {
        insertNew(selectpersonnages, 'jayne', {
          "place": places_temp[6]
        });
        selectpersonnages.jayne.magie = choix_temp[6];
      }

      $('[data-chambre="0"]').html(selectpersonnages.groupe.chambre[0]);
      $('[data-chambre="1"]').html(selectpersonnages.groupe.chambre[1]);
      $('[data-chambre="2"]').html(selectpersonnages.groupe.chambre[2]);
      $('[data-chambre="3"]').html(selectpersonnages.groupe.chambre[3]);
      $('[data-chambre="4"]').html(selectpersonnages.groupe.chambre[4]);
      $('[data-chambre="5"]').html(selectpersonnages.groupe.chambre[5]);
      $('[data-chambre="6"]').html(selectpersonnages.groupe.chambre[6]);
      $('[data-chambre="7"]').html(selectpersonnages.groupe.chambre[7]);
      saveGame();
    }

    if (selectedplayer.numero == 25) {
      insertNew(selectperso, 'Lasseni');
      insertNew(selectperso, 'MacLumy');
      insertNew(selectperso, 'Kannan');
      insertNew(selectperso, 'Panacle');
      insertNew(selectperso, 'Haziko');
      insertNew(selectperso, 'Anselm');

      if (selectpersonnages.jayn != null) {
        insertNew(selectperso, 'Jayn');
      } else {
        insertNew(selectperso, 'Jayne');
      }

      if (selectedplayer.sexe == 'fille') {
        selectperso.Kannan.debloque2 = true;
        insertNew(selectlieux, 'Téogile');
      } else {
        selectperso.MacLumy.debloque2 = true;
        insertNew(selectlieux, 'Melb');
      }

      saveGame();
    }

    if (selectedplayer.numero == 26) {
      if (selectedplayer.avantage == 'courageux') {
        selectedplayer.objet.piment = true;
      }

      if (selectedplayer.avantage == 'riche') {
        selectedplayer.objet.fiole = true;
      }

      insertNew(selectlieux, 'Marina');
      saveGame();
    }

    if (selectedplayer.numero == 27) {
      $('.malediction').hide();

      if (selectedplayer.malediction != null) {
        if (selectedplayer.malediction == 'vrai') {
          $('.malediction').show();
        }
      }

      insertNew(selectlieux, 'Azalysbibliotheque');
      saveGame();
    }

    if (selectedplayer.numero == 28) {
      insertNew(selectperso, 'Nello');
      insertNew(selectperso, 'Appou');

      if (selectedplayer.avantage == 'populaire') {
        selectperso.Appou.ami = true;
        selectperso.Nello.ami = true;
      }

      saveGame();
    }

    if (selectedplayer.numero == 30) {
      insertNew(selectcreature, 'Noctule');
      saveGame();
    }

    if (selectedplayer.numero == 33) {
      insertNew(selectperso, 'Qobur');
      $('.ch').hide();

      if (selectedplayer.magie == 'vent') {
        $('.magie-vent').show();
      } else if (selectedplayer.magie == 'eau') {
        $('.magie-eau').show();
      } else if (selectedplayer.magie == 'feu') {
        $('.magie-feu').show();
      } else {
        $('.magie-terre').show();
      }

      selectedplayer.energie = "verte";
      saveGame();
    }

    if (selectedplayer.numero == 34) {
      $('.ch').hide();

      if (selectedplayer.magie == 'vent') {
        $('.magie-vent').show();
      } else if (selectedplayer.magie == 'eau') {
        $('.magie-eau').show();
      } else if (selectedplayer.magie == 'feu') {
        $('.magie-feu').show();
      } else {
        $('.magie-terre').show();
      }

      selectedplayer.energie = "noire";
      saveGame();
    }

    if (selectedplayer.numero == 35 || selectedplayer.numero == 36 || selectedplayer.numero == 37 || selectedplayer.numero == 38) {
      nomautre();
      $('.nom-autre').html(selectpersonnages.compagnon);
    }

    if (selectedplayer.numero == 36) {}

    if (selectedplayer.numero == 37) {
      insertNew(selectperso, 'Osman');
      saveGame();
    }

    if (selectedplayer.numero == 38) {
      insertNew(selectperso, 'Guérénile');
      saveGame();
    }

    if (selectedplayer.numero == 39 || selectedplayer.numero == 40) {
      $('.nom-autre').html(selectpersonnages.compagnon);
      console.log(selectsorts);
      insertNew(selectsorts, 'masque');
      saveGame();
    }

    if (selectedplayer.numero == 41 || selectedplayer.numero == 42) {
      if (selectedplayer.numero == 41 && selectedplayer.energie == 'noire') {
        selectpersonnages.lasseni.peur = true;
        selectedplayer.surveille = true;
      }

      $('.nom-autre').html(selectpersonnages.compagnon);
      insertNew(selectsorts, 'glaciation');
      saveGame();
    }

    if (selectedplayer.numero == 43 || selectedplayer.numero == 44) {
      $('.nom-autre').html(selectpersonnages.compagnon);
      insertNew(selectsorts, 'lumiere');
      saveGame();
    }

    if (selectedplayer.numero == 45) {
      $('.nom-autre').html(selectpersonnages.compagnon);
      insertNew(selectsorts, 'racines');
      saveGame();
    }

    if (selectedplayer.numero == 46) {
      $('.nom-autre').html(selectpersonnages.compagnon);
      insertNew(selectsorts, 'chute');
      saveGame();
    }

    if (selectedplayer.numero == 47) {
      $('.nom-autre').html(selectpersonnages.compagnon);
      insertNew(selectsorts, 'vision');
      saveGame();
    }

    if (selectedplayer.numero == 48) {
      $('.nom-autre').html(selectpersonnages.compagnon);
      insertNew(selectsorts, 'flamme');
      saveGame();
    }

    if (selectedplayer.numero == 49) {
      $('.nom-autre').html(selectpersonnages.compagnon);
      insertNew(selectsorts, 'guerison');
      saveGame();
    }

    if (selectedplayer.numero == 50) {
      $('.nom-autre').html(selectpersonnages.groupe.chambre[1]);
      getJayne();

      if (selectpersonnages.lasseni.peur != null) {
        if (selectpersonnages.lasseni.peur == true) {
          $('.lassenipeur').removeClass('hidden');
        }
      }

      insertNew(selectlieux, 'Condorra');
      insertNew(selectlieux, 'Azano');
      insertNew(selectlieux, 'GrandCouronne');
      insertNew(selectcreature, 'Manticore');
      insertNew(selectcreature, 'Marandouins');
      insertNew(selectcreature, 'Héréons');

      if (selectedplayer.avantage == 'riche') {
        insertNew(selectperso, 'Cellya');
      }

      if (selectperso.Lasseni.debloque == true) {
        selectperso.Lasseni.rumeur = true;
      }

      if (selectedplayer.place == "9") {
        $('.place-9').removeClass('hidden');
      } else if (selectedplayer.place == "1") {
        $('.place-1').removeClass('hidden');
      } else {
        $('.place-3, .place-4').removeClass('hidden');
      }

      if (selectperso.Jayn != null && selectperso.Jayn != undefined) {
        $('.nom-jayne-jayn').html('Jayn');
      } else {
        $('.nom-jayne-jayn').html('Jayne');
        $('.e-jayne-jayn').removeClass('hidden');
      }
    }

    if (selectedplayer.numero == 51) {
      insertNew(selectcreature, 'moufton');
      insertNew(selectcreature, 'ruflard');
      insertNew(selectcreature, 'canidin');
      insertNew(selectcreature, 'félidé');
      saveGame();
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
      $('.grp2-1').html(selectpersonnages.groupe.mission[4]);
      $('.grp2-2').html(selectpersonnages.groupe.mission[5]);
      $('.grp2-3').html(selectpersonnages.groupe.mission[6]);
      $('.grp2-4').html(selectpersonnages.groupe.mission[7]);
    }

    if (selectedplayer.numero == 52) {
      insertNew(selectperso, 'Miguel');
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
      $('.grp2-1').html(selectpersonnages.groupe.mission[4]);
      $('.grp2-2').html(selectpersonnages.groupe.mission[5]);
      $('.grp2-3').html(selectpersonnages.groupe.mission[6]);
      $('.grp2-4').html(selectpersonnages.groupe.mission[7]);
      selectpersonnages.groupe.chef = [selectedplayer.nom, 'Lasseni'];
      saveGame();
    }

    if (selectedplayer.numero == 53) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
      $('.grp2-1').html(selectpersonnages.groupe.mission[4]);
      $('.grp2-2').html(selectpersonnages.groupe.mission[5]);
      $('.grp2-3').html(selectpersonnages.groupe.mission[6]);
      $('.grp2-4').html(selectpersonnages.groupe.mission[7]);
      selectpersonnages.groupe.chef = [selectpersonnages.groupe.mission[1], 'Lasseni'];
      saveGame();
    }

    if (selectedplayer.numero == 54) {
      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      }
    }

    if (selectedplayer.numero == 55) {
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp2-2').html(selectpersonnages.groupe.mission[5]);
    }

    if (selectedplayer.numero == 56) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);

      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      insertNew(selectperso, 'Guyot');
      insertNew(selectlieux, 'Paumet');
      saveGame();
    }

    if (selectedplayer.numero == 57) {
      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }
    }

    if (selectedplayer.numero == 58) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);

      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.ch.chef').remove();
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      if (selectedplayer.energie == 'noire') {
        $('.energie-noire').removeClass('hidden');
        $('.energie-verte').remove();
      } else {
        $('.energie-verte').removeClass('hidden');
        $('.energie-noire').remove();
      }
    }

    if (selectedplayer.numero == 59) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
      insertNew(selectcreature, 'longlant');
      saveGame();
    }

    if (selectedplayer.numero == 60 || selectedplayer.numero == 61) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
    }

    if (selectedplayer.numero == 62) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
    }

    if (selectedplayer.numero == 63) {
      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }
    }

    if (selectedplayer.numero == 64) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
    }

    if (selectedplayer.numero == 65) {
      if (selectedplayer.avantage == "sportif") {
        if (selectedplayer.magie == "terre") {
          $('.ch').not('.super-racine').remove();
          $('.pas-super-racine').remove();
        } else {
          $('.ch').not('.attaque').remove();
          $('.super-racine').remove();
        }
      } else {
        if (selectedplayer.magie == "terre" && selectedplayer.energie == "noire") {
          $('.ch').not('.super-racine').remove();
          $('.pas-super-racine').remove();
        } else if (selectedplayer.magie == "vent") {
          $('.ch').not('.chute-contro').remove();
          $('.super-racine').remove();
        } else {
          $('.ch').not('.chute-autre').remove();
          $('.super-racine').remove();
        }
      }

      getMagesGroupe();
    }

    if (selectedplayer.numero == 66) {
      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      selectedplayer.mission1 = 'echec';
      saveGame();
    }

    if (selectedplayer.numero == 67) {
      mage1 = selectpersonnages.groupe.mission[1];
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];

      if (selectedplayer.magie == 'vent') {
        $('.magie-pas-vent, .chute-autre').remove();
      } else {
        $('.magie-vent, .chute-contro').remove();

        if (selectpersonnages[mage1.toLowerCase()].magie == "vent") {
          $('.mage-vent').html(mage1);
        } else if (selectpersonnages[mage2.toLowerCase()].magie == "vent") {
          $('.mage-vent').html(mage2);
        } else if (selectpersonnages[mage3.toLowerCase()].magie == "vent") {
          $('.mage-vent').html(mage3);
        }
      }
    }

    if (selectedplayer.numero == 68) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
      getMagesGroupe();

      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      if (selectedplayer.defaut == 'bagarreur') {
        selectedplayer.mission1 = 'reussie';
        saveGame();
      }
    }

    if (selectedplayer.numero == 69) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
      getMagesGroupe();

      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      selectedplayer.mission1 = 'reussie';
      saveGame();
    }

    if (selectedplayer.numero == 70 || selectedplayer.numero == 71) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);

      if (selectedplayer.energie == "noire") {
        $('.ernegie-noire').removeClass('hidden');
      } else {
        $('.ernegie-verte').removeClass('hidden');
      }

      getMagesGroupe();
    }

    if (selectedplayer.numero == 72) {
      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
      getMagesGroupe();
    }

    if (selectedplayer.numero == 73) {
      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
      getMagesGroupe();
    }

    if (selectedplayer.numero == 74) {
      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }
    }

    if (selectedplayer.numero == 75) {
      $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
      $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
      $('.grp1-3').html(selectpersonnages.groupe.mission[3]);

      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
      } else {
        $('.cheh').removeClass('hidden');
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      selectedplayer.mission1 = 'reussie';
      saveGame();
    }

    if (selectedplayer.numero == 76) {
      $('.nom-autre').html(selectpersonnages.groupe.chambre[1]);
      getGroupeMission();
    }

    if (selectedplayer.numero == 79) {
      $('.nom-autre').html(selectpersonnages.groupe.chambre[1]);
      getGroupeMission();
    }

    if (selectedplayer.numero == 80) {
      $('.nom-autre').html(selectpersonnages.groupe.chambre[1]);

      if (selectpersonnages.groupe.chambre[1] == selectpersonnages.groupe.mission[1]) {
        $('.sipasmeme').remove();
      } else {
        $('.simeme').remove();
      }

      getGroupeMission();
    }

    if (selectedplayer.numero == 81) {
      if (selectedplayer.sexe == 'fille') {
        insertNew(selectperso, 'Levy');
      } else {
        insertNew(selectperso, 'Levy2');
      }

      insertNew(selectperso, 'Esteban');
      saveGame();
    }

    if (selectedplayer.numero == 82) {
      insertNew(selectperso, 'Estrion');
      insertNew(selectlieux, 'Darmen');
      getGroupeMission();
      saveGame();
    }

    if (selectedplayer.numero == 83) {
      insertNew(selectperso, 'Hector');
      getGroupeMission();
      saveGame();
    }

    if (selectedplayer.numero == 84) {
      getGroupeMission();

      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
        $('.ch.cheh').remove();
      } else {
        $('.cheh').removeClass('hidden');
        $('.ch.chef').remove();
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }
    }

    if (selectedplayer.numero == 85 || selectedplayer.numero == 86 || selectedplayer.numero == 87 || selectedplayer.numero == 88) {
      getGroupeMission();
    }

    if (selectedplayer.numero == 87) {
      insertNew(selectperso, 'Jolt');
      saveGame();
    }

    if (selectedplayer.numero == 89) {
      getGroupeMission();
      getMagesGroupe();

      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
        $('.ch.cheh').remove();
      } else {
        $('.cheh').removeClass('hidden');
        $('.ch.chef').remove();
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);

        if (selectpersonnages.groupe.chef[0] == $('.mage-vent').text()) {
          $('.cheh-nom-2').html('Mais la décision finale est que c\'est trop risqué. ');
        }
      }
    }

    if (selectedplayer.numero == 90) {
      getMagesGroupe();
    }

    if (selectedplayer.numero == 91) {
      getMagesGroupe();
    }

    if (selectedplayer.numero == 92) {
      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
        $('.ch.cheh').remove();
      } else {
        $('.cheh').removeClass('hidden');
        $('.ch.chef').remove();
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      getMagesGroupe();
    }

    if (selectedplayer.numero == 98) {
      if (selectedplayer.objet.piment) {
        selectedplayer.objet.piment = false;
        saveGame();
        $('.pas-piment').remove();
      } else {
        $('.piment').remove();
      }
    }

    if (selectedplayer.numero == 101) {
      selectedplayer.objet.chefbandit = true;
      saveGame();
    }

    if (selectedplayer.numero == 100) {
      getGroupeMission();
    }

    if (selectedplayer.numero == 102) {
      getMagesGroupe();
    }

    if (selectedplayer.numero == 103) {
      getMagesGroupe();
      selectedplayer.objet.pluschef = true;
      saveGame();
    }

    if (selectedplayer.numero == 104) {
      getMagesGroupe();
      selectedplayer.objet.magevent = "capture";
      saveGame();
    }

    if (selectedplayer.numero == 105) {
      getMagesGroupe();
    }

    if (selectedplayer.numero == 106) {
      getMagesGroupe();
    }

    if (selectedplayer.numero == 107) {
      getMagesGroupe();
      selectedplayer.objet.chefbandit = true;
      saveGame();
    }

    if (selectedplayer.numero == 109 || selectedplayer.numero == 110) {
      getMagesGroupe();

      if (selectpersonnages.groupe.chef[0] == selectedplayer.nom) {
        $('.chef').removeClass('hidden');
        $('.ch.cheh').remove();
      } else {
        $('.cheh').removeClass('hidden');
        $('.ch.chef').remove();
        $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      }

      if (selectedplayer.objet.chefbandit) {
        $('.chefbandit').removeClass('hidden');
      }

      if (selectedplayer.objet.magevent == "capture") {
        mage1 = selectpersonnages.groupe.mission[1];
        mage2 = selectpersonnages.groupe.mission[2];
        mage3 = selectpersonnages.groupe.mission[3];
        unautre = "";

        if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie != "vent") {
          unautre = mage1;
        } else if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie != "vent") {
          unautre = mage2;
        } else {
          unautre = mage3;
        }

        $('.unautre').html(unautre);
      } else {
        mage1 = selectpersonnages.groupe.mission[1];
        $('.unautre').html(mage1);
      }
    }

    if (selectedplayer.numero == 111) {
      getMagesGroupe();

      if (selectedplayer.magie != 'feu') {
        selectedplayer.objet.magefeu = "capture";
        saveGame();
      }

      if (selectedplayer.objet.magevent == "capture") {
        mage1 = selectpersonnages.groupe.mission[1];
        mage2 = selectpersonnages.groupe.mission[2];
        mage3 = selectpersonnages.groupe.mission[3];
        unautre = "";

        if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie != "vent") {
          unautre = mage1;
        } else if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie != "vent") {
          unautre = mage2;
        } else {
          unautre = mage3;
        }

        $('.unautre').html(unautre);
      } else {
        mage1 = selectpersonnages.groupe.mission[1];
        $('.unautre').html(mage1);
      }
    }

    if (selectedplayer.numero == 112) {
      getMagesGroupe(); //if(selectedplayer.magie != 'feu'){

      selectedplayer.objet.mageeau = "blesse";
      saveGame(); //}
    }

    if (selectedplayer.numero == 113) {
      selectedplayer.toutseul = true;
      getMagesGroupe();
      saveGame();
    }

    if (selectedplayer.numero == 114 || selectedplayer.numero == 118 || selectedplayer.numero == 120 || selectedplayer.numero == 121 || selectedplayer.numero == 124) {
      insertNew(selectperso, 'Modrak');
      getMagesGroupe();

      if (selectedplayer.voile != null && selectedplayer.voile == 'vrai') {
        $(".carte-voile").removeClass('hidden');
      }

      if (selectedplayer.avantage == 'courageux') {
        selectedplayer.mensonge = true;
        saveGame();
      }

      if (selectedplayer.numero == 114 || selectedplayer.numero == 118 || selectedplayer.numero == 121) {
        selectedplayer.cascadeseul = true;
      } else {
        selectedplayer.cascadeseul = false;
      } //124 120 pas seule
      //114 118 121  seule
      //

    }

    if (selectedplayer.numero == 115) {
      getMagesGroupe();
      mage1 = selectpersonnages.groupe.mission[1];
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventmanque').remove();
      }

      if (selectedplayer.magie == 'eau') {
        if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "terre") {
          $('.dernier').html(mage1);

          if (getfillegarcon(mage1)) {
            $('.luielle').html('elle');
          } else {
            $('.luielle').html('lui');
          }
        } else if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "terre") {
          $('.dernier').html(mage2);

          if (getfillegarcon(mage2)) {
            $('.luielle').html('elle');
          } else {
            $('.luielle').html('lui');
          }
        } else if (selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "terre") {
          $('.dernier').html(mage3);

          if (getfillegarcon(mage3)) {
            $('.luielle').html('elle');
          } else {
            $('.luielle').html('lui');
          }
        }
      } else {
        if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "eau") {
          $('.dernier').html(mage1);

          if (getfillegarcon(mage1)) {
            $('.luielle').html('elle');
          } else {
            $('.luielle').html('lui');
          }
        } else if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "eau") {
          $('.dernier').html(mage2);

          if (getfillegarcon(mage2)) {
            $('.luielle').html('elle');
          } else {
            $('.luielle').html('lui');
          }
        } else if (selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "eau") {
          $('.dernier').html(mage3);

          if (getfillegarcon(mage3)) {
            $('.luielle').html('elle');
          } else {
            $('.luielle').html('lui');
          }
        }
      }
    }

    if (selectedplayer.numero == 116) {
      getMagesGroupe();
      selectedplayer.aveceau = true;

      if (selectedplayer.magie == 'terre') {
        selectedplayer.objet.mageeau = "guerie";
      }

      saveGame();
    }

    if (selectedplayer.numero == 122 || selectedplayer.numero == 123) {
      insertNew(selectperso, 'Dex');
      insertNew(selectperso, 'Terrence');

      if (selectedplayer.cascadeseul) {
        $('.sipasseule').remove();
      } else {
        $('.siseule').remove();
      }
    }

    if (selectedplayer.numero == 125) {
      getGroupeMission();
      getMagesGroupe();
      nbcap = 0;
      mage1 = selectpersonnages.groupe.mission[1];
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];
      var if1 = true;
      var if2 = true;
      var if3 = true;
      var present = [];
      var absent = [];
      var mort = [];

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventcapture').remove();
      } else {
        nbcap++;

        if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "vent") {
          if1 = false;
        }

        if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "vent") {
          if2 = false;
        }

        if (selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "vent") {
          if3 = false;
        }
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.magefeucapture').remove();
      } else {
        nbcap++;

        if (selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "feu") {
          if1 = false;
        }

        if (selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "feu") {
          if2 = false;
        }

        if (selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "feu") {
          if3 = false;
        }
      }

      if (selectedplayer.objet.mageeau != "blesse") {
        $('.mageeaublesse').remove();
      }

      if (nbcap == 2) {
        $('.capture1').remove();
      } else if (nbcap == 1) {
        $('.capture2').remove();
      } else {
        $('.capture1, .capture2').remove();
      }

      if (!if1) {
        $('.sinumero1').remove();
        absent.push(mage1);
      } else {
        present.push(mage1);
      }

      if (!if2) {
        $('.sinumero2').remove();
        absent.push(mage2);
      } else {
        present.push(mage2);
      }

      if (!if3) {
        $('.sinumero3').remove();
        absent.push(mage3);
      } else {
        present.push(mage3);
      } //console.log(selectedplayer.toutseul);


      if (selectedplayer.toutseul == true) {//    console.log('true');
      } else {
        //console.log('false');
        //$('.toutseul').remove();
        if (selectedplayer.aveceau == true) {} else {
          $('.toutseul').remove();
        }
      }

      if (selectedplayer.aveceau == true) {
        $('.toutseul2').removeClass('hidden');
      }

      if (selectedplayer.mensonge != true) {
        $('.choixSoufflardent').remove();
      }

      present.push('Estrion');
      present.push('Dex');
      present.push('Terrence');
      selectpersonnages.groupe.final = [absent, present, mort];
      saveGame();
    }

    if (selectedplayer.numero == 126 || selectedplayer.numero == 128) {
      insertNew(selectlieux, 'Kaelleg');

      if (selectpersonnages.groupe.final[1][1] == 'Estrion') {
        $('.present2').html(selectpersonnages.groupe.final[1][2]);
      } else {
        $('.present2').html(selectpersonnages.groupe.final[1][1]);
      }

      if (selectpersonnages.groupe.final[0].length > 0) {} else {
        $('.siabsent').remove();
      }
    }

    if (selectedplayer.numero == 157) {
      index = selectpersonnages.groupe.final[1].indexOf('Dex');

      if (index > -1) {
        selectpersonnages.groupe.final[1].splice(index, 1);
      }

      selectpersonnages.groupe.final[2].push('Dex');
      saveGame();

      if (selectpersonnages.groupe.final[1][0] == 'Estrion') {
        $('.sipresent').remove();
      } else {
        lesnoms = '';
        nb = 0;

        for (var i = 0; i < selectpersonnages.groupe.final[1].length; i++) {
          if (selectpersonnages.groupe.final[1][i] == 'Estrion') {
            break;
          } else {
            lesnoms += selectpersonnages.groupe.final[1][i] + ", ";
            nb++;
          }
        }

        lesnoms = lesnoms.substr(0, lesnoms.length - 2);

        if (nb == 1) {
          lesnoms += ' a';
        } else if (nb > 1) {
          lesnoms += ' ont';
        }

        $('.allnomspresents').html(lesnoms);
      } // console.log(selectcreature);
      // console.log(selectcreature.Noctule);


      if (selectcreature.Noctule) {} else {
        $('.noctule').remove();
      }
    }

    if (selectedplayer.numero == 129) {
      index = selectpersonnages.groupe.final[1].indexOf('Dex');

      if (index > -1) {
        selectpersonnages.groupe.final[1].splice(index, 1);
      }

      selectpersonnages.groupe.final[2].push('Dex');
      saveGame();

      if (selectedplayer.bataille) {
        $('.ch').not('.carteeclair').remove();
      } else {
        $('.carteeclair').remove();
      }

      if (selectpersonnages.groupe.final[1][0] == 'Estrion') {
        $('.sipresent').remove();
      } else {
        lesnoms = '';
        nb = 0;

        for (var ii = 0; ii < selectpersonnages.groupe.final[1].length; ii++) {
          if (selectpersonnages.groupe.final[1][ii] == 'Estrion') {
            break;
          } else {
            lesnoms += selectpersonnages.groupe.final[1][ii] + ", ";
            nb++;
          }
        }

        lesnoms = lesnoms.substr(0, lesnoms.length - 2);

        if (nb == 1) {
          lesnoms += ' a';
        } else if (nb > 1) {
          lesnoms += ' ont';
        }

        $('.allnomspresents').html(lesnoms);
      }
    }

    if (selectedplayer.numero == 130) {
      getGroupeMission();
      getMagesGroupe();
      nbcap = 0;

      if (selectedplayer.objet.magevent != "capture") {
        $('.simagevent').remove();
      } else {
        nbcap++;
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.simagefeu').remove();
      } else {
        nbcap++;
      }
    }

    if (selectedplayer.numero == 131) {
      selectperso.Estrion.debloque2 = true;
      index = selectpersonnages.groupe.final[1].indexOf('Estrion');

      if (index > -1) {
        selectpersonnages.groupe.final[1].splice(index, 1);
      }

      selectpersonnages.groupe.final[2].push('Estrion');
      saveGame();

      if (selectpersonnages.groupe.final[1].length <= 1) {
        $('.sipresent').remove();
      }
    }

    if (selectedplayer.numero == 132) {
      selectperso.Terrence.debloque2 = true;
      selectperso.Modrak.debloque2 = true;
      getGroupeMission();
      getMagesGroupe();

      if (selectcreature.Noctule) {} else {
        $('.noctule').remove();
      }

      if (selectpersonnages.groupe.final[1].length <= 2) {
        $('.passeul').remove();
      } else {
        $('.seul').remove();
      }

      index = selectpersonnages.groupe.final[2].indexOf('Dex');

      if (index > -1) {
        selectpersonnages.groupe.final[2].splice(index, 1);
      }

      selectpersonnages.groupe.final[1].push('Dex');
      saveGame();
    }

    if (selectedplayer.numero == 133) {
      selectperso.Terrence.debloque2 = true;
      selectperso.Modrak.debloque2 = true;
      getGroupeMission();
      getMagesGroupe();

      if (selectcreature.Noctule) {} else {
        $('.noctule').remove();
      }

      if (selectpersonnages.groupe.final[1].length <= 1) {
        $('.sipresent').remove();
      } else {
        $('.siseul').remove();
      }

      index = selectpersonnages.groupe.final[1].indexOf('Terrence');

      if (index > -1) {
        selectpersonnages.groupe.final[1].splice(index, 1);
      }

      selectpersonnages.groupe.final[0].push('Terrence');
      mage1 = selectpersonnages.groupe.mission[1];
      index2 = selectpersonnages.groupe.final[1].indexOf(mage1);

      if (index2 > -1) {
        $('.sipasgrp1-1').remove();
        selectedplayer.grp1 = true;
        selectpersonnages.groupe.final[1].splice(index2, 1);
        selectpersonnages.groupe.final[0].push(mage1);
      } else {
        $('.sigrp1-1').remove();
      }

      saveGame();
    }

    if (selectedplayer.numero == 134) {
      selectperso.Terrence.debloque3 = true;

      if (selectedplayer.energie == "verte") {
        selectperso.Modrak.debloque5 = true;
      } else {
        selectperso.Modrak.debloque3 = true;
      }

      getGroupeMission();
      getMagesGroupe();

      if (selectcreature.Noctule) {} else {
        $('.noctule').remove();
      }

      if (selectpersonnages.groupe.final[1].length <= 1) {
        $('.passeul').remove();
      } else {
        $('.seul').remove();
      }

      index = selectpersonnages.groupe.final[1].indexOf('Terrence');

      if (index > -1) {
        selectpersonnages.groupe.final[1].splice(index, 1);
      }

      selectpersonnages.groupe.final[2].push('Terrence');
      mage1 = selectpersonnages.groupe.mission[1];
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];
      nbm = 0;
      nomm = "";
      index = selectpersonnages.groupe.final[1].indexOf(mage2);

      if (index > -1) {
        nbm++;
        nomm = mage2;
      }

      index = selectpersonnages.groupe.final[1].indexOf(mage3);

      if (index > -1) {
        nbm++;
        nomm = mage3;
      }

      index = selectpersonnages.groupe.final[1].indexOf(mage1);

      if (index > -1) {
        nbm++;
        nomm = mage1;
      }

      if (nbm != 0) {
        $('.quelquun').html(nomm);
      }

      saveGame();
    }

    if (selectedplayer.numero == 135) {
      selectperso.Terrence.debloque4 = true;
      saveGame();
    }

    if (selectedplayer.numero == 136) {
      selectperso.Terrence.debloque2 = true;
      selectperso.Modrak.debloque2 = true;
      saveGame();
    }

    if (selectedplayer.numero == 135 || selectedplayer.numero == 136 || selectedplayer.numero == 140 || selectedplayer.numero == 153 || selectedplayer.numero == 154) {
      getGroupeMission();
      getMagesGroupe();

      if (selectedplayer.grp1) {
        $('.sipasgrp1-1').remove();
      } else {
        $('.sigrp1-1').remove();
      }

      nomsall = "";
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];
      nbm = 0;
      nomm = "";
      index = selectpersonnages.groupe.final[1].indexOf(mage2);

      if (index > -1) {
        nbm++;
        nomm = mage2;
        nomsall += mage2 + " et ";
      }

      index = selectpersonnages.groupe.final[1].indexOf(mage3);

      if (index > -1) {
        nbm++;
        nomm = mage3;
        nomsall += mage3 + " et ";
      }

      if (nbm == 0) {
        $('.sigrpautre').remove();
      } else {
        $('.unautre').html(nomm);
      }

      nomsall = nomsall.substr(0, nomsall.length - 4);
      $('.nomsall').html(nomsall);
    }

    if (selectedplayer.numero == 137) {
      if (selectpersonnages.groupe.final[1].length <= 0) {
        $('.seul').remove();
      } else {
        $('.passeul').remove();
      }

      selectedplayer.fin1 = 1;
      saveGame();
      $('.finchapitre').off().on('click', function () {
        // finchapitre2
        selectedplayer.chapitrenum = 'chapitre2';
        selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
        selectedplayer.numero = 1;
        saveGame();
        $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
          binding();
          $("body, .num, main, html").scrollTop(0);
          $(window).scrollTop(0);
          $(document).scrollTop(0);
        });
      });
    }

    if (selectedplayer.numero == 138) {
      getGroupeMission();
      getMagesGroupe();

      if (selectedplayer.grp1) {
        $('.sipasgrp1-1').remove();
      } else {
        $('.sigrp1-1').remove();
      }

      nbcap = 0;

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventcapture').remove();
      } else {
        nbcap++;
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.magefeucapture').remove();
      } else {
        nbcap++;
      }

      if (nbcap == 2) {
        $('.capture1').remove();
      } else if (nbcap == 1) {
        $('.capture2').remove();
      } else {
        $('.capture1, .capture2').remove();
      }

      selectedplayer.fin1 = 2;
      saveGame();
      $('.finchapitre').off().on('click', function () {
        // finchapitre2
        selectedplayer.chapitrenum = 'chapitre2';
        selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
        selectedplayer.numero = 1;
        saveGame();
        $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
          binding();
          $("body, .num, main, html").scrollTop(0);
          $(window).scrollTop(0);
          $(document).scrollTop(0);
        });
      });
    }

    if (selectedplayer.numero == 139) {
      getGroupeMission();
      getMagesGroupe();

      if (selectcreature.Noctule) {} else {
        $('.noctule').remove();
      }

      if (selectpersonnages.groupe.final[1].length <= 0) {
        $('.passeul').remove();
      } else {
        $('.seul').remove();
      }

      mage1 = selectpersonnages.groupe.mission[1];
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];
      nbm = 0;
      nomm = "";
      index = selectpersonnages.groupe.final[1].indexOf(mage2);

      if (index > -1) {
        nbm++;
        nomm = mage2;
      }

      index = selectpersonnages.groupe.final[1].indexOf(mage3);

      if (index > -1) {
        nbm++;
        nomm = mage3;
      }

      index = selectpersonnages.groupe.final[1].indexOf(mage1);

      if (index > -1) {
        nbm++;
        nomm = mage1;
      }

      if (nbm != 0) {
        $('.quelquun').html(nomm);
      }

      if (selectedplayer.trahison == 'vrai') {} else {
        $('.trahison').remove();
      }

      mage1 = selectpersonnages.groupe.mission[1];
      index2 = selectpersonnages.groupe.final[1].indexOf(mage1);

      if (index2 > -1) {
        $('.sipasgrp1-1').remove();
        selectedplayer.grp1 = true;
        selectpersonnages.groupe.final[1].splice(index2, 1);
        selectpersonnages.groupe.final[0].push(mage1);
      } else {
        $('.sigrp1-1').remove();
      }

      saveGame();
    }

    if (selectedplayer.numero == 141) {
      getGroupeMission();
      getMagesGroupe();

      if (selectedplayer.grp1) {
        $('.sipasgrp1-1').remove();
      } else {
        $('.sigrp1-1').remove();
      }

      nomsall = "";
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];
      nbm = 0;
      nomm = "";
      index = selectpersonnages.groupe.final[1].indexOf(mage2);

      if (index > -1) {
        nbm++;
        nomm = mage2;
        nomsall += mage2 + " et ";
      }

      index = selectpersonnages.groupe.final[1].indexOf(mage3);

      if (index > -1) {
        nbm++;
        nomm = mage3;
        nomsall += mage3 + " et ";
      }

      if (nbm == 0) {
        $('.sigrpautre').remove();
      } else {
        $('.unautre').html(nomm);
      }

      nomsall = nomsall.substr(0, nomsall.length - 4);
      $('.nomsall').html(nomsall);

      if (selectedplayer.energie == "verte") {
        selectedplayer.capturemodrak = true;
      }

      saveGame();
    }

    if (selectedplayer.numero == 142) {
      getGroupeMission();
      getMagesGroupe();

      if (selectedplayer.grp1) {
        $('.sipasgrp1-1').remove();
      } else {
        $('.sigrp1-1').remove();
      }

      nbcap = 0;

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventcapture').remove();
      } else {
        nbcap++;
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.magefeucapture').remove();
      } else {
        nbcap++;
      }

      if (nbcap == 2) {
        $('.capture1').remove();
      } else if (nbcap == 1) {
        $('.capture2').remove();
      } else {
        $('.capture1, .capture2').remove();
      }

      selectedplayer.fin1 = 3;
      saveGame();
      $('.finchapitre').off().on('click', function () {
        // finchapitre2
        selectedplayer.chapitrenum = 'chapitre2';
        selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
        selectedplayer.numero = 1;
        saveGame();
        $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
          binding();
          $("body, .num, main, html").scrollTop(0);
          $(window).scrollTop(0);
          $(document).scrollTop(0);
        });
      });
    }

    if (selectedplayer.numero == 143) {
      nbcap = 0;

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventcapture').remove();
      } else {
        nbcap++;
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.magefeucapture').remove();
      } else {
        nbcap++;
      }

      if (nbcap == 2) {
        $('.capture1').remove();
      } else if (nbcap == 1) {
        $('.capture2').remove();
      } else {
        $('.capture1, .capture2').remove();
      }

      selectedplayer.fin1 = 4;
      saveGame();
      $('.finchapitre').off().on('click', function () {
        // finchapitre2
        selectedplayer.chapitrenum = 'chapitre2';
        selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
        selectedplayer.numero = 1;
        saveGame();
        $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
          binding();
          $("body, .num, main, html").scrollTop(0);
          $(window).scrollTop(0);
          $(document).scrollTop(0);
        });
      });
    }

    if (selectedplayer.numero == 144) {
      getGroupeMission();
      getMagesGroupe();
      var mageventexist = false;

      if (selectedplayer.objet.magevent != "capture") {
        $('.sipasmagevent').remove();
        mageventexist = true;
      } else {
        $('.simagevent').remove();
      }

      if (selectedplayer.magie == 'vent') {
        mageventexist = true;
      }

      if (mageventexist) {
        $('.ch.infopasmagie').remove();
      } else {
        $('.ch.infomagie').remove();
      } //selectedplayer.fin1 = 4;


      saveGame();
    }

    if (selectedplayer.numero == 145) {
      getGroupeMission();
      getMagesGroupe();

      if (selectedplayer.magie == 'vent' && selectedplayer.avantage == 'populaire') {
        $('.autre.ch').remove();
      } else {
        $('.infomagepop.ch').remove();
      }
    }

    if (selectedplayer.numero == 147 || selectedplayer.numero == 152) {
      getGroupeMission();
      getMagesGroupe();
      nbcap = 0;

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventcapture').remove();
      } else {
        nbcap++;
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.magefeucapture').remove();
      } else {
        nbcap++;
      }
    }

    if (selectedplayer.numero == 148 || selectedplayer.numero == 149) {
      getGroupeMission();
      getMagesGroupe();
    }

    if (selectedplayer.numero == 150) {
      selectperso.Estrion.debloque3 = true;
      getGroupeMission();
      getMagesGroupe();

      if (selectcreature.Noctule) {} else {
        $('.noctule').remove();
      }
    }

    if (selectedplayer.numero == 157) {
      selectperso.Modrak.debloque2 = true;
    }

    if (selectedplayer.numero == 153) {
      selectperso.Terrence.debloque4 = true;
      saveGame();
    }

    if (selectedplayer.numero == 154) {
      selectperso.Terrence.debloque2 = true;
      saveGame();
    }

    if (selectedplayer.numero == 151) {
      selectperso.Estrion.debloque3 = true;
      selectedplayer.fin1 = 5;
      saveGame();
      $('.finchapitre').off().on('click', function () {
        // finchapitre2
        selectedplayer.chapitrenum = 'chapitre2';
        selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
        selectedplayer.numero = 1;
        saveGame();
        $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
          binding();
          $("body, .num, main, html").scrollTop(0);
          $(window).scrollTop(0);
          $(document).scrollTop(0);
        });
      });
    }

    if (selectedplayer.numero == 155 || selectedplayer.numero == 156) {
      getGroupeMission();
      getMagesGroupe();
      mage1 = selectpersonnages.groupe.mission[1];
      index2 = selectpersonnages.groupe.final[1].indexOf(mage1);

      if (index2 > -1) {
        $('.sipasgrp1-1').remove();
        selectedplayer.grp1 = true;
      } else {
        $('.sigrp1-1').remove();
        selectperso.Estrion.debloque4 = true;
      }

      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];
      nbm = 0;
      nomm = "";
      index = selectpersonnages.groupe.final[1].indexOf(mage2);

      if (index > -1) {
        nbm++;
        nomm = mage2;
      }

      index = selectpersonnages.groupe.final[1].indexOf(mage3);

      if (index > -1) {
        nbm++;
        nomm = mage3;
      }

      if (nbm == 0) {
        $('.siautre').remove();
      } else {
        $('.unautre').html(nomm);
      }

      saveGame();
    }

    if (selectedplayer.numero == 159) {
      getGroupeMission();
      getMagesGroupe();
      mage1 = selectpersonnages.groupe.mission[1];
      index2 = selectpersonnages.groupe.final[1].indexOf(mage1);

      if (index2 > -1) {
        $('.sipasgrp1-1').remove();
        selectedplayer.grp1 = true;
        selectedplayer.grp1disparait = true;
      } else {
        $('.sigrp1-1').remove();
      }

      nbcap = 0;

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventcapture').remove();
      } else {
        nbcap++;
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.magefeucapture').remove();
      } else {
        nbcap++;
      }

      if (nbcap == 0) {
        $('.sicapture').remove();
      }

      saveGame();
    }

    if (selectedplayer.numero == 160) {
      getGroupeMission();
      getMagesGroupe();
      mage1 = selectpersonnages.groupe.mission[1];
      index2 = selectpersonnages.groupe.final[1].indexOf(mage1);

      if (index2 > -1) {
        $('.sipasgrp1-1').remove();
        selectedplayer.grp1 = true;
      } else {
        $('.sigrp1-1').remove();
      }

      nbcap = 0;

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventcapture').remove();
      } else {
        nbcap++;
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.magefeucapture').remove();
      } else {
        nbcap++;
      }

      if (nbcap == 2) {
        $('.capture1').remove();
      } else if (nbcap == 1) {
        $('.capture2').remove();
      } else {
        $('.capture1, .capture2').remove();
      }

      saveGame();
    }

    if (selectedplayer.numero == 161) {
      selectperso.Modrak.debloque4 = true;
      getGroupeMission();
      getMagesGroupe();

      if (selectedplayer.grp1disparait) {} else {
        $('.sigrp1-1').remove();
      }

      selectedplayer.fin1 = 6;
      saveGame();
      $('.finchapitre').off().on('click', function () {
        // finchapitre2
        selectedplayer.chapitrenum = 'chapitre2';
        selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
        selectedplayer.numero = 1;
        saveGame();
        $("main").load("template/" + selectedplayer.livrenum + "/" + selectedplayer.chapitrenum + "/" + selectedplayer.numero + ".html", function () {
          binding();
          $("body, .num, main, html").scrollTop(0);
          $(window).scrollTop(0);
          $(document).scrollTop(0);
        });
      });
    }

    if (selectedplayer.numero == 162) {
      nbcap = 0;

      if (selectedplayer.objet.magevent != "capture") {
        $('.mageventcapture').remove();
      } else {
        nbcap++;
      }

      if (selectedplayer.objet.magefeu != "capture") {
        $('.magefeucapture').remove();
      } else {
        nbcap++;
      }

      if (nbcap == 0) {
        $('.capture').remove();
      }
    } //** FIN NUMEROS ***/

  } else if (selectedplayer.chapitrenum == 'chapitre2') {
    goActionChapter2();
  } else if (selectedplayer.chapitrenum == 'chapitre3') {
    goActionChapter3();
  } // FIN CHAPITRE 1


  if (selectedplayer.debloqueinfo != undefined && selectedplayer.debloqueinfo != null) {
    if (selectedplayer.debloqueinfo == 'vrai') {
      $('footer .infos').show();
    }
  }
}