<?php
$db_config['host'] = "localhost";
$db_config['db'] = "jeux";
$db_config['user'] = "root";
$db_config['pass'] = "root";

global $conn;

try {
  $conn = new PDO("mysql:host=".$db_config['host'].";dbname=".$db_config['db']."", $db_config['user'], $db_config['pass']);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}	catch(PDOException $e){
   echo "Connection failed: " . $e->getMessage();
}
?>
