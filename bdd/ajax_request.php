<?php
// include("config.php");
// include("functions.php");
$db_config['host'] = "localhost";
$db_config['db'] = "jeux";
$db_config['user'] = "root";
$db_config['pass'] = "root";

global $conn;

try {
  $conn = new PDO("mysql:host=".$db_config['host'].";dbname=".$db_config['db']."", $db_config['user'], $db_config['pass']);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //echo 'connected';
}	catch(PDOException $e){
   echo "Connection failed: " . $e->getMessage();
}

function joueur_init($idjoueur){
  // Vérifie que le joueur existe
  $req = "SELECT * FROM `joueur` WHERE profile_google = $idjoueur";
  if(isset($conn)){
    $resultats=$conn->query($req);
    $resultats->setFetchMode(PDO::FETCH_OBJ);
    $row = [];
    while( $resultat = $resultats->fetch() ) {
      array_push($row, $resultat);
    }

    // S'il n'existe pas le créé
    if(count($row) == 0){
      $req = "INSERT INTO  `joueur` (profile_google, 	avatar) VALUES ( $idjoueur, 'male/06.png')";
      $resultats=$conn->prepare($req);
      $resultats->execute();
      $last_id = $conn->lastInsertId();
      $req = $last_id;
      $resultatinsert = (object) ['ID_joueur' => $req, 'profile_google' => $req, 'last_connect' => time(), 'avatar' => 'male/06.png'];
      array_push($row, $resultatinsert);
    }

    return  $row;
  } else {
    var_dump($conn);
  }
}

if(isset($_POST)){
  if(isset($_POST['func']) AND $_POST['func'] == "checkgoogleid"){

    $thejoueur;

    if(isset($_POST["gid"]) AND $_POST["gid"] != 'undefined' AND $_POST["gid"] != null){
      $thejoueur = joueur_init($_POST["gid"]);
    } else {
      $thejoueur = "error";
    }

    echo json_encode(array("joueur"=>$thejoueur));
  }
}

//var_dump($conn);

?>
