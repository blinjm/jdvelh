// Include gulp
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var babel = require('gulp-babel');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var cleanCSS = require('gulp-clean-css');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(concat('all.css'))
        .pipe(gulp.dest('dist'))
        .pipe(cleanCSS())
        .pipe(rename('all.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src([
        'js/*.js'])
        .pipe(babel())
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    browserSync.init({
      files: ['*.htm', '*.html'],
      server: {
        baseDir: "./"
      },
      options: {
        reloadDelay: 250
      },
      notify: false
    });

    gulp.watch('js/*.js', ['lint', 'scripts']);
    gulp.watch('scss/common/*.scss', ['sass']);
    gulp.watch('scss/module/*.scss', ['sass']);
    gulp.watch('scss/*.scss', ['sass']);

    // gulp.watch('js/*.js', gulp.series('lint', 'scripts'));
    // gulp.watch('scss/*.scss', gulp.series('sass'));
    // gulp.watch('scss/*/*.scss', gulp.series('sass'));
    // gulp.watch('scss/*/*/*.scss', gulp.series('sass'));
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);
// gulp.task('default', gulp.series('lint', 'sass', 'scripts', 'watch'));
