<?php 

if(isset($_POST)){
	if(isset($_POST['func']) AND $_POST['func'] == 'export'){
		$nom = $_POST['nom']; 

		$dataperso = $_POST['dataperso']; 
		$fpperso = fopen('Export_perso_'.$nom.'.json', 'w');
		fwrite($fpperso, json_encode($dataperso));
		fclose($fpperso);

		$datalieux = $_POST['datalieux']; 
		$fplieux = fopen('Export_lieux_'.$nom.'.json', 'w');
		fwrite($fplieux, json_encode($datalieux));
		fclose($fplieux);

		$datacreature = $_POST['datacreature']; 
		$fpcrea = fopen('Export_crea_'.$nom.'.json', 'w');
		fwrite($fpcrea, json_encode($datacreature));
		fclose($fpcrea);

		$datapersonnages = $_POST['datapersonnages']; 
		$fppersonnages = fopen('Export_personnages_'.$nom.'.json', 'w');
		fwrite($fppersonnages, json_encode($datapersonnages));
		fclose($fppersonnages);

		$datasort = $_POST['datasort']; 
		$fpsort = fopen('Export_sort_'.$nom.'.json', 'w');
		fwrite($fpsort, json_encode($datasort));
		fclose($fpsort);

		$dataplayer = $_POST['dataplayer']; 
		$fpplayer = fopen('Export_player_'.$nom.'.json', 'w');
		fwrite($fpplayer, json_encode($dataplayer));
		fclose($fpplayer);
	}
	if(isset($_POST['func']) AND $_POST['func'] == 'import'){
		echo 'test';
	}
	if(isset($_POST['func']) AND $_POST['func'] == 'liste'){
		$html = '';
		$htmldeja = [];
		if ($handle = opendir('.')) {

			while (false !== ($entry = readdir($handle))) {

				if ($entry != "." && $entry != "..") {
					
					$explode1 = explode('.', $entry);
					$explosion = explode('_', $explode1[0]);
					if(count($explosion) > 2){
						if(!in_array($explosion[2], $htmldeja)){
							$htmldeja[] = $explosion[2];
							
							$html .= "<option value='". $explosion[2] ."' >". $explosion[2] ."</option>";
						}
					}
					// echo "$entry\n";
				}
			}
			closedir($handle);
		}
		echo $html;
	}
}

?>