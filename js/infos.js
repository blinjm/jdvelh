function getinfo(){

  $('#listeinfo button').off().on('click', function(){
    var url = $(this).attr('data-info');
     $( "main" ).load( url, function() {
  		 if(url =="template/infos/persos/index.html")
  			getpersos();
  		 if(url =="template/infos/lieux/index.html")
  			getlieux();
  		 if(url =="template/infos/creatures/index.html")
  			getcreatures();
   		 if(url =="template/infos/carte/index.html")
   			getcarte();
     });
  });

  $('.Export').off().on('click', function(){
    saveonline();
  });

}

function getcarte(){
    var multi = 5;
    $('.plussize').off().on('click', function(){
      multi++;
      if(multi > 9){
        multi = 9;
      }
      $('.infoss-map img').width(multi*100+"%");
    });
    $('.moinssize').off().on('click', function(){
      multi--;
      if(multi < 1){
        multi = 1;
      }
      $('.infoss-map img').width(multi*100+"%");
    });
}

function saveonline(){
    var myperso = JSON.stringify(selectperso);
    var mylieux = JSON.stringify(selectlieux);
    var mycreatures = JSON.stringify(selectcreature);
    var mypersonnages = JSON.stringify(selectpersonnages);
    var mysorts = JSON.stringify(selectsorts);
    var myplayer = JSON.stringify(selectedplayer);
    $.post( "export/export.php", { func: "export", dataperso: myperso, datalieux: mylieux, datacreature: mycreatures, datapersonnages: mypersonnages, datasort: mysorts, dataplayer: myplayer, nom: selectedplayer.nom }, function( data ) {
      $('.Export').text('SAUVEGARDE FAITE');
    });
}

function getpersos(){
    $('#infoss label').on('click', function(){
      $(this).toggleClass('open');
      $(this).parent().find('.contenu').slideToggle(200);
    });

    if(selectperso != null && selectperso != undefined){
      for(var key in selectperso){
        $('[data-perso="'+key+'"]').removeClass('hidden');
      }

      if(selectperso.Espéride && selectperso.Espéride.debloque){
        if(selectperso.Espéride.triche){
          $('[data-perso="Espéride"] .phrase2').removeClass('hidden');
        }
      }
      if(selectperso.Lasseni && selectperso.Lasseni.debloque){
        $('[data-perso="Lasseni"] .place').html(selectpersonnages.lasseni.place);
        $('[data-perso="Lasseni"] .magie').html(selectpersonnages.lasseni.magie);
      }
      if(selectperso.Panacle && selectperso.Panacle.debloque){
        $('[data-perso="Panacle"] .place').html(selectpersonnages.panacle.place);
        $('[data-perso="Panacle"] .magie').html(selectpersonnages.panacle.magie);
      }
      if(selectperso.Haziko && selectperso.Haziko.debloque){
        $('[data-perso="Haziko"] .place').html(selectpersonnages.haziko.place);
        $('[data-perso="Haziko"] .magie').html(selectpersonnages.haziko.magie);
      }
      if(selectperso.Anselm && selectperso.Anselm.debloque){
        $('[data-perso="Anselm"] .place').html(selectpersonnages.anselm.place);
        $('[data-perso="Anselm"] .magie').html(selectpersonnages.anselm.magie);
      }
      if(selectperso.MacLumy && selectperso.MacLumy.debloque){
        $('[data-perso="MacLumy"] .place').html(selectpersonnages.macLumy.place);
        $('[data-perso="MacLumy"] .magie').html(selectpersonnages.macLumy.magie);
        if(selectperso.MacLumy.debloque2){
          $('[data-perso="MacLumy"] .phrase2').removeClass('hidden');
        }
      }
      if(selectperso.Kannan && selectperso.Kannan.debloque){
        $('[data-perso="Kannan"] .place').html(selectpersonnages.kannan.place);
        $('[data-perso="Kannan"] .magie').html(selectpersonnages.kannan.magie);
        if(selectperso.Kannan.debloque2){
          $('[data-perso="Kannan"] .phrase2').removeClass('hidden');
        }
      }
      if(selectperso.Jayne && selectperso.Jayne.debloque){
        $('[data-perso="Jayne"] .place').html(selectpersonnages.jayne.place);
        $('[data-perso="Jayne"] .magie').html(selectpersonnages.jayne.magie);
      }
      if(selectperso.Jayn && selectperso.Jayn.debloque){
        $('[data-perso="Jayn"] .place').html(selectpersonnages.jayn.place);
        $('[data-perso="Jayn"] .magie').html(selectpersonnages.jayn.magie);
      }

      if(selectperso.Modrak && selectperso.Modrak.debloque2){
        $('[data-perso="Modrak"] .phrase2').removeClass('hidden');
      }
      if(selectperso.Modrak && selectperso.Modrak.debloque3){
        $('[data-perso="Modrak"] .phrase3').removeClass('hidden');
      }
      if(selectperso.Modrak && selectperso.Modrak.debloque4){
        $('[data-perso="Modrak"] .phrase4').removeClass('hidden');
      }
      if(selectperso.Modrak && selectperso.Modrak.debloque5){
        $('[data-perso="Modrak"] .phrase5').removeClass('hidden');
      }
      if(selectperso.Estrion && selectperso.Estrion.debloque2){
        $('[data-perso="Estrion"] .phrase2').removeClass('hidden');
      }
      if(selectperso.Estrion && selectperso.Estrion.debloque3){
        $('[data-perso="Estrion"] .phrase3').removeClass('hidden');
      }
      if(selectperso.Estrion && selectperso.Estrion.debloque4){
        $('[data-perso="Estrion"] .phrase4').removeClass('hidden');
      }
      if(selectperso.Terrence && selectperso.Terrence.debloque2){
        $('[data-perso="Terrence"] .phrase2').removeClass('hidden');
      }
      if(selectperso.Terrence && selectperso.Terrence.debloque3){
        $('[data-perso="Terrence"] .phrase3').removeClass('hidden');
      }
      if(selectperso.Terrence && selectperso.Terrence.debloque4){
        $('[data-perso="Terrence"] .phrase4').removeClass('hidden');
      }
      if(selectperso.Konakey && selectperso.Konakey.d2){
        $('[data-perso="Konakey"] .phrase2').removeClass('hidden');
      }
      if(selectperso.Imp && selectperso.Imp.d2){
        $('[data-perso="Imp"] .phrase2').removeClass('hidden');
      }
      if(selectperso.Imp && selectperso.Imp.d3){
        $('[data-perso="Imp"] .phrase3').removeClass('hidden');
      }

    }
    if(persosmort != null && persosmort != undefined){
      for(var key2 in persosmort){
        $('[data-persomort="'+key2+'"]').removeClass('hidden');
      }
    }
}

function getlieux(){
    $('#infoss label').on('click', function(){
      $(this).toggleClass('open');
      $(this).parent().find('.contenu').slideToggle(200);
    });

    if(selectlieux != null && selectlieux != undefined){
      for(var key in selectlieux){
        $('[data-lieux="'+key+'"]').removeClass('hidden');
      }
    }
}

function getcreatures(){
    $('#infoss label').on('click', function(){
      $(this).toggleClass('open');
      $(this).parent().find('.contenu').slideToggle(200);
    });
    console.log(selectcreature);
    if(selectcreature != null && selectcreature != undefined){
      for(var key in selectcreature){
        $('[data-creature="'+key+'"]').removeClass('hidden');
      }
    }
}
