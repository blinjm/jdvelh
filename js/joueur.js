var current_modif_joueur = 0;

function initperso(){
    trad(lang);
    var joueur_new = [];
    // SEXE
    $('.img-fille, .img-garcon').on('click', function(){
      $('.popin').attr('data-sexe',$(this).attr('data-img')).show();

      $('.wrapperpopin .oui').off().on('click', function(){
        joueur_new.sexe = $('.popin').attr('data-sexe');
        $('.popin').removeAttr('data-sexe').hide();
        $('.part1').hide();
        $('.part2').show();
      });
      $('.wrapperpopin .non').off().on('click', function(){
        $('.popin').removeAttr('data-sexe').hide();
      });
    });

    // NOM
    $('#validnom').off().on('click', function(){
      $('.popin .plus').html('Vous vous appelez donc bien: '+$('#monnom').val()+' ?').show();
      $('.popin').attr('data-nom',$('#monnom').val()).show();

      $('.wrapperpopin .oui').off().on('click', function(){
        joueur_new.nom = $('.popin').attr('data-nom');
        $('.popin').removeAttr('data-nom').hide();
        $('.part2').hide();
        $('.part3').show();
      });
      $('.wrapperpopin .non').off().on('click', function(){
        $('.popin').removeAttr('data-nom').hide();
      });
    });

    // AVANTAGE
    $('.part3 button').off().on('click', function(){
      $('.popin').attr('data-av',$(this).attr('data-value')).show();
      $('.popin .plus').html($('.part3 p[data-value="'+$(this).attr('data-value')+'"]')).show();
      $('.wrapperpopin .oui').off().on('click', function(){
        joueur_new.avantage = $('.popin').attr('data-av');
        $('.popin').removeAttr('data-av').hide();
        $('.part3').hide();
        $('.part4').show();
        if(joueur_new.avantage == "studieux"){
          $('.tricheur').hide();
        }
        if(joueur_new.avantage == "courageux"){
          $('.peureux').hide();
        }
        if(joueur_new.avantage == "sportif"){
          $('.cache-sportif').show();
        }
      });
      $('.wrapperpopin .non').off().on('click', function(){
        $('.popin').removeAttr('data-av').hide();
        $('.popin .plus').html('');
      });
    });

    // DEFAUT
    $('.part4 button').off().on('click', function(){
      $('.popin').attr('data-def',$(this).attr('data-value')).show();
      $('.popin .plus').html($('.part4 p[data-value="'+$(this).attr('data-value')+'"]')).show();
      $('.wrapperpopin .oui').off().on('click', function(){
        joueur_new.defaut = $('.popin').attr('data-def');
        $('.popin').removeAttr('data-def').hide();
        $('.part4').hide();


        if (typeof(Storage) !== "undefined") {
          var auj = new Date();
          if(joueur_saved == null){
            joueur_saved = jQuery.parseJSON('{ "joueur1":{ "date": "'+auj+'"} }');
            joueur_saved.joueur1.nom = joueur_new.nom;
            joueur_saved.joueur1.sexe = joueur_new.sexe;
            joueur_saved.joueur1.avantage = joueur_new.avantage;
            joueur_saved.joueur1.defaut = joueur_new.defaut;
            joueur_saved.joueur1.livre = '';
            joueur_saved.joueur1.livrenum = '';
            joueur_saved.joueur1.chapitre = '';
            joueur_saved.joueur1.chapitrenum = '';
            joueur_saved.joueur1.numero = 0;
            joueur_saved.joueur1.objet = {};
            localStorage.setItem("joueurs", JSON.stringify(joueur_saved));
          } else {
            var nb_obj = (Object.keys(joueur_saved).length);
            if(joueur_saved['joueur'+(nb_obj+1)] == null){
              joueur_saved['joueur'+(nb_obj+1)] = {};
              joueur_saved['joueur'+(nb_obj+1)].date = auj;
              joueur_saved['joueur'+(nb_obj+1)].nom = joueur_new.nom;
              joueur_saved['joueur'+(nb_obj+1)].sexe = joueur_new.sexe;
              joueur_saved['joueur'+(nb_obj+1)].avantage = joueur_new.avantage;
              joueur_saved['joueur'+(nb_obj+1)].defaut = joueur_new.defaut;
              joueur_saved['joueur'+(nb_obj+1)].livre = '';
              joueur_saved['joueur'+(nb_obj+1)].livrenum = '';
              joueur_saved['joueur'+(nb_obj+1)].chapitre = '';
              joueur_saved['joueur'+(nb_obj+1)].chapitrenum = '';
              joueur_saved['joueur'+(nb_obj+1)].numero = 0;
              joueur_saved['joueur'+(nb_obj+1)].objet = {};
              localStorage.setItem("joueurs", JSON.stringify(joueur_saved));
            }

          }
        }

        $( "main" ).load( "template/creer/affichage.html", function() {
          getPerso();
        });

      });
      $('.wrapperpopin .non').off().on('click', function(){
        $('.popin').removeAttr('data-def').hide();
        $('.popin .plus').html('');
      });
    });

}

function getPerso(){
  trad(lang);
  var html = "";
  var nb_obj;
  var my_width;


  if(joueur_saved != null){

    // AFFICHAGE DES PERSOS ENREGISTRES
    nb_obj = (Object.keys(joueur_saved).length);
    for(var keys in joueur_saved){
      html += '<div class="fiche fiche-'+joueur_saved[keys].sexe+' fiche-'+joueur_saved[keys].livrenum+' fiche-'+joueur_saved[keys].chapitrenum+'" data-id="'+keys+'">';
      html += '<div class="nom">'+joueur_saved[keys].nom+'</div>';
      html += '<p class="livre">'+joueur_saved[keys].livre+'</p>';
      html += '<p class="chapitre">'+joueur_saved[keys].chapitre+'</p>';
      html += '<button class="continuerPartie">Continuer</button>';
      html += '</div>';
    }

    // AJUSTEMENT DU CAROUSSEL
    my_width = $('.allpersos').width() + 40;
    $('.allpersos').width(my_width*nb_obj);

    if(nb_obj == 1){
      $('.previous_perso, .next_perso').addClass('disabled');
    } else if(nb_obj > 1){
      $('.previous_perso').addClass('disabled');
    } else {
      $('.select_perso, .previous_perso, .next_perso').hide();
    }

    var current_pos = 1;
    $('.previous_perso').on('click', function(){
      if(!$(this).hasClass('disabled')){
        current_pos--;
        if(current_pos == 1){
          $(this).addClass('disabled');
        }
        $('.next_perso').removeClass('disabled');
        $('.allpersos').animate({
          'margin-left': '+='+my_width+'px'
        }, 500, function(){});
      }
    });

    $('.next_perso').on('click', function(){
      if(!$(this).hasClass('disabled')){
        current_pos++;
        if(current_pos == nb_obj){
          $(this).addClass('disabled');
        }
        $('.previous_perso').removeClass('disabled');
        $('.allpersos').animate({
          'margin-left': '-='+my_width+'px'
        }, 500, function(){});
      }
    });

    // BIND DE LA SELECTION DE PERSO
    $('.select_perso').on('click', function(){
      current_modif_joueur = current_pos;
       $( "main" ).load( "template/creer/modifier.html", function() {
         modifperso();
       });
    });
  } else {
    $('.select_perso, .previous_perso, .next_perso').hide();
  }

  $('.allpersos').html(html);
  $('.allpersos .fiche').width((my_width*nb_obj) / nb_obj);

  // BIND CREATION NEW PERSO
  $('#creer').on('click', function(){
     $( "main" ).load( "template/creer/perso.html", function() {
       initperso();
     });
  });

  // CLICK SUR LA FICHE - bouton continuer
  $('.fiche').on('click', function(){
     var joueur_temp = ($(this).attr('data-id'));

     selectedplayer = joueur_saved[""+joueur_temp];
     selectperso = persos[""+joueur_temp];
     selectlieux = lieux[""+joueur_temp];
     selectcreature = creatures[""+joueur_temp];
     selectpersonnages = personnages[""+joueur_temp];
     selectsorts = sorts[""+joueur_temp];

     if(selectedplayer.livre.length == 0){
       $('footer .livre').trigger('click');
     } else {
         $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
           binding();
         });
     }

  });
}


function modifperso(){
  var mymod;
  mymod = joueur_saved["joueur"+current_modif_joueur];
  $('.modif_nom input').val(mymod.nom);
  if(mymod.sexe == 'fille'){
    $('.portrait').addClass('fille');
  } else {
    $('.portrait').addClass('garcon');
  }

  $('.infos_av').html('<b>Avantage</b><br />'+mymod.avantage);
  $('.infos_def').html('<b>Défaut</b><br />'+mymod.defaut);
  $('.infos_livre').html('<b>Livre</b><br />'+mymod.livre);
  $('.infos_chapitre').html('<b>Chapitre</b><br />'+mymod.chapitre);

  $('.energie').html('<b>Energie</b> : '+mymod.energie);
  $('.magie').html('<b>Magie favorite</b> : '+mymod.magie);
  var niveau = 'Première année';
  if(mymod.chapitrenum == 2){
    niveau = 'Troisième année';
  }
  $('.niveaumagie').html('<b>Niveau de magie</b> : '+niveau);

}
