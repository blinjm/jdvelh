function getMagesGroupe(){
  var mage1 = selectpersonnages.groupe.mission[1];
  var mage2 = selectpersonnages.groupe.mission[2];
  var mage3 = selectpersonnages.groupe.mission[3];

  if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "vent"){
    $('.mage-vent').html(mage1);
    if(getfillegarcon(mage1)){
      $('.mage-vent-garcon').remove();
    } else {
      $('.mage-vent-fille').remove();
    }
  } else if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "vent"){
    $('.mage-vent').html(mage2);
    if(getfillegarcon(mage2)){
      $('.mage-vent-garcon').remove();
    } else {
      $('.mage-vent-fille').remove();
    }
  } else if(selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "vent"){
    $('.mage-vent').html(mage3);
    if(getfillegarcon(mage3)){
      $('.mage-vent-garcon').remove();
    } else {
      $('.mage-vent-fille').remove();
    }
  }
  if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "eau"){
    $('.mage-eau').html(mage1);
    if(getfillegarcon(mage1)){
      $('.mage-eau-garcon').remove();
    } else {
      $('.mage-eau-fille').remove();
    }
  } else if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "eau"){
    $('.mage-eau').html(mage2);
    if(getfillegarcon(mage2)){
      $('.mage-eau-garcon').remove();
    } else {
      $('.mage-eau-fille').remove();
    }
  } else if(selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "eau"){
    $('.mage-eau').html(mage3);
    if(getfillegarcon(mage3)){
      $('.mage-eau-garcon').remove();
    } else {
      $('.mage-eau-fille').remove();
    }
  }
  if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "feu"){
    $('.mage-feu').html(mage1);
    if(getfillegarcon(mage1)){
      $('.mage-feu-garcon').remove();
    } else {
      $('.mage-feu-fille').remove();
    }
  } else if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "feu"){
    $('.mage-feu').html(mage2);
    if(getfillegarcon(mage2)){
      $('.mage-feu-garcon').remove();
    } else {
      $('.mage-feu-fille').remove();
    }
  } else if(selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "feu"){
    $('.mage-feu').html(mage3);
    if(getfillegarcon(mage3)){
      $('.mage-feu-garcon').remove();
    } else {
      $('.mage-feu-fille').remove();
    }
  }
  if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "terre"){
    $('.mage-terre').html(mage1);
    if(getfillegarcon(mage1)){
      $('.mage-terre-garcon').remove();
    } else {
      $('.mage-terre-fille').remove();
    }
  } else if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "terre"){
    $('.mage-terre').html(mage2);
    if(getfillegarcon(mage2)){
      $('.mage-terre-garcon').remove();
    } else {
      $('.mage-terre-fille').remove();
    }
  } else if(selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "terre"){
    $('.mage-terre').html(mage3);
    if(getfillegarcon(mage3)){
      $('.mage-terre-garcon').remove();
    } else {
      $('.mage-terre-fille').remove();
    }
  }
}
function getfillegarcon(nom){
  var fille = false;
  if(nom.toLowerCase() == 'kannan' || nom.toLowerCase() == 'haziko' || nom.toLowerCase() == 'jayne' || nom.toLowerCase() == 'lasseni'){
    fille = true;
  }
  return fille;
}

function getGroupeMission(){
  var nom1 = selectpersonnages.groupe.mission[1];
  var nom2 = selectpersonnages.groupe.mission[2];
  var nom3 = selectpersonnages.groupe.mission[3];
  //console.log(nom3);
  $('.grp1-1').html(nom1);
  $('.grp1-2').html(nom2);
  $('.grp1-3').html(nom3);
  if(nom1.toLowerCase() == 'kannan' || nom1.toLowerCase() == 'haziko' || nom1.toLowerCase() == 'jayne' || nom1.toLowerCase() == 'lasseni'){
    $('.grp1-1-garcon').remove();
  } else {
    $('.grp1-1-fille').remove();
  }
  if(nom2.toLowerCase() == 'kannan' || nom2.toLowerCase() == 'haziko' || nom2.toLowerCase() == 'jayne' || nom2.toLowerCase() == 'lasseni'){
    $('.grp1-2-garcon').remove();
  } else {
    $('.grp1-2-fille').remove();
  }
  if(nom3.toLowerCase() == 'kannan' || nom3.toLowerCase() == 'haziko' || nom3.toLowerCase() == 'jayne' || nom3.toLowerCase() == 'lasseni'){
    $('.grp1-3-garcon').remove();
  } else {
    $('.grp1-3-fille').remove();
  }
}

function getJayne(){
  var nom = "";
  var namee = "";
  if(selectpersonnages.jayn != null){
    nom = 'Jayn';
  } else {
    nom = 'Jayne';
    namee = "e";
  }
  $(".nom-jayne-jayn").html(nom);
  $(".e-jayne-jayn").html(namee);
}
function nomautre (){
  var nomautre = '';
  if(selectpersonnages.anselm.magie == selectedplayer.magie){
    nomautre = 'Anselm';
  } else if(selectpersonnages.haziko.magie == selectedplayer.magie){
    nomautre = 'Haziko';
  } else if(selectpersonnages.kannan.magie == selectedplayer.magie){
    nomautre = 'Kannan';
  } else if(selectpersonnages.lasseni.magie == selectedplayer.magie){
    nomautre = 'Lasseni';
  } else if(selectpersonnages.maclumy.magie == selectedplayer.magie){
    nomautre = 'Mac Lumy';
  } else if(selectpersonnages.panacle.magie == selectedplayer.magie){
    nomautre = 'Panacle';
  } else{
    if(selectedplayer.sexe == 'fille'){
      nomautre = 'Jayn';
    } else {
      nomautre = 'Jayne';
    }
  }
  selectpersonnages.compagnon = nomautre;
  saveGame();
}
function insertNew(obj, entry, debloque){

  if(obj[entry] == null)
    obj[entry] = {"debloque": false};
  if(debloque == undefined || debloque == null || debloque == ''){
    obj[entry].debloque = true;
  } else {
    obj[entry] = debloque;
  }

}
