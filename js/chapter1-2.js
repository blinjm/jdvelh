var groupe_chap2 = [];
var aveck = false;
var rem = false;
var nao = false;
var mparti = "";
var forteresseguy = "";
var forteresseguyS = "F";
var miguelname  = false;
var numgroupe = 0;
var levydis = false;
var impcapture = false;
var impmort = false;
var grpsansfg = false;
var decouvert = false;
// MAC LUMY + LASSENI + REM + TOI + KONAKEY 26 --> 1
// MAC LUMY + LASSENI + NAO + TOI + KONAKEY 26 --> 2
// MAC LUMY + LASSENI + NAO + TOI  27 --> 3
// MAC LUMY + LASSENI + REM + TOI  27 --> 4
// MAC LUMY + LASSENI + NAO + TOI + KANNAN 29 --> 5
// MAC LUMY + LASSENI + REM + TOI + KANNAN 29 --> 6
// MAC LUMY + LASSENI + REM + TOI YMIR + KANNAN 32 ou 30 --> 7
// MAC LUMY + LASSENI + REM + TOI YMIR 32 ou 30 --> 8

// TOI YMIR 33 --> 9 // n'existe plus au 50 - REPOS
// TOI YMIR + KANNAN 33 --> 10 // n'existe plus au 50 - REPOS

function goActionChapter2(){
  getInfoChap2();
  getGroupeMission();
  getMagesGroupe();
  mparti = selectpersonnages.groupe.mission[1];

  if(decouvert){
    $('.decouvert').show();
    $('.pas-decouvert').remove();
  } else {
    $('.decouvert').remove();
    $('.pas-decouvert').show();
  }
  if(rem){
    $('.rem').show();
    $('.pas-rem').remove();
  } else {
    $('.rem').remove();
    $('.pas-rem').show();
  }

  if(grpsansfg){
    $('.grpsansfg').show();  // vous connaissez Konakey mais elle est pas dans le groupe
    $('.grpsansfg-2').remove();
  } else {
    $('.grpsansfg').remove();
    $('.grpsansfg-2').show();
  }

  if(levydis){
    $('.iflevy').remove();
    $('.if-pas-levy').show();
  } else {
    $('.iflevy').show();
    $('.if-pas-levy').remove();
  }

  if(impcapture){
    $('.impcapture').show();
  } else {
    $('.impcapture').remove();
  }

  if(nao){
    $('.nao').show();
    $('.pas-nao').remove();
  } else {
    $('.nao').remove();
    $('.pas-nao').show();
  }

  if(numgroupe != 0){
    $('.numgroupe').not('.numgroupe-'+numgroupe).remove();
  }

  if(aveck){
    $('.aveck').show();
    $('.pas-aveck').remove();
    $('.aveck-nom').html(mparti);
  } else {
    $('.aveck').remove();
    $('.pas-aveck').show();
  }

  if(forteresseguy == 'Panacle'){
    forteresseguyS = 'G';
  }

  console.log(forteresseguy);
  if(forteresseguy == "Konakey"){
    console.log(forteresseguy);
    $('.pas-konakey').remove();
    $('.fort-nom').html(forteresseguy);
  } else {
    $('.konakey').remove();
    $('.fort-nom').html(forteresseguy);
  }

  if(forteresseguyS == 'G'){
    $('.sfg-g').show();
    $('.sfg-f').remove();
  } else {
    $('.sfg-f').show();
    $('.sfg-g').remove();
  }
  if(selectedplayer.fin1 > 0){
    $('.fin').not('.fin-'+selectedplayer.fin1).remove();
  }

  if(forteresseguy == selectpersonnages.groupe.mission[1]){
    $('.aveck2').show();
    $('.pas-aveck2').remove();
  } else {
    $('.aveck2').remove();
    $('.pas-aveck2').show();
  }

    $('.finchapitre').off().on('click', function(){
      // finchapitre3
      $('.messagefin').fadeIn(500);

    });

  if(selectedplayer.numero == 1){
    var x = document.getElementById("myAudio");
     x.play();

    setTimeout(function(){
      $('.newchapter, .newchapter2').fadeOut(2000);
      $('.choix2, .resume').fadeIn(2000);
    }, 10000);

    if(selectedplayer.fin1 > 0){
      $('.fin').not('.fin-'+selectedplayer.fin1).remove();
    }

    $('.maplace').html(selectedplayer.place);

    if(selectedplayer.grp1){
      $('.sipasgrp1-1').remove();

      if(selectedplayer.fin1 == 1 || selectedplayer.fin1 == 2 || selectedplayer.fin1 == 3){
        forteresseguy = selectpersonnages.groupe.mission[1];
      } else {
        forteresseguy = "Konakey";
      }
    } else {
      $('.sigrp1-1').remove();
      forteresseguy = "Konakey";
    }

    saveGame();
  }


  if(selectedplayer.numero == 2){
      if(selectedplayer.grp1){
        $('.sipasgrp1-1').remove();
      } else {
        $('.sigrp1-1').remove();
      }

      groupe_chap2.push('Lasseni');
      groupe_chap2.push('Mac Lumy');
      selectpersonnages.groupe.chap2 = groupe_chap2;
      saveGame();
  }

  if(selectedplayer.numero == 3){
      // Rajout de celui qui s'est échappé avec toi
      rem = true;
      nao = false;
      if(selectedplayer.grp1){
        $('.sipasgrp1-1').remove();
        aveck = true;
        miguelname = true;
        groupe_chap2.push(selectpersonnages.groupe.mission[1]);
      } else {
        $('.sigrp1-1').remove();
      }
      selectpersonnages.groupe.chap2 = groupe_chap2;
      saveGame();
  }

  if(selectedplayer.numero == 6){
    insertNew(selectperso, 'Heldegarde');
    insertNew(selectperso, 'Fandrean');
    saveGame();
  }
  if(selectedplayer.numero == 9){
      if(selectedplayer.grp1){
        $('.sipasgrp1-1').remove();
      } else {
        $('.sigrp1-1').remove();
      }

      groupe_chap2.push('Lasseni');
      groupe_chap2.push('Mac Lumy');
      selectpersonnages.groupe.chap2 = groupe_chap2;
      saveGame();
  }

  if(selectedplayer.numero == 10){
      insertNew(selectperso, 'Mullihane');
      insertNew(selectperso, 'Pasha');
      insertNew(selectperso, 'Andaelor');
      insertNew(selectperso, 'Fandrean');
      insertNew(selectperso, 'Rem');
      //insertNew(selectlieux, 'Darmen');
      saveGame();
  }

  if(selectedplayer.numero == 10){
      insertNew(selectperso, 'Heldegarde');
      saveGame();
  }

  if(selectedplayer.numero == 15){
      groupe_chap2.push('Rem');
      rem = true;
      nao = false;
      selectpersonnages.groupe.chap2 = groupe_chap2;
      saveGame();
  }

  if(selectedplayer.numero == 17){
    insertNew(selectperso, 'Nao');
      groupe_chap2.push('Nao');
      nao = true;
      rem = false;
      selectpersonnages.groupe.chap2 = groupe_chap2;
      saveGame();
  }

  if(selectedplayer.numero == 18){
    insertNew(selectperso, 'Usaki');
    saveGame();
  }

  if(selectedplayer.numero == 20){
    selectperso.Usaki.d2 = true;
    saveGame();
  }

  if(selectedplayer.numero == 23){

      if(selectedplayer.fin1 != 6){
        $('.fin.fin-6').remove();
      } else {
        $('.fin.fin-autre').remove();
      }

  }

  if(selectedplayer.numero == 24){
    if(forteresseguy == "Konakey"){
      insertNew(selectperso, 'Konakey');
    }
  }

  if(selectedplayer.numero == 26){
      groupe_chap2.push('Konakey');
      selectpersonnages.groupe.chap2 = groupe_chap2;
      if(nao){
        numgroupe = 2;
      } else {
        numgroupe = 1;
      }
      saveGame();
  }
  if(selectedplayer.numero == 27){
      grpsansfg = true;
      if(nao){
        numgroupe = 3;
      } else {
        numgroupe = 4;
      }
  }

  if(selectedplayer.numero == 29){
    insertNew(selectperso, 'Usaki');
      if(nao){
        numgroupe = 5;
      } else {
        numgroupe = 6;
      }
      miguelname = true;
      groupe_chap2.push(forteresseguy);
      selectpersonnages.groupe.chap2 = groupe_chap2;
      saveGame();
  }

  if(selectedplayer.numero == 30 || selectedplayer.numero == 32){
      if(aveck){
        numgroupe = 7;
      } else {
        numgroupe = 8;
      }
      groupe_chap2.push('Mac Lumy');
      groupe_chap2.push('Lasseni');
      groupe_chap2.push('Rem');
      selectpersonnages.groupe.chap2 = groupe_chap2;
      saveGame();
  }
  if(selectedplayer.numero == 33){
      if(aveck){
        numgroupe = 10;
      } else {
        numgroupe = 9;
      }
  }

  if(selectedplayer.numero == 41 || selectedplayer.numero == 42 || selectedplayer.numero == 43 || selectedplayer.numero == 47 || selectedplayer.numero == 48){

      if(selectedplayer.fin1 != 6){
        $('.fin.fin-6').remove();
      } else {
        $('.fin.fin-autre').remove();
      }
  }

  if(selectedplayer.numero == 45){
      if(numgroupe == 10){
        numgroupe = 7;
      } else if(numgroupe == 9){
        numgroupe = 8;
      }
  }
  if(selectedplayer.numero == 48){
      if(nao){
        numgroupe = 3;
      } else {
        numgroupe = 4;
      }
  }
  if(selectedplayer.numero == 50){

      if(numgroupe == 1 || numgroupe == 2){
        selectperso.Konakey.d2 = true;
      }
      if(numgroupe == 1 || numgroupe == 2 || numgroupe == 5 || numgroupe == 6){
        miguelname = true;
      }
      if(selectedplayer.fin1 != 6){
        $('.fin.fin-6').remove();
      } else {
        $('.fin.fin-autre').remove();
      }
  }
  if(selectedplayer.numero == 51){
    insertNew(selectperso, 'Imp');
    selectedplayer.energie = "noire2";
    saveGame();
  }
  if(selectedplayer.numero == 55){
    insertNew(selectsorts, 'blizzard');
    saveGame();
  }
  if(selectedplayer.numero == 60){
    insertNew(selectsorts, 'pilier de pierre');
    saveGame();
  }
  if(selectedplayer.numero == 61){
    insertNew(selectsorts, 'choc psy');
    saveGame();
  }
  if(selectedplayer.numero == 67){
    insertNew(selectperso, 'Imp');
    saveGame();
  }
  if(selectedplayer.numero == 69){
    decouvert = true;
    saveGame();
  }
  if(selectedplayer.numero == 73 || selectedplayer.numero == 72 || selectedplayer.numero == 71 || selectedplayer.numero == 68){
    levydis = true;
    saveGame();
  }

  if(selectedplayer.numero == 80){
      insertNew(selectcreature, 'porcussin');
      saveGame();
  }
  if(selectedplayer.numero == 83){
    impcapture = true;
    selectperso.Imp.d2 = true;
    saveGame();
  }
  if(selectedplayer.numero == 85){
    if(forteresseguy == "Konakey"){
      $('.kannan').remove();
    } else {
      $('.pas-kannan').remove();
    }
  }
  if(selectedplayer.numero == 88){
    selectperso.Imp.d3 = true;
    impmort = true;
    saveGame();
  }
  if(selectedplayer.numero == 89){
    $('.kannan-nom').html(mparti);
    if(selectedplayer.grp1){
      $('.kannan').show();
    } else {
      $('.kannan').remove();
    }
  }
  if(selectedplayer.numero == 90){
    forteresseguy = "Konakey";
    saveGame();
  }
  if(selectedplayer.numero == 103){
    selectperso.Usaki.d2 = true;
    saveGame();
  }
  if(selectedplayer.numero == 152){
    var x1 = document.getElementById("myAudio");
     x1.play();
    selectedplayer.fin2 = 1;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      gotoChap3();
    });
  }
  if(selectedplayer.numero == 153){
    var x2 = document.getElementById("myAudio");
     x2.play();
    selectedplayer.fin2 = 2;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      gotoChap3();
    });
  }
  if(selectedplayer.numero == 154){
    var x3 = document.getElementById("myAudio");
     x3.play();
    selectedplayer.fin2 = 3;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      gotoChap3();
    });
  }
  if(selectedplayer.numero == 155){
    var x4 = document.getElementById("myAudio");
     x4.play();
    selectedplayer.fin2 = 4;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      gotoChap3();
    });
  }

  saveInfoChap2();
}

function gotoChap3() {
  // finchapitre3
  selectedplayer.chapitrenum = 'chapitre3';
  selectedplayer.chapitre = 'Chap.3 - La bataille de la Lune';
  selectedplayer.numero = 1;
  saveGame();
  $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
    binding();
    $( "body, .num, main, html" ).scrollTop( 0 );
    $( window ).scrollTop( 0 );
    $( document ).scrollTop( 0 );
  });
}

function getInfoChap2(){
  aveck = selectedplayer.aveck;
  rem = selectedplayer.rem;
  nao = selectedplayer.nao;
  mparti = selectedplayer.mparti;
  forteresseguy = selectedplayer.forteresseguy;
  forteresseguyS = selectedplayer.forteresseguyS;
  miguelname = selectedplayer.miguelname;
  numgroupe = selectedplayer.numgroupe;
  levydis = selectedplayer.levydis;
  impcapture = selectedplayer.impcapture;
  grpsansfg = selectedplayer.grpsansfg;
  impmort = selectedplayer.impmort;
  decouvert = selectedplayer.decouvert;
}
function saveInfoChap2(){
  selectedplayer.impmort = impmort;
  selectedplayer.grpsansfg = grpsansfg;
  selectedplayer.impcapture = impcapture;
  selectedplayer.levydis = levydis;
  selectedplayer.numgroupe = numgroupe;
  selectedplayer.aveck = aveck;
  selectedplayer.rem = rem;
  selectedplayer.nao = nao;
  selectedplayer.mparti = mparti;
  selectedplayer.forteresseguy = forteresseguy;
  selectedplayer.forteresseguyS = forteresseguyS;
  selectedplayer.miguelname = miguelname;
  selectedplayer.decouvert = decouvert;
  saveGame();
}
