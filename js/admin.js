function getadmin(){

  var player = 'joueur1';
  // COMMENT
  if(joueur_saved != null){
    selectedplayer = joueur_saved[player];
    selectperso = persos[player];
    selectlieux = lieux[player];
    selectcreature = creatures[player];
    selectpersonnages = personnages[player];

    var nb_obj = (Object.keys(joueur_saved).length);
    if(nb_obj > 0){
      for(var keys in joueur_saved){
        $('.selectplayer').append('<option value="'+keys+'">'+joueur_saved[keys].nom+'</option>');
      }
    } else {
      $('.getplayer').html('Aucun joueur enregistré');
    }

    if($('.selectplayer').length > 0){
      updateFields();
    }

    $('.save').off().on('click', function(){
      selectedplayer.avantage = $('.avantage').val();
      selectedplayer.defaut = $('.defaut').val();
      selectedplayer.numero = $('.numero').val();
      selectedplayer.sexe = $('.sexe').val();
      selectedplayer.place = $('.place').val();
      selectedplayer.magie = $('.magie').val();
      selectedplayer.energie = $('.energie').val();
      saveGame();
      $('.msg').html('saved');
    });

    $('.selectplayer').on('change', function(){
      player = $(this).val();
      selectedplayer = joueur_saved[player];
      selectperso = persos[player];
      selectlieux = lieux[player];
      selectcreature = creatures[player];
      selectpersonnages = personnages[player];
      updateFields();
    });
  }

  function updateFields(){
    $('.selectplayer').val(player);
    $('.avantage').val(selectedplayer.avantage);
    $('.defaut').val(selectedplayer.defaut);
    $('.numero').val(selectedplayer.numero);
    $('.sexe').val(selectedplayer.sexe);
    $('.place').val(selectedplayer.place);
    $('.magie').val(selectedplayer.magie);
    $('.energie').val(selectedplayer.energie);
  }

  var my_url = document.URL;
  if(my_url == 'http://localhost:3000/'){
    var array_name = ['Clem', 'ffghfgh', 'Helene', 'Mathilde', 'Naomi', 'Océane', 'test'];
    for(var i=0;i<array_name.length;i++){
      	$('.exportlst').append("<option value='"+array_name[i]+"' >"+array_name[i]+"</option>");
    }
  } else {
    $.post( "export/export.php", { func: "liste" }, function( data ) {
      $('.exportlst').html(data);
      if(data == ''){
        $('.exportlst, .import').remove();
      }
    });
  }

$('.import').off().on('click', function(){

      var qui = $('.exportlst').val();
      var mycreaturesI, mypersosI, myjoueurI, mylieuxI, mypersonnagesI, mysortsI;
      var passe = true;
      if(joueur_saved != undefined){
        if(joueur_saved.joueur1 != undefined && qui == joueur_saved.joueur1.nom){
          passe = false;
        }
        if(joueur_saved.joueur2 != undefined && qui == joueur_saved.joueur2.nom){
          passe = false;
        }
        if(joueur_saved.joueur3 != undefined && qui == joueur_saved.joueur3.nom){
          passe = false;
        }
      }
      if(passe){
        $.getJSON( "export/Export_crea_"+qui+".json", function( data ) {
          mycreaturesI = data;
          $.getJSON( "export/Export_perso_"+qui+".json", function( data ) {
            mypersosI = data;
            $.getJSON( "export/Export_player_"+qui+".json", function( data ) {
              myjoueurI = data;
              $.getJSON( "export/Export_lieux_"+qui+".json", function( data ) {
                mylieuxI = data;
                $.getJSON( "export/Export_personnages_"+qui+".json", function( data ) {
                  mypersonnagesI = data;
                  $.getJSON( "export/Export_sort_"+qui+".json", function( data ) {
                    mysortsI = data;

                    if(joueur_saved.joueur1 == null){
                      joueur_saved.joueur1 = jQuery.parseJSON(myjoueurI);
                      persos.joueur1 = jQuery.parseJSON(mypersosI);
                      lieux.joueur1 = jQuery.parseJSON(mylieuxI);
                      creatures.joueur1 = jQuery.parseJSON(mycreaturesI);
                      personnages.joueur1 = jQuery.parseJSON(mypersonnagesI);
                      sorts.joueur1 = jQuery.parseJSON(mysortsI);
                      $('.import').text('Joueur 1 ajouté.');
                    } else if(joueur_saved.joueur2 == null){
                      joueur_saved.joueur2 = jQuery.parseJSON(myjoueurI);
                      persos.joueur2 = jQuery.parseJSON(mypersosI);
                      lieux.joueur2 = jQuery.parseJSON(mylieuxI);
                      creatures.joueur2 = jQuery.parseJSON(mycreaturesI);
                      personnages.joueur2 = jQuery.parseJSON(mypersonnagesI);
                      sorts.joueur2 = jQuery.parseJSON(mysortsI);
                      $('.import').text('Joueur 2 ajouté.');
                    } else if(joueur_saved.joueur3 == null){
                      joueur_saved.joueur3 = jQuery.parseJSON(myjoueurI);
                      persos.joueur3 = jQuery.parseJSON(mypersosI);
                      lieux.joueur3 = jQuery.parseJSON(mylieuxI);
                      creatures.joueur3 = jQuery.parseJSON(mycreaturesI);
                      personnages.joueur3 = jQuery.parseJSON(mypersonnagesI);
                      sorts.joueur3 = jQuery.parseJSON(mysortsI);
                      $('.import').text('Joueur 3 ajouté.');
                    } else {
                      $('.import').text('Vous avez déjà trois profils, merci d\'en supprimer un.');
                    }
                    localStorage.setItem("joueurs", JSON.stringify(joueur_saved));
                    localStorage.setItem("persos", JSON.stringify(persos));
                    localStorage.setItem("lieux", JSON.stringify(lieux));
                    localStorage.setItem("creatures", JSON.stringify(creatures));
                    localStorage.setItem("personnages", JSON.stringify(personnages));
                    localStorage.setItem("sorts", JSON.stringify(sorts));
                  });
                });
              });
            });
          });
        });
      } else {
        $('.import').text('Vous avez déjà une partie en cours pour le personnage : '+qui);
      }
  });
}
