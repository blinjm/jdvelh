var joueur;
var joueur_saved;
var selectedplayer;
var persos;
var selectperso;
var lieux;
var selectlieux;
var creatures;
var selectcreature;
var personnages;
var selectpersonnages;
var sorts;
var selectsorts;
var mage1, mage2, mage3;
var unautre;
var index, index2;
var lesnoms;
var nb;
var nbcap;
var nbm;
var nomm;
var nomsall;

jQuery(document).ready(function($){


	// RECUP SAVED ?
  if (typeof(Storage) !== "undefined") {
    joueur_saved = jQuery.parseJSON(localStorage.getItem("joueurs"));
    persos = jQuery.parseJSON(localStorage.getItem("persos"));
    lieux = jQuery.parseJSON(localStorage.getItem("lieux"));
    creatures = jQuery.parseJSON(localStorage.getItem("creatures"));
    personnages = jQuery.parseJSON(localStorage.getItem("personnages"));
    sorts = jQuery.parseJSON(localStorage.getItem("sorts"));
  }
	// console.log(joueur_saved);
	if(joueur_saved != null){

	} else {
    joueur_saved = jQuery.parseJSON('{}');
	}
  if(persos == null){
    persos = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }
  if(lieux == null){
    lieux = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }
  if(creatures == null){
    creatures = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }
  if(personnages == null){
    personnages = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }
  if(sorts == null){
    sorts = jQuery.parseJSON('{"joueur1": {}, "joueur2": {}, "joueur3": {}, "joueur4": {}, "joueur5": {}, "joueur6": {}, "joueur7": {}, "joueur8": {}, "joueur9": {}, "joueur10": {}}');
  }

	trad(lang);

	$('.radiobt').parent().on('click', function(){
		$(this).find('.radiobt').toggleClass('checked');
	});

});


var groupes_mission = [];
var places_temp = [1,2,4,5,6,7,8];
var choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "vent"];

function saveGame(){
  //console.log(selectedplayer);
  if(selectedplayer.nom == joueur_saved.joueur1.nom){
    joueur_saved.joueur1 = selectedplayer;
    persos.joueur1 = selectperso;
    lieux.joueur1 = selectlieux;
    creatures.joueur1 = selectcreature;
    personnages.joueur1 = selectpersonnages;
    sorts.joueur1 = selectsorts;
  } else if(selectedplayer.nom == joueur_saved.joueur2.nom){
    joueur_saved.joueur2 = selectedplayer;
    persos.joueur2 = selectperso;
    lieux.joueur2 = selectlieux;
    creatures.joueur2 = selectcreature;
    personnages.joueur2 = selectpersonnages;
    sorts.joueur2 = selectsorts;
  } else if(selectedplayer.nom == joueur_saved.joueur3.nom){
    joueur_saved.joueur3 = selectedplayer;
    persos.joueur3 = selectperso;
    lieux.joueur3 = selectlieux;
    creatures.joueur3 = selectcreature;
    personnages.joueur3 = selectpersonnages;
    sorts.joueur3 = selectsorts;
  }
  localStorage.setItem("joueurs", JSON.stringify(joueur_saved));
  localStorage.setItem("persos", JSON.stringify(persos));
  localStorage.setItem("lieux", JSON.stringify(lieux));
  localStorage.setItem("creatures", JSON.stringify(creatures));
  localStorage.setItem("personnages", JSON.stringify(personnages));
  localStorage.setItem("sorts", JSON.stringify(sorts));

  saveonline();
}

function binding(){
  if(selectedplayer.sexe == 'fille'){
    $('.sexe.sexe-garcon, .sg').remove();
  } else {
    $('.sexe.sexe-fille, .sf').remove();
  }
  if(selectedplayer.energie != null){
    if(selectedplayer.energie == 'verte'){
      $('.magie-noire').remove();
      $('.magie-noire2').remove();
    } else {
      if(selectedplayer.energie == 'noire'){
        $('.magie-verte').remove();
        $('.magie-noire2').remove();
      } else {
        $('.magie-verte').remove();
      }
    }
  }

  $('.av-courageux, .av-populaire, .av-riche, .av-sportif, .av-studieux').hide();
  if(selectedplayer.avantage == 'courageux'){
    $('.av-courageux').show();
  }
  if(selectedplayer.avantage == 'populaire'){
    $('.av-populaire').show();
  }
  if(selectedplayer.avantage == 'riche'){
    $('.av-riche').show();
  }
  if(selectedplayer.avantage == 'sportif'){
    $('.av-sportif').show();
  }
  if(selectedplayer.avantage == 'studieux'){
    $('.av-studieux').show();
  } else {
    if(selectedplayer.chapitrenum != "chapitre1"){
      $('.av-studieux').remove();
    }
  }

  $('.def-bagarreur, .def-naif, .def-parfait, .def-peureux, .def-tricheur').hide();
  if(selectedplayer.defaut == 'bagarreur'){
    $('.def-bagarreur').show();
  }
  if(selectedplayer.defaut == 'naif'){
    $('.def-naif').show();
  }
  if(selectedplayer.defaut == 'parfait'){
    $('.def-parfait').show();
  }
  if(selectedplayer.defaut == 'peureux'){
    $('.def-peureux').show();
  }
  if(selectedplayer.defaut == 'tricheur'){
    $('.def-tricheur').show();
  }

  $('.magieMType').html(selectedplayer.magie);
  $('.magie-terre, .magie-feu, .magie-eau, .magie-vent').hide();
  if(selectedplayer.magie == 'terre'){
    $('.magie-terre').show();
  }
  if(selectedplayer.magie == 'feu'){
    $('.magie-feu').show();
  }
  if(selectedplayer.magie == 'eau'){
    $('.magie-eau').show();
  }
  if(selectedplayer.magie == 'vent'){
    $('.magie-vent').show();
  }

  if(selectedplayer.magie == 'terre'){
    $('.magie-pas-terre').hide();
  }
  if(selectedplayer.magie == 'feu'){
    $('.magie-pas-feu').hide();
  }
  if(selectedplayer.magie == 'eau'){
    $('.magie-pas-eau').hide();
  }
  if(selectedplayer.magie == 'vent'){
    $('.magie-pas-vent').hide();
  }


  if(selectedplayer.avantage == 'courageux'){
    $('.av-pas-courageux').remove();
  }
  if(selectedplayer.avantage == 'populaire'){
    $('.av-pas-populaire').remove();
  }
  if(selectedplayer.avantage == 'riche'){
    $('.av-pas-riche').remove();
  }
  if(selectedplayer.avantage == 'sportif'){
    $('.av-pas-sportif').remove();
  }
  if(selectedplayer.avantage == 'studieux'){
    $('.av-pas-studieux').remove();
  }

  if(selectedplayer.defaut == 'bagarreur'){
    $('.def-pas-bagarreur').remove();
  }
  if(selectedplayer.defaut == 'naif'){
    $('.def-pas-naif').remove();
  }
  if(selectedplayer.defaut == 'parfait'){
    $('.def-pas-parfait').remove();
  }
  if(selectedplayer.defaut == 'peureux'){
    $('.def-pas-peureux').remove();
  }
  if(selectedplayer.defaut == 'tricheur'){
    $('.def-pas-tricheur').remove();
  }

  $('.tonnom').html(selectedplayer.nom);

  $('.ch').off().on('click', function(){
    var num_temp = parseInt($(this).attr('data-go'));
    selectedplayer.numero = num_temp;
    saveGame();

    $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
      binding();
      $( "body, .num, main, html" ).scrollTop( 0 );
      $( window ).scrollTop( 0 );
      $( document ).scrollTop( 0 );
    });
  });

  // LIVRE 2
  if(selectedplayer.livrenum == 'livre2') {
    if(selectedplayer.chapitrenum == 'chapitre21') {
      goActionLivre2_1();
    } else {
      goActionLivre2_2();
    }
  } else if(selectedplayer.chapitrenum == 'chapitre1') {
  if(selectedplayer.numero > 125){
    getGroupeMission();
    getMagesGroupe();
  }

  if(selectedplayer.numero == 1){
    insertNew(selectperso, 'Ymir');
    insertNew(selectperso, 'Zaïra');
    insertNew(selectlieux, 'montsnoirs');
    insertNew(selectlieux, 'terresnord');
    insertNew(selectcreature, 'soufflardent');
    insertNew(selectcreature, 'worg');
    insertNew(selectcreature, 'pistre');
    insertNew(selectcreature, 'barlin');
    saveGame();
  }
  if(selectedplayer.numero == 2){
    insertNew(selectlieux, 'Sarambe');
    insertNew(selectperso, 'Espéride');
    if(selectedplayer.avantage == 'riche'){
      insertNew(selectperso, 'Médeya');
    }
    if(selectedplayer.avantage == 'studieux'){
      insertNew(selectperso, 'Eran');
    }
    if(selectedplayer.avantage == 'populaire'){
      insertNew(selectperso, 'Annette');
      insertNew(selectperso, 'Rodolphe');
    }
    saveGame();
  }
  if(selectedplayer.numero == 4){
    selectedplayer.magieforce = 'vent';
    selectperso.Espéride.triche = true;
    saveGame();
  }
  if(selectedplayer.numero == 5 || selectedplayer.numero == 6 || selectedplayer.numero == 7 || selectedplayer.numero == 8){
    selectedplayer.debloqueinfo = 'vrai';
    saveGame();
  }
  if(selectedplayer.numero == 5){
    selectedplayer.place = '1';
    saveGame();
  }
  if(selectedplayer.numero == 6){
    selectedplayer.place = '4';
    saveGame();
  }
  if(selectedplayer.numero == 7){
    selectedplayer.place = '3';
    saveGame();
  }
  if(selectedplayer.numero == 8){
    selectedplayer.place = '9';
    selectedplayer.tricheur = 'vrai';
    saveGame();
  }
  if(selectedplayer.numero == 9){
  	if(selectedplayer.avantage == 'riche'){
      insertNew(selectperso, 'Romond');
  	}
    insertNew(selectlieux, 'Azalys');
    insertNew(selectlieux, 'Déluine');
    saveGame();
  }
  if(selectedplayer.numero == 11){
    selectedplayer.bataille = 'vrai';
    saveGame();
  }
  if(selectedplayer.numero == 12){
    selectedplayer.trahison = 'vrai';
    saveGame();
  }
  if(selectedplayer.numero == 13){
    selectedplayer.choix = 'vrai';
    saveGame();
  }
  if(selectedplayer.numero == 14){
    selectedplayer.malediction = 'vrai';
    saveGame();
  }
  if(selectedplayer.numero == 15){
    selectedplayer.voile = 'vrai';
    saveGame();
  }
  if(selectedplayer.numero == 16){
    if(selectedplayer.tricheur == 'vrai'){
      $('.pas-tricheur').hide();
    } else {
      $('.tricheur').hide();
    }
    if(selectedplayer.place == '1'){
      $('.place-3, .place-4, .place-9').hide();
    } else if(selectedplayer.place == '3'){
      $('.place-1, .place-4, .place-9').hide();
    } else if(selectedplayer.place == '4'){
      $('.place-1, .place-3, .place-9').hide();
    } else if(selectedplayer.place == '9'){
      $('.place-1, .place-3, .place-4').hide();
    }
    insertNew(selectperso, 'Dioné');
    insertNew(selectperso, 'Soyos');

  	var groupes_chambre = [];
  	if(selectedplayer.sexe == "fille"){
  		groupes_chambre = [selectedplayer.nom, 'Kannan', 'Lasseni', 'Haziko', 'Mac Lumy', 'Jayn', 'Anselm', 'Panacle'];
  	} else {
  		groupes_chambre = [selectedplayer.nom, 'Mac Lumy', 'Lasseni', 'Haziko', 'Kannan', 'Jayne', 'Anselm', 'Panacle'];
  	}
  	if(selectpersonnages.groupe == null){
  		selectpersonnages.groupe = {"chambre" : groupes_chambre};
  	}
    saveGame();
  }

  if(selectedplayer.numero == 21){
  	selectedplayer.magie = 'vent';
    choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "terre"];
  	if(selectedplayer.sexe == "fille"){
  		groupes_mission = [selectedplayer.nom, 'Kannan', 'Mac Lumy', 'Anselm', 'Lasseni', 'Jayn', 'Haziko', 'Panacle'];
  	} else {
  		groupes_mission = [selectedplayer.nom, 'Kannan', 'Haziko', 'Anselm', 'Lasseni', 'Jayne', 'Mac Lumy', 'Panacle'];
  	}
  	selectpersonnages.groupe.mission = groupes_mission;
  	if(selectedplayer.magieforce == "vent"){
  		places_temp = [1,2,3,5,6,7,8];
  	} else {
  		if(selectedplayer.place == '1'){
  			places_temp = [2,3,4,5,6,7,8];
  		} else if(selectedplayer.place == '3'){
  			places_temp = [1,2,4,5,6,7,8];
  		} else if(selectedplayer.place == '4'){
  			places_temp = [1,2,3,5,6,7,8];
  		}
  	}
    insertNew(selectpersonnages, 'lasseni', {"place": places_temp[0]});
    selectpersonnages.lasseni.magie = choix_temp[0];
    insertNew(selectpersonnages, 'maclumy', {"place": places_temp[1]});
    selectpersonnages.maclumy.magie = choix_temp[1];
    insertNew(selectpersonnages, 'haziko', {"place": places_temp[2]});
    selectpersonnages.haziko.magie = choix_temp[2];
    insertNew(selectpersonnages, 'anselm', {"place": places_temp[3]});
    selectpersonnages.anselm.magie = choix_temp[3];
    insertNew(selectpersonnages, 'kannan', {"place": places_temp[4]});
    selectpersonnages.kannan.magie = choix_temp[4];
    insertNew(selectpersonnages, 'panacle', {"place": places_temp[5]});
    selectpersonnages.panacle.magie = choix_temp[5];

    if(selectedplayer.sexe == 'fille'){
      insertNew(selectpersonnages, 'jayn', {"place": places_temp[6]});
      selectpersonnages.jayn.magie = choix_temp[6];
    } else {
      insertNew(selectpersonnages, 'jayne', {"place": places_temp[6]});
      selectpersonnages.jayne.magie = choix_temp[6];
    }

    $('[data-chambre="0"]').html(selectpersonnages.groupe.chambre[0]);
    $('[data-chambre="1"]').html(selectpersonnages.groupe.chambre[1]);
    $('[data-chambre="2"]').html(selectpersonnages.groupe.chambre[2]);
    $('[data-chambre="3"]').html(selectpersonnages.groupe.chambre[3]);
    $('[data-chambre="4"]').html(selectpersonnages.groupe.chambre[4]);
    $('[data-chambre="5"]').html(selectpersonnages.groupe.chambre[5]);
    $('[data-chambre="6"]').html(selectpersonnages.groupe.chambre[6]);
    $('[data-chambre="7"]').html(selectpersonnages.groupe.chambre[7]);

    saveGame();
  }

  if(selectedplayer.numero == 22){
  	selectedplayer.magie = 'eau';

  	if(selectedplayer.place == '1'){
  		places_temp = [2,3,4,5,6,7,8];
  		choix_temp = ["eau", "feu", "feu", "terre", "vent", "vent", "terre"];
  	} else if(selectedplayer.place == '3'){
  		places_temp = [1,2,4,5,6,7,8];
  		choix_temp = ["eau", "feu", "feu", "terre", "vent", "vent", "terre"];
  	} else if(selectedplayer.place == '4'){
  		places_temp = [1,2,3,5,6,7,8];
  		choix_temp = ["eau", "feu", "feu", "terre", "vent", "vent", "terre"];
  	}
  	if(selectedplayer.sexe == "fille"){
  		groupes_mission = [selectedplayer.nom, 'Kannan', 'Mac Lumy', 'Anselm', 'Lasseni', 'Jayn', 'Haziko', 'Panacle'];
  	} else {
  		groupes_mission = [selectedplayer.nom, 'Kannan', 'Haziko', 'Anselm', 'Lasseni', 'Jayne', 'Mac Lumy', 'Panacle'];
  	}
  	selectpersonnages.groupe.mission = groupes_mission;

    insertNew(selectpersonnages, 'lasseni', {"place": places_temp[0]});
    selectpersonnages.lasseni.magie = choix_temp[0];
    insertNew(selectpersonnages, 'maclumy', {"place": places_temp[1]});
    selectpersonnages.maclumy.magie = choix_temp[1];
    insertNew(selectpersonnages, 'haziko', {"place": places_temp[2]});
    selectpersonnages.haziko.magie = choix_temp[2];
    insertNew(selectpersonnages, 'anselm', {"place": places_temp[3]});
    selectpersonnages.anselm.magie = choix_temp[3];
    insertNew(selectpersonnages, 'kannan', {"place": places_temp[4]});
    selectpersonnages.kannan.magie = choix_temp[4];
    insertNew(selectpersonnages, 'panacle', {"place": places_temp[5]});
    selectpersonnages.panacle.magie = choix_temp[5];

    if(selectedplayer.sexe == 'fille'){
      insertNew(selectpersonnages, 'jayn', {"place": places_temp[6]});
      selectpersonnages.jayn.magie = choix_temp[6];
    } else {
      insertNew(selectpersonnages, 'jayne', {"place": places_temp[6]});
      selectpersonnages.jayne.magie = choix_temp[6];
    }

    $('[data-chambre="0"]').html(selectpersonnages.groupe.chambre[0]);
    $('[data-chambre="1"]').html(selectpersonnages.groupe.chambre[1]);
    $('[data-chambre="2"]').html(selectpersonnages.groupe.chambre[2]);
    $('[data-chambre="3"]').html(selectpersonnages.groupe.chambre[3]);
    $('[data-chambre="4"]').html(selectpersonnages.groupe.chambre[4]);
    $('[data-chambre="5"]').html(selectpersonnages.groupe.chambre[5]);
    $('[data-chambre="6"]').html(selectpersonnages.groupe.chambre[6]);
    $('[data-chambre="7"]').html(selectpersonnages.groupe.chambre[7]);
    saveGame();
  }
  if(selectedplayer.numero == 23){
	   selectedplayer.magie = 'feu';
  	if(selectedplayer.place == '1'){
  		places_temp = [2,3,4,5,6,7,8];
  		choix_temp = ["eau", "feu", "vent", "terre", "eau", "vent", "terre"];
  	} else if(selectedplayer.place == '3'){
  		places_temp = [1,2,4,5,6,7,8];
  		choix_temp = ["eau", "feu", "vent", "terre", "eau", "vent", "terre"];
  	} else if(selectedplayer.place == '4'){
  		places_temp = [1,2,3,5,6,7,8];
  		choix_temp = ["eau", "feu", "vent", "terre", "eau", "vent", "terre"];
  	}
  	if(selectedplayer.sexe == "fille"){
  		groupes_mission = [selectedplayer.nom, 'Panacle', 'Kannan', 'Jayn', 'Lasseni', 'Anselm', 'Mac Lumy', 'Haziko'];
  	} else {
  		groupes_mission = [selectedplayer.nom, 'Kannan', 'Haziko', 'Anselm', 'Lasseni', 'Jayne', 'Mac Lumy', 'Panacle'];
  	}
  	selectpersonnages.groupe.mission = groupes_mission;

    insertNew(selectpersonnages, 'lasseni', {"place": places_temp[0]});
    selectpersonnages.lasseni.magie = choix_temp[0];
    insertNew(selectpersonnages, 'maclumy', {"place": places_temp[1]});
    selectpersonnages.maclumy.magie = choix_temp[1];
    insertNew(selectpersonnages, 'haziko', {"place": places_temp[2]});
    selectpersonnages.haziko.magie = choix_temp[2];
    insertNew(selectpersonnages, 'anselm', {"place": places_temp[3]});
    selectpersonnages.anselm.magie = choix_temp[3];
    insertNew(selectpersonnages, 'kannan', {"place": places_temp[4]});
    selectpersonnages.kannan.magie = choix_temp[4];
    insertNew(selectpersonnages, 'panacle', {"place": places_temp[5]});
    selectpersonnages.panacle.magie = choix_temp[5];

    if(selectedplayer.sexe == 'fille'){
      insertNew(selectpersonnages, 'jayn', {"place": places_temp[6]});
      selectpersonnages.jayn.magie = choix_temp[6];
    } else {
      insertNew(selectpersonnages, 'jayne', {"place": places_temp[6]});
      selectpersonnages.jayne.magie = choix_temp[6];
    }

    $('[data-chambre="0"]').html(selectpersonnages.groupe.chambre[0]);
    $('[data-chambre="1"]').html(selectpersonnages.groupe.chambre[1]);
    $('[data-chambre="2"]').html(selectpersonnages.groupe.chambre[2]);
    $('[data-chambre="3"]').html(selectpersonnages.groupe.chambre[3]);
    $('[data-chambre="4"]').html(selectpersonnages.groupe.chambre[4]);
    $('[data-chambre="5"]').html(selectpersonnages.groupe.chambre[5]);
    $('[data-chambre="6"]').html(selectpersonnages.groupe.chambre[6]);
    $('[data-chambre="7"]').html(selectpersonnages.groupe.chambre[7]);
    saveGame();
  }
  if(selectedplayer.numero == 24){
  	selectedplayer.magie = 'terre';
  	if(selectedplayer.place == '1'){
  		places_temp = [2,3,4,5,6,7,8];
  		choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "vent"];
  	} else if(selectedplayer.place == '3'){
  		places_temp = [1,2,4,5,6,7,8];
  		choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "vent"];
  	} else if(selectedplayer.place == '4'){
  		places_temp = [1,2,3,5,6,7,8];
  		choix_temp = ["eau", "feu", "feu", "terre", "eau", "vent", "vent"];
  	}
	if(selectedplayer.sexe == "fille"){
		groupes_mission = [selectedplayer.nom, 'Panacle', 'Mac Lumy', 'Kannan', 'Lasseni', 'Jayn', 'Haziko', 'Anselm'];
	} else {
		groupes_mission = [selectedplayer.nom, 'Panacle', 'Haziko', 'Kannan', 'Lasseni', 'Jayne', 'Mac Lumy', 'Anselm'];
	}
  	selectpersonnages.groupe.mission = groupes_mission;

    insertNew(selectpersonnages, 'lasseni', {"place": places_temp[0]});
	  selectpersonnages.lasseni.magie = choix_temp[0];
    insertNew(selectpersonnages, 'maclumy', {"place": places_temp[1]});
	  selectpersonnages.maclumy.magie = choix_temp[1];
    insertNew(selectpersonnages, 'haziko', {"place": places_temp[2]});
	  selectpersonnages.haziko.magie = choix_temp[2];
    insertNew(selectpersonnages, 'anselm', {"place": places_temp[3]});
	  selectpersonnages.anselm.magie = choix_temp[3];
    insertNew(selectpersonnages, 'kannan', {"place": places_temp[4]});
	  selectpersonnages.kannan.magie = choix_temp[4];
    insertNew(selectpersonnages, 'panacle', {"place": places_temp[5]});
	  selectpersonnages.panacle.magie = choix_temp[5];

	  if(selectedplayer.sexe == 'fille'){
      insertNew(selectpersonnages, 'jayn', {"place": places_temp[6]});
  	  selectpersonnages.jayn.magie = choix_temp[6];
	  } else {
      insertNew(selectpersonnages, 'jayne', {"place": places_temp[6]});
  	  selectpersonnages.jayne.magie = choix_temp[6];
	  }

    $('[data-chambre="0"]').html(selectpersonnages.groupe.chambre[0]);
    $('[data-chambre="1"]').html(selectpersonnages.groupe.chambre[1]);
    $('[data-chambre="2"]').html(selectpersonnages.groupe.chambre[2]);
    $('[data-chambre="3"]').html(selectpersonnages.groupe.chambre[3]);
    $('[data-chambre="4"]').html(selectpersonnages.groupe.chambre[4]);
    $('[data-chambre="5"]').html(selectpersonnages.groupe.chambre[5]);
    $('[data-chambre="6"]').html(selectpersonnages.groupe.chambre[6]);
    $('[data-chambre="7"]').html(selectpersonnages.groupe.chambre[7]);
    saveGame();
  }
  if(selectedplayer.numero == 25){

    insertNew(selectperso, 'Lasseni');
    insertNew(selectperso, 'MacLumy');
    insertNew(selectperso, 'Kannan');
    insertNew(selectperso, 'Panacle');
    insertNew(selectperso, 'Haziko');
    insertNew(selectperso, 'Anselm');

    if(selectpersonnages.jayn != null){
      insertNew(selectperso, 'Jayn');
    } else  {
      insertNew(selectperso, 'Jayne');
    }
    if(selectedplayer.sexe == 'fille'){
      selectperso.Kannan.debloque2 = true;
      insertNew(selectlieux, 'Téogile');
    } else {
      selectperso.MacLumy.debloque2 = true;
      insertNew(selectlieux, 'Melb');
    }

    saveGame();

  }
  if(selectedplayer.numero == 26){
	  if(selectedplayer.avantage == 'courageux'){
      selectedplayer.objet.piment = true;
    }
	  if(selectedplayer.avantage == 'riche'){
      selectedplayer.objet.fiole = true;
    }
    insertNew(selectlieux, 'Marina');
    saveGame();
  }
  if(selectedplayer.numero == 27){
	  $('.malediction').hide();
	  if(selectedplayer.malediction != null){
  		if(selectedplayer.malediction == 'vrai'){
  			$('.malediction').show();
  		}
	  }
    insertNew(selectlieux, 'Azalysbibliotheque');
    saveGame();
  }
  if(selectedplayer.numero == 28){
    insertNew(selectperso, 'Nello');
    insertNew(selectperso, 'Appou');
	  if(selectedplayer.avantage == 'populaire'){
      selectperso.Appou.ami = true;
      selectperso.Nello.ami = true;
    }
    saveGame();
  }
  if(selectedplayer.numero == 30){
    insertNew(selectcreature, 'Noctule');
    saveGame();
  }
  if(selectedplayer.numero == 33){
    insertNew(selectperso, 'Qobur');

    $('.ch').hide();
    if(selectedplayer.magie == 'vent'){
      $('.magie-vent').show();
    } else if(selectedplayer.magie == 'eau'){
      $('.magie-eau').show();
    } else if(selectedplayer.magie == 'feu'){
      $('.magie-feu').show();
    } else {
      $('.magie-terre').show();
    }
    selectedplayer.energie = "verte";
    saveGame();
  }
  if(selectedplayer.numero == 34){
    $('.ch').hide();
    if(selectedplayer.magie == 'vent'){
      $('.magie-vent').show();
    } else if(selectedplayer.magie == 'eau'){
      $('.magie-eau').show();
    } else if(selectedplayer.magie == 'feu'){
      $('.magie-feu').show();
    } else {
      $('.magie-terre').show();
    }
    selectedplayer.energie = "noire";
    saveGame();
  }
  if(selectedplayer.numero == 35 || selectedplayer.numero == 36 || selectedplayer.numero == 37 || selectedplayer.numero == 38){
    nomautre();
    $('.nom-autre').html(selectpersonnages.compagnon);
  }
  if(selectedplayer.numero == 36){
  }
  if(selectedplayer.numero == 37){
    insertNew(selectperso, 'Osman');
    saveGame();
  }
  if(selectedplayer.numero == 38){
    insertNew(selectperso, 'Guérénile');
    saveGame();
  }
  if(selectedplayer.numero == 39 || selectedplayer.numero == 40){
    $('.nom-autre').html(selectpersonnages.compagnon);
    console.log(selectsorts);
    insertNew(selectsorts, 'masque');
    saveGame();
  }
  if(selectedplayer.numero == 41 || selectedplayer.numero == 42){
    if(selectedplayer.numero == 41 && selectedplayer.energie == 'noire') {
      selectpersonnages.lasseni.peur = true;
      selectedplayer.surveille = true;
    }
    $('.nom-autre').html(selectpersonnages.compagnon);
    insertNew(selectsorts, 'glaciation');
    saveGame();
  }
  if(selectedplayer.numero == 43 || selectedplayer.numero == 44){
    $('.nom-autre').html(selectpersonnages.compagnon);
    insertNew(selectsorts, 'lumiere');
    saveGame();
  }
  if(selectedplayer.numero == 45){
    $('.nom-autre').html(selectpersonnages.compagnon);
    insertNew(selectsorts, 'racines');
    saveGame();
  }
  if(selectedplayer.numero == 46){
    $('.nom-autre').html(selectpersonnages.compagnon);
    insertNew(selectsorts, 'chute');
    saveGame();
  }
  if(selectedplayer.numero == 47){
    $('.nom-autre').html(selectpersonnages.compagnon);
    insertNew(selectsorts, 'vision');
    saveGame();
  }
  if(selectedplayer.numero == 48){
    $('.nom-autre').html(selectpersonnages.compagnon);
    insertNew(selectsorts, 'flamme');
    saveGame();
  }
  if(selectedplayer.numero == 49){
    $('.nom-autre').html(selectpersonnages.compagnon);
    insertNew(selectsorts, 'guerison');
    saveGame();
  }
  if(selectedplayer.numero == 50){
    $('.nom-autre').html(selectpersonnages.groupe.chambre[1]);
    getJayne();
    if(selectpersonnages.lasseni.peur != null){
      if(selectpersonnages.lasseni.peur == true){
        $('.lassenipeur').removeClass('hidden');
      }
    }
    insertNew(selectlieux, 'Condorra');
    insertNew(selectlieux, 'Azano');
    insertNew(selectlieux, 'GrandCouronne');

    insertNew(selectcreature, 'Manticore');
    insertNew(selectcreature, 'Marandouins');
    insertNew(selectcreature, 'Héréons');

	if(selectedplayer.avantage == 'riche'){
		insertNew(selectperso, 'Cellya');
	}
	if(selectperso.Lasseni.debloque == true){
		selectperso.Lasseni.rumeur = true;
	}

    if(selectedplayer.place == "9"){
      $('.place-9').removeClass('hidden');
    } else if(selectedplayer.place == "1"){
      $('.place-1').removeClass('hidden');
    } else {
      $('.place-3, .place-4').removeClass('hidden');
    }

    if(selectperso.Jayn != null && selectperso.Jayn != undefined){
      $('.nom-jayne-jayn').html('Jayn');
    } else {
      $('.nom-jayne-jayn').html('Jayne');
      $('.e-jayne-jayn').removeClass('hidden');
    }


  }
  if(selectedplayer.numero == 51){

    insertNew(selectcreature, 'moufton');
    insertNew(selectcreature, 'ruflard');
    insertNew(selectcreature, 'canidin');
    insertNew(selectcreature, 'félidé');
    saveGame();

	$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
	$('.grp2-1').html(selectpersonnages.groupe.mission[4]);
	$('.grp2-2').html(selectpersonnages.groupe.mission[5]);
	$('.grp2-3').html(selectpersonnages.groupe.mission[6]);
	$('.grp2-4').html(selectpersonnages.groupe.mission[7]);

  }
  if(selectedplayer.numero == 52){
    insertNew(selectperso, 'Miguel');

	$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
	$('.grp2-1').html(selectpersonnages.groupe.mission[4]);
	$('.grp2-2').html(selectpersonnages.groupe.mission[5]);
	$('.grp2-3').html(selectpersonnages.groupe.mission[6]);
	$('.grp2-4').html(selectpersonnages.groupe.mission[7]);
	selectpersonnages.groupe.chef = [selectedplayer.nom, 'Lasseni'];
    saveGame();
  }
  if(selectedplayer.numero == 53){

	$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
	$('.grp2-1').html(selectpersonnages.groupe.mission[4]);
	$('.grp2-2').html(selectpersonnages.groupe.mission[5]);
	$('.grp2-3').html(selectpersonnages.groupe.mission[6]);
	$('.grp2-4').html(selectpersonnages.groupe.mission[7]);
	selectpersonnages.groupe.chef = [selectpersonnages.groupe.mission[1], 'Lasseni'];
    saveGame();

  }
  if(selectedplayer.numero == 54){

	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
		$('.chef').removeClass('hidden');
	}

  }
  if(selectedplayer.numero == 55){

	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	$('.grp2-2').html(selectpersonnages.groupe.mission[5]);

  }
  if(selectedplayer.numero == 56){

  	$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
  	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
  	$('.grp1-3').html(selectpersonnages.groupe.mission[3]);

  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
  	} else {
  		$('.cheh').removeClass('hidden');
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
  	}
    insertNew(selectperso, 'Guyot');
    insertNew(selectlieux, 'Paumet');
    saveGame();
  }
  if(selectedplayer.numero == 57){

	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
		$('.chef').removeClass('hidden');
	} else {
		$('.cheh').removeClass('hidden');
		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
	}

  }
  if(selectedplayer.numero == 58){

    $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
    $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
    $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
  	} else {
  		$('.cheh').removeClass('hidden');
  		$('.ch.chef').remove();
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
  	}
  	if(selectedplayer.energie == 'noire') {
  		$('.energie-noire').removeClass('hidden');
  		$('.energie-verte').remove();
  	} else {
  		$('.energie-verte').removeClass('hidden');
  		$('.energie-noire').remove();
  	}
  }
  if(selectedplayer.numero == 59){
	$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
    insertNew(selectcreature, 'longlant');
    saveGame();
  }
  if(selectedplayer.numero == 60 || selectedplayer.numero == 61){
	$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
  }
  if(selectedplayer.numero == 62){
	$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
  }
  if(selectedplayer.numero == 63){

	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
		$('.chef').removeClass('hidden');
	} else {
		$('.cheh').removeClass('hidden');
		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
	}
  }
  if(selectedplayer.numero == 64){
	$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
  }
  if(selectedplayer.numero == 65){
	  if(selectedplayer.avantage == "sportif"){
		  if(selectedplayer.magie == "terre"){
			  $('.ch').not('.super-racine').remove();
        $('.pas-super-racine').remove();
		  } else {
			  $('.ch').not('.attaque').remove();
        $('.super-racine').remove();
		  }
	  } else {
		  if(selectedplayer.magie == "terre" && selectedplayer.energie == "noire"){
			  $('.ch').not('.super-racine').remove();
        $('.pas-super-racine').remove();
		  } else if(selectedplayer.magie == "vent"){
			  $('.ch').not('.chute-contro').remove();
        $('.super-racine').remove();
		  } else {
			  $('.ch').not('.chute-autre').remove();
        $('.super-racine').remove();
		  }
	  }
	  getMagesGroupe();
  }
  if(selectedplayer.numero == 66){
  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
  	} else {
  		$('.cheh').removeClass('hidden');
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
  	}
    selectedplayer.mission1 = 'echec';
    saveGame();
  }
  if(selectedplayer.numero == 67){
	  mage1 = selectpersonnages.groupe.mission[1];
	  mage2 = selectpersonnages.groupe.mission[2];
	  mage3 = selectpersonnages.groupe.mission[3];
    if(selectedplayer.magie == 'vent'){
      $('.magie-pas-vent, .chute-autre').remove();
    } else {
      $('.magie-vent, .chute-contro').remove();
  	  if(selectpersonnages[mage1.toLowerCase()].magie == "vent"){
  		  $('.mage-vent').html(mage1);
  	  } else if(selectpersonnages[mage2.toLowerCase()].magie == "vent"){
  		  $('.mage-vent').html(mage2);
  	  } else if(selectpersonnages[mage3.toLowerCase()].magie == "vent"){
  		  $('.mage-vent').html(mage3);
  	  }
    }
  }
  if(selectedplayer.numero == 68){
		$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
		$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
		$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
	  getMagesGroupe();
		if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
			$('.chef').removeClass('hidden');
		} else {
			$('.cheh').removeClass('hidden');
			$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
		}

	if(selectedplayer.defaut == 'bagarreur'){
		selectedplayer.mission1 = 'reussie';
		saveGame();
	}
  }
  if(selectedplayer.numero == 69){
		$('.grp1-1').html(selectpersonnages.groupe.mission[1]);
		$('.grp1-2').html(selectpersonnages.groupe.mission[2]);
		$('.grp1-3').html(selectpersonnages.groupe.mission[3]);
	  getMagesGroupe();
		if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
			$('.chef').removeClass('hidden');
		} else {
			$('.cheh').removeClass('hidden');
			$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
		}

	selectedplayer.mission1 = 'reussie';
	saveGame();
  }
  if(selectedplayer.numero == 70 || selectedplayer.numero == 71){
	  $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	  $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	  $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
	  if(selectedplayer.energie == "noire"){
		  $('.ernegie-noire').removeClass('hidden');
	  } else {
		  $('.ernegie-verte').removeClass('hidden');
	  }
	  getMagesGroupe();
  }
  if(selectedplayer.numero == 72){
  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
  	} else {
  		$('.cheh').removeClass('hidden');
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
  	}
  	  $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
  	  $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
  	  $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
  	  getMagesGroupe();
  }
  if(selectedplayer.numero == 73){
  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
  	} else {
  		$('.cheh').removeClass('hidden');
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
  	}
  	  $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
  	  $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
  	  $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
  	  getMagesGroupe();
  }
  if(selectedplayer.numero == 74){
  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
  	} else {
  		$('.cheh').removeClass('hidden');
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
  	}
  }
  if(selectedplayer.numero == 75){
	  $('.grp1-1').html(selectpersonnages.groupe.mission[1]);
	  $('.grp1-2').html(selectpersonnages.groupe.mission[2]);
	  $('.grp1-3').html(selectpersonnages.groupe.mission[3]);
  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
  	} else {
  		$('.cheh').removeClass('hidden');
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
  	}
    selectedplayer.mission1 = 'reussie';
    saveGame();
  }
  if(selectedplayer.numero == 76){
    $('.nom-autre').html(selectpersonnages.groupe.chambre[1]);
	  getGroupeMission();
  }
  if(selectedplayer.numero == 79){
    $('.nom-autre').html(selectpersonnages.groupe.chambre[1]);
	  getGroupeMission();
  }
  if(selectedplayer.numero == 80){
    $('.nom-autre').html(selectpersonnages.groupe.chambre[1]);
    if(selectpersonnages.groupe.chambre[1] == selectpersonnages.groupe.mission[1]){
      $('.sipasmeme').remove();
    } else {
      $('.simeme').remove();
    }
	  getGroupeMission();
  }
  if(selectedplayer.numero == 81){
    if(selectedplayer.sexe == 'fille'){
      insertNew(selectperso, 'Levy');
    } else {
      insertNew(selectperso, 'Levy2');
    }
    insertNew(selectperso, 'Esteban');
    saveGame();
  }
  if(selectedplayer.numero == 82){
    insertNew(selectperso, 'Estrion');
    insertNew(selectlieux, 'Darmen');
	  getGroupeMission();
    saveGame();
  }
  if(selectedplayer.numero == 83){
    insertNew(selectperso, 'Hector');
	  getGroupeMission();
    saveGame();
  }
  if(selectedplayer.numero == 84){
	  getGroupeMission();
  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
      $('.ch.cheh').remove();
  	} else {
  		$('.cheh').removeClass('hidden');
      $('.ch.chef').remove();
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
  	}
  }
  if(selectedplayer.numero == 85 || selectedplayer.numero == 86 || selectedplayer.numero == 87 || selectedplayer.numero == 88){
	  getGroupeMission();
  }
  if(selectedplayer.numero == 87){
    insertNew(selectperso, 'Jolt');
    saveGame();
  }
  if(selectedplayer.numero == 89){
	  getGroupeMission();
    getMagesGroupe();

  	if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
  		$('.chef').removeClass('hidden');
      $('.ch.cheh').remove();
  	} else {
  		$('.cheh').removeClass('hidden');
      $('.ch.chef').remove();
  		$('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
      if(selectpersonnages.groupe.chef[0] == $('.mage-vent').text()){
        $('.cheh-nom-2').html('Mais la décision finale est que c\'est trop risqué. ');
      }
  	}
  }
  if(selectedplayer.numero == 90){
    getMagesGroupe();
  }
  if(selectedplayer.numero == 91){
    getMagesGroupe();
  }
  if(selectedplayer.numero == 92){
    if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
      $('.chef').removeClass('hidden');
      $('.ch.cheh').remove();
    } else {
      $('.cheh').removeClass('hidden');
      $('.ch.chef').remove();
      $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
    }
    getMagesGroupe();
  }
  if(selectedplayer.numero == 98){
    if(selectedplayer.objet.piment){
      selectedplayer.objet.piment = false;
      saveGame();
      $('.pas-piment').remove();
    } else {
      $('.piment').remove();
    }
  }
  if(selectedplayer.numero == 101){
	  selectedplayer.objet.chefbandit = true;
    saveGame();
  }
  if(selectedplayer.numero == 100){
	  getGroupeMission();
  }
  if(selectedplayer.numero == 102){
    getMagesGroupe();
  }
  if(selectedplayer.numero == 103){
    getMagesGroupe();
    selectedplayer.objet.pluschef = true;
    saveGame();
  }
  if(selectedplayer.numero == 104){
    getMagesGroupe();
    selectedplayer.objet.magevent = "capture";
    saveGame();
  }
  if(selectedplayer.numero == 105){
    getMagesGroupe();
  }
  if(selectedplayer.numero == 106){
    getMagesGroupe();
  }
  if(selectedplayer.numero == 107){
    getMagesGroupe();
  	  selectedplayer.objet.chefbandit = true;
      saveGame();
  }
  if(selectedplayer.numero == 109 || selectedplayer.numero == 110){
    getMagesGroupe();

    if(selectpersonnages.groupe.chef[0] == selectedplayer.nom){
      $('.chef').removeClass('hidden');
      $('.ch.cheh').remove();
    } else {
      $('.cheh').removeClass('hidden');
      $('.ch.chef').remove();
      $('.cheh-nom').html(selectpersonnages.groupe.chef[0]);
    }

    if(selectedplayer.objet.chefbandit){
      $('.chefbandit').removeClass('hidden');
    }
    if(selectedplayer.objet.magevent == "capture"){
      mage1 = selectpersonnages.groupe.mission[1];
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];
      unautre = "";
      if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie != "vent"){
        unautre = mage1;
      } else if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie != "vent"){
        unautre = mage2;
      } else {
        unautre = mage3;
      }
      $('.unautre').html(unautre);
    } else {
      mage1 = selectpersonnages.groupe.mission[1];
      $('.unautre').html(mage1);
    }
  }
  if(selectedplayer.numero == 111){
    getMagesGroupe();

    if(selectedplayer.magie != 'feu'){
      selectedplayer.objet.magefeu = "capture";
      saveGame();
    }
    if(selectedplayer.objet.magevent == "capture"){
      mage1 = selectpersonnages.groupe.mission[1];
      mage2 = selectpersonnages.groupe.mission[2];
      mage3 = selectpersonnages.groupe.mission[3];
      unautre = "";
      if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie != "vent"){
        unautre = mage1;
      } else if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie != "vent"){
        unautre = mage2;
      } else {
        unautre = mage3;
      }
      $('.unautre').html(unautre);
    } else {
      mage1 = selectpersonnages.groupe.mission[1];
      $('.unautre').html(mage1);
    }
  }
  if(selectedplayer.numero == 112){
    getMagesGroupe();

    //if(selectedplayer.magie != 'feu'){
      selectedplayer.objet.mageeau = "blesse";
      saveGame();
    //}
  }
  if(selectedplayer.numero == 113){
    selectedplayer.toutseul = true;
    getMagesGroupe();

    saveGame();
  }
  if(selectedplayer.numero == 114 || selectedplayer.numero == 118 || selectedplayer.numero == 120 || selectedplayer.numero == 121 || selectedplayer.numero == 124){
    insertNew(selectperso, 'Modrak');
    getMagesGroupe();
    if(selectedplayer.voile != null && selectedplayer.voile == 'vrai'){
      $(".carte-voile").removeClass('hidden');
    }
    if(selectedplayer.avantage == 'courageux'){
      selectedplayer.mensonge = true;
      saveGame();
    }
    if(selectedplayer.numero == 114 || selectedplayer.numero == 118 || selectedplayer.numero == 121){
      selectedplayer.cascadeseul = true;
    } else {
      selectedplayer.cascadeseul = false;
    }
    //124 120 pas seule
    //114 118 121  seule
    //
  }
  if(selectedplayer.numero == 115){
    getMagesGroupe();
    mage1 = selectpersonnages.groupe.mission[1];
    mage2 = selectpersonnages.groupe.mission[2];
    mage3 = selectpersonnages.groupe.mission[3];
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventmanque').remove();
    }
    if(selectedplayer.magie == 'eau'){
      if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "terre"){
        $('.dernier').html(mage1);
        if(getfillegarcon(mage1)){
          $('.luielle').html('elle');
        } else {
          $('.luielle').html('lui');
        }
      } else if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "terre"){
        $('.dernier').html(mage2);
        if(getfillegarcon(mage2)){
          $('.luielle').html('elle');
        } else {
          $('.luielle').html('lui');
        }
      } else if(selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "terre"){
        $('.dernier').html(mage3);
        if(getfillegarcon(mage3)){
          $('.luielle').html('elle');
        } else {
          $('.luielle').html('lui');
        }
      }
    } else {
      if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "eau"){
        $('.dernier').html(mage1);
        if(getfillegarcon(mage1)){
          $('.luielle').html('elle');
        } else {
          $('.luielle').html('lui');
        }
      } else if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "eau"){
        $('.dernier').html(mage2);
        if(getfillegarcon(mage2)){
          $('.luielle').html('elle');
        } else {
          $('.luielle').html('lui');
        }
      } else if(selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "eau"){
        $('.dernier').html(mage3);
        if(getfillegarcon(mage3)){
          $('.luielle').html('elle');
        } else {
          $('.luielle').html('lui');
        }
      }
    }
  }
  if(selectedplayer.numero == 116){
    getMagesGroupe();
    selectedplayer.aveceau = true;
    if(selectedplayer.magie == 'terre'){
      selectedplayer.objet.mageeau = "guerie";
    }
    saveGame();
  }
  if(selectedplayer.numero == 122 || selectedplayer.numero == 123){
    insertNew(selectperso, 'Dex');
    insertNew(selectperso, 'Terrence');
    if(selectedplayer.cascadeseul){
      $('.sipasseule').remove();
    } else {
      $('.siseule').remove();
    }
  }
  if(selectedplayer.numero == 125){
    getGroupeMission();
    getMagesGroupe();
    nbcap = 0;
    mage1 = selectpersonnages.groupe.mission[1];
    mage2 = selectpersonnages.groupe.mission[2];
    mage3 = selectpersonnages.groupe.mission[3];

    var if1 = true;
    var if2 = true;
    var if3 = true;
    var present = [];
    var absent = [];
    var mort = [];
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventcapture').remove();
    } else {
      nbcap++;
      if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "vent"){
        if1 = false;
      }
      if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "vent"){
        if2 = false;
      }
      if(selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "vent"){
        if3 = false;
      }
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.magefeucapture').remove();
    } else {
      nbcap++;
      if(selectpersonnages[mage1.toLowerCase().replace(/\s+/g, '')].magie == "feu"){
        if1 = false;
      }
      if(selectpersonnages[mage2.toLowerCase().replace(/\s+/g, '')].magie == "feu"){
        if2 = false;
      }
      if(selectpersonnages[mage3.toLowerCase().replace(/\s+/g, '')].magie == "feu"){
        if3 = false;
      }
    }
    if(selectedplayer.objet.mageeau != "blesse"){
      $('.mageeaublesse').remove();
    }
    if(nbcap == 2){
      $('.capture1').remove();
    } else if(nbcap == 1){
      $('.capture2').remove();
    } else {
      $('.capture1, .capture2').remove();
    }

    if(!if1){
      $('.sinumero1').remove();
      absent.push(mage1);
    } else {
      present.push(mage1);
    }
    if(!if2){
      $('.sinumero2').remove();
      absent.push(mage2);
    } else {
      present.push(mage2);
    }
    if(!if3){
      $('.sinumero3').remove();
      absent.push(mage3);
    } else {
      present.push(mage3);
    }

    //console.log(selectedplayer.toutseul);
    if(selectedplayer.toutseul == true){

      //    console.log('true');
    } else {
        //console.log('false');
      //$('.toutseul').remove();


      if(selectedplayer.aveceau == true){

      } else {
        $('.toutseul').remove();
      }
    }

    if(selectedplayer.aveceau == true){
      $('.toutseul2').removeClass('hidden');
    }

    if(selectedplayer.mensonge != true){
      $('.choixSoufflardent').remove();
    }

    present.push('Estrion');
    present.push('Dex');
    present.push('Terrence');

    selectpersonnages.groupe.final = [absent, present, mort];
    saveGame();
  }
  if(selectedplayer.numero == 126 || selectedplayer.numero == 128){
    insertNew(selectlieux, 'Kaelleg');
    if(selectpersonnages.groupe.final[1][1] == 'Estrion'){
      $('.present2').html(selectpersonnages.groupe.final[1][2]);
    } else {
      $('.present2').html(selectpersonnages.groupe.final[1][1]);
    }

    if(selectpersonnages.groupe.final[0].length > 0){

    } else {
      $('.siabsent').remove();
    }
  }
  if(selectedplayer.numero == 157){
    index = selectpersonnages.groupe.final[1].indexOf('Dex');
    if (index > -1) {
      selectpersonnages.groupe.final[1].splice(index, 1);
    }
    selectpersonnages.groupe.final[2].push('Dex');
    saveGame();

    if(selectpersonnages.groupe.final[1][0] == 'Estrion'){
      $('.sipresent').remove();
    } else {
      lesnoms = '';
      nb = 0;
      for(var i=0;i<selectpersonnages.groupe.final[1].length;i++){
        if(selectpersonnages.groupe.final[1][i] == 'Estrion'){
          break;
        } else {
          lesnoms += selectpersonnages.groupe.final[1][i]+", ";
          nb++;
        }
      }
      lesnoms = lesnoms.substr(0, lesnoms.length-2);
      if(nb == 1) {
        lesnoms += ' a';
      } else if (nb > 1){
        lesnoms += ' ont';
      }
      $('.allnomspresents').html(lesnoms);
    }

    // console.log(selectcreature);
    // console.log(selectcreature.Noctule);
    if(selectcreature.Noctule){

    } else {
      $('.noctule').remove();
    }

  }
  if(selectedplayer.numero == 129){
    index = selectpersonnages.groupe.final[1].indexOf('Dex');
    if (index > -1) {
      selectpersonnages.groupe.final[1].splice(index, 1);
    }
    selectpersonnages.groupe.final[2].push('Dex');
    saveGame();
    if(selectedplayer.bataille){
      $('.ch').not('.carteeclair').remove();
    } else {
      $('.carteeclair').remove();
    }
    if(selectpersonnages.groupe.final[1][0] == 'Estrion'){
      $('.sipresent').remove();
    } else {
      lesnoms = '';
      nb = 0;
      for(var ii=0;ii<selectpersonnages.groupe.final[1].length;ii++){
        if(selectpersonnages.groupe.final[1][ii] == 'Estrion'){
          break;
        } else {
          lesnoms += selectpersonnages.groupe.final[1][ii]+", ";
          nb++;
        }
      }
      lesnoms = lesnoms.substr(0, lesnoms.length-2);
      if(nb == 1) {
        lesnoms += ' a';
      } else if (nb > 1){
        lesnoms += ' ont';
      }
      $('.allnomspresents').html(lesnoms);
    }

  }
  if(selectedplayer.numero == 130){
    getGroupeMission();
    getMagesGroupe();

    nbcap = 0;
    if(selectedplayer.objet.magevent != "capture"){
      $('.simagevent').remove();
    } else {
      nbcap++;
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.simagefeu').remove();
    } else {
      nbcap++;
    }
  }
  if(selectedplayer.numero == 131){
    selectperso.Estrion.debloque2 = true;

    index = selectpersonnages.groupe.final[1].indexOf('Estrion');
    if (index > -1) {
      selectpersonnages.groupe.final[1].splice(index, 1);
    }

    selectpersonnages.groupe.final[2].push('Estrion');
    saveGame();

    if(selectpersonnages.groupe.final[1].length <= 1){
      $('.sipresent').remove();
    }

  }
  if(selectedplayer.numero == 132){
    selectperso.Terrence.debloque2 = true;
    selectperso.Modrak.debloque2 = true;
    getGroupeMission();
    getMagesGroupe();
    if(selectcreature.Noctule){

    } else {
      $('.noctule').remove();
    }
    if(selectpersonnages.groupe.final[1].length <= 2){
      $('.passeul').remove();
    } else {
      $('.seul').remove();
    }
    index = selectpersonnages.groupe.final[2].indexOf('Dex');
    if (index > -1) {
      selectpersonnages.groupe.final[2].splice(index, 1);
    }
    selectpersonnages.groupe.final[1].push('Dex');
    saveGame();
  }
  if(selectedplayer.numero == 133){
    selectperso.Terrence.debloque2 = true;
    selectperso.Modrak.debloque2 = true;
    getGroupeMission();
    getMagesGroupe();
    if(selectcreature.Noctule){

    } else {
      $('.noctule').remove();
    }
    if(selectpersonnages.groupe.final[1].length <= 1){
      $('.sipresent').remove();
    } else {
      $('.siseul').remove();
    }

    index = selectpersonnages.groupe.final[1].indexOf('Terrence');
    if (index > -1) {
      selectpersonnages.groupe.final[1].splice(index, 1);
    }
    selectpersonnages.groupe.final[0].push('Terrence');

    mage1 = selectpersonnages.groupe.mission[1];
    index2 = selectpersonnages.groupe.final[1].indexOf(mage1);
    if (index2 > -1) {
      $('.sipasgrp1-1').remove();
      selectedplayer.grp1 = true;
      selectpersonnages.groupe.final[1].splice(index2, 1);
      selectpersonnages.groupe.final[0].push(mage1);
    } else {
      $('.sigrp1-1').remove();
    }

    saveGame();
  }
  if(selectedplayer.numero == 134){
    selectperso.Terrence.debloque3 = true;

    if(selectedplayer.energie == "verte"){
      selectperso.Modrak.debloque5 = true;
    } else {
      selectperso.Modrak.debloque3 = true;
    }
    getGroupeMission();
    getMagesGroupe();
    if(selectcreature.Noctule){

    } else {
      $('.noctule').remove();
    }
    if(selectpersonnages.groupe.final[1].length <= 1){
      $('.passeul').remove();
    } else {
      $('.seul').remove();
    }

    index = selectpersonnages.groupe.final[1].indexOf('Terrence');
    if (index > -1) {
      selectpersonnages.groupe.final[1].splice(index, 1);
    }
    selectpersonnages.groupe.final[2].push('Terrence');


    mage1 = selectpersonnages.groupe.mission[1];
    mage2 = selectpersonnages.groupe.mission[2];
    mage3 = selectpersonnages.groupe.mission[3];
    nbm = 0;
    nomm = "";
    index = selectpersonnages.groupe.final[1].indexOf(mage2);
    if (index > -1) {
      nbm++;
      nomm = mage2;
    }
    index = selectpersonnages.groupe.final[1].indexOf(mage3);
    if (index > -1) {
      nbm++;
      nomm = mage3;
    }
    index = selectpersonnages.groupe.final[1].indexOf(mage1);
    if (index > -1) {
      nbm++;
      nomm = mage1;
    }
    if(nbm != 0){
      $('.quelquun').html(nomm);
    }

    saveGame();
  }
  if(selectedplayer.numero == 135 ){
    selectperso.Terrence.debloque4 = true;
    saveGame();
  }
  if(selectedplayer.numero == 136){
    selectperso.Terrence.debloque2 = true;
    selectperso.Modrak.debloque2 = true;
    saveGame();
  }
  if(selectedplayer.numero == 135 || selectedplayer.numero == 136 || selectedplayer.numero == 140 || selectedplayer.numero == 153 || selectedplayer.numero == 154){
    getGroupeMission();
    getMagesGroupe();
    if(selectedplayer.grp1){
      $('.sipasgrp1-1').remove();
    } else {
      $('.sigrp1-1').remove();
    }

    nomsall = "";
    mage2 = selectpersonnages.groupe.mission[2];
    mage3 = selectpersonnages.groupe.mission[3];
    nbm = 0;
    nomm = "";
    index = selectpersonnages.groupe.final[1].indexOf(mage2);
    if (index > -1) {
      nbm++;
      nomm = mage2;
      nomsall += mage2+" et ";
    }
    index = selectpersonnages.groupe.final[1].indexOf(mage3);
    if (index > -1) {
      nbm++;
      nomm = mage3;
      nomsall += mage3+" et ";
    }
    if(nbm == 0){
      $('.sigrpautre').remove();
    } else {
      $('.unautre').html(nomm);
    }
    nomsall = nomsall.substr(0, nomsall.length-4);
    $('.nomsall').html(nomsall);
  }
  if(selectedplayer.numero == 137){

    if(selectpersonnages.groupe.final[1].length <=0){
      $('.seul').remove();
    } else {
      $('.passeul').remove();
    }

    selectedplayer.fin1 = 1;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      // finchapitre2
      selectedplayer.chapitrenum = 'chapitre2';
      selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
      selectedplayer.numero = 1;
      saveGame();
      $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
        binding();
        $( "body, .num, main, html" ).scrollTop( 0 );
        $( window ).scrollTop( 0 );
        $( document ).scrollTop( 0 );
      });
    });

  }
  if(selectedplayer.numero == 138){

    getGroupeMission();
    getMagesGroupe();
    if(selectedplayer.grp1){
      $('.sipasgrp1-1').remove();
    } else {
      $('.sigrp1-1').remove();
    }
    nbcap = 0;
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventcapture').remove();
    } else {
      nbcap++;
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.magefeucapture').remove();
    } else {
      nbcap++;
    }

    if(nbcap == 2){
      $('.capture1').remove();
    } else if(nbcap == 1){
      $('.capture2').remove();
    } else {
      $('.capture1, .capture2').remove();
    }

    selectedplayer.fin1 = 2;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      // finchapitre2
      selectedplayer.chapitrenum = 'chapitre2';
      selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
      selectedplayer.numero = 1;
      saveGame();
      $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
        binding();
        $( "body, .num, main, html" ).scrollTop( 0 );
        $( window ).scrollTop( 0 );
        $( document ).scrollTop( 0 );
      });
    });

  }
  if(selectedplayer.numero == 139){
    getGroupeMission();
    getMagesGroupe();
    if(selectcreature.Noctule){

    } else {
      $('.noctule').remove();
    }
    if(selectpersonnages.groupe.final[1].length <= 0){
      $('.passeul').remove();
    } else {
      $('.seul').remove();
    }

    mage1 = selectpersonnages.groupe.mission[1];
    mage2 = selectpersonnages.groupe.mission[2];
    mage3 = selectpersonnages.groupe.mission[3];
    nbm = 0;
    nomm = "";
    index = selectpersonnages.groupe.final[1].indexOf(mage2);
    if (index > -1) {
      nbm++;
      nomm = mage2;
    }
    index = selectpersonnages.groupe.final[1].indexOf(mage3);
    if (index > -1) {
      nbm++;
      nomm = mage3;
    }
    index = selectpersonnages.groupe.final[1].indexOf(mage1);
    if (index > -1) {
      nbm++;
      nomm = mage1;
    }
    if(nbm != 0){
      $('.quelquun').html(nomm);
    }

    if(selectedplayer.trahison == 'vrai'){

    } else {
      $('.trahison').remove();
    }

    mage1 = selectpersonnages.groupe.mission[1];
    index2 = selectpersonnages.groupe.final[1].indexOf(mage1);
    if (index2 > -1) {
      $('.sipasgrp1-1').remove();
      selectedplayer.grp1 = true;
      selectpersonnages.groupe.final[1].splice(index2, 1);
      selectpersonnages.groupe.final[0].push(mage1);
    } else {
      $('.sigrp1-1').remove();
    }

    saveGame();
  }
  if(selectedplayer.numero == 141){
    getGroupeMission();
    getMagesGroupe();
    if(selectedplayer.grp1){
      $('.sipasgrp1-1').remove();
    } else {
      $('.sigrp1-1').remove();
    }

    nomsall = "";
    mage2 = selectpersonnages.groupe.mission[2];
    mage3 = selectpersonnages.groupe.mission[3];
    nbm = 0;
    nomm = "";
    index = selectpersonnages.groupe.final[1].indexOf(mage2);
    if (index > -1) {
      nbm++;
      nomm = mage2;
      nomsall += mage2+" et ";
    }
    index = selectpersonnages.groupe.final[1].indexOf(mage3);
    if (index > -1) {
      nbm++;
      nomm = mage3;
      nomsall += mage3+" et ";
    }
    if(nbm == 0){
      $('.sigrpautre').remove();
    } else {
      $('.unautre').html(nomm);
    }
    nomsall = nomsall.substr(0, nomsall.length-4);
    $('.nomsall').html(nomsall);

    if(selectedplayer.energie == "verte"){
      selectedplayer.capturemodrak = true;
    }

    saveGame();
  }
  if(selectedplayer.numero == 142){
    getGroupeMission();
    getMagesGroupe();
    if(selectedplayer.grp1){
      $('.sipasgrp1-1').remove();
    } else {
      $('.sigrp1-1').remove();
    }
    nbcap = 0;
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventcapture').remove();
    } else {
      nbcap++;
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.magefeucapture').remove();
    } else {
      nbcap++;
    }

    if(nbcap == 2){
      $('.capture1').remove();
    } else if(nbcap == 1){
      $('.capture2').remove();
    } else {
      $('.capture1, .capture2').remove();
    }

    selectedplayer.fin1 = 3;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      // finchapitre2
      selectedplayer.chapitrenum = 'chapitre2';
      selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
      selectedplayer.numero = 1;
      saveGame();
      $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
        binding();
        $( "body, .num, main, html" ).scrollTop( 0 );
        $( window ).scrollTop( 0 );
        $( document ).scrollTop( 0 );
      });
    });

  }
  if(selectedplayer.numero == 143){

    nbcap = 0;
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventcapture').remove();
    } else {
      nbcap++;
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.magefeucapture').remove();
    } else {
      nbcap++;
    }

    if(nbcap == 2){
      $('.capture1').remove();
    } else if(nbcap == 1){
      $('.capture2').remove();
    } else {
      $('.capture1, .capture2').remove();
    }

    selectedplayer.fin1 = 4;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      // finchapitre2
      selectedplayer.chapitrenum = 'chapitre2';
      selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
      selectedplayer.numero = 1;
      saveGame();
      $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
        binding();
        $( "body, .num, main, html" ).scrollTop( 0 );
        $( window ).scrollTop( 0 );
        $( document ).scrollTop( 0 );
      });
    });

  }
  if(selectedplayer.numero == 144){

    getGroupeMission();
    getMagesGroupe();

    var mageventexist = false;
    if(selectedplayer.objet.magevent != "capture"){
      $('.sipasmagevent').remove();
      mageventexist = true;
    } else {
      $('.simagevent').remove();
    }
    if(selectedplayer.magie == 'vent'){
      mageventexist = true;
    }

    if(mageventexist){
      $('.ch.infopasmagie').remove();
    } else {
      $('.ch.infomagie').remove();
    }

    //selectedplayer.fin1 = 4;
    saveGame();


  }
  if(selectedplayer.numero == 145){

    getGroupeMission();
    getMagesGroupe();

    if(selectedplayer.magie == 'vent' && selectedplayer.avantage == 'populaire'){
      $('.autre.ch').remove();
    } else {
      $('.infomagepop.ch').remove();
    }


  }
  if(selectedplayer.numero == 147 || selectedplayer.numero == 152){

    getGroupeMission();
    getMagesGroupe();

    nbcap = 0;
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventcapture').remove();
    } else {
      nbcap++;
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.magefeucapture').remove();
    } else {
      nbcap++;
    }

  }
  if(selectedplayer.numero == 148 || selectedplayer.numero == 149){

    getGroupeMission();
    getMagesGroupe();

  }

  if(selectedplayer.numero == 150){
    selectperso.Estrion.debloque3 = true;
    getGroupeMission();
    getMagesGroupe();
    if(selectcreature.Noctule){

    } else {
      $('.noctule').remove();
    }

  }
  if(selectedplayer.numero == 157){
    selectperso.Modrak.debloque2 = true;
  }
  if(selectedplayer.numero == 153){
    selectperso.Terrence.debloque4 = true;
    saveGame();
  }
  if(selectedplayer.numero == 154){
    selectperso.Terrence.debloque2 = true;
    saveGame();
  }
  if(selectedplayer.numero == 151){
    selectperso.Estrion.debloque3 = true;
    selectedplayer.fin1 = 5;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      // finchapitre2
      selectedplayer.chapitrenum = 'chapitre2';
      selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
      selectedplayer.numero = 1;
      saveGame();
      $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
        binding();
        $( "body, .num, main, html" ).scrollTop( 0 );
        $( window ).scrollTop( 0 );
        $( document ).scrollTop( 0 );
      });
    });

  }

  if(selectedplayer.numero == 155 || selectedplayer.numero == 156){
    getGroupeMission();
    getMagesGroupe();

    mage1 = selectpersonnages.groupe.mission[1];
    index2 = selectpersonnages.groupe.final[1].indexOf(mage1);
    if (index2 > -1) {
      $('.sipasgrp1-1').remove();
      selectedplayer.grp1 = true;
    } else {
      $('.sigrp1-1').remove();
      selectperso.Estrion.debloque4 = true;
    }

    mage2 = selectpersonnages.groupe.mission[2];
    mage3 = selectpersonnages.groupe.mission[3];
    nbm = 0;
    nomm = "";
    index = selectpersonnages.groupe.final[1].indexOf(mage2);
    if (index > -1) {
      nbm++;
      nomm = mage2;
    }
    index = selectpersonnages.groupe.final[1].indexOf(mage3);
    if (index > -1) {
      nbm++;
      nomm = mage3;
    }
    if(nbm == 0){
      $('.siautre').remove();
    } else {
      $('.unautre').html(nomm);
    }
    saveGame();
  }
  if(selectedplayer.numero == 159){
    getGroupeMission();
    getMagesGroupe();

    mage1 = selectpersonnages.groupe.mission[1];
    index2 = selectpersonnages.groupe.final[1].indexOf(mage1);
    if (index2 > -1) {
      $('.sipasgrp1-1').remove();
      selectedplayer.grp1 = true;
      selectedplayer.grp1disparait = true;
    } else {
      $('.sigrp1-1').remove();
    }
    nbcap = 0;
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventcapture').remove();
    } else {
      nbcap++;
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.magefeucapture').remove();
    } else {
      nbcap++;
    }
    if(nbcap == 0){
      $('.sicapture').remove();
    }

    saveGame();
  }
  if(selectedplayer.numero == 160){
    getGroupeMission();
    getMagesGroupe();

    mage1 = selectpersonnages.groupe.mission[1];
    index2 = selectpersonnages.groupe.final[1].indexOf(mage1);
    if (index2 > -1) {
      $('.sipasgrp1-1').remove();
      selectedplayer.grp1 = true;
    } else {
      $('.sigrp1-1').remove();
    }
    nbcap = 0;
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventcapture').remove();
    } else {
      nbcap++;
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.magefeucapture').remove();
    } else {
      nbcap++;
    }

    if(nbcap == 2){
      $('.capture1').remove();
    } else if(nbcap == 1){
      $('.capture2').remove();
    } else {
      $('.capture1, .capture2').remove();
    }

    saveGame();
  }
  if(selectedplayer.numero == 161){
    selectperso.Modrak.debloque4 = true;
    getGroupeMission();
    getMagesGroupe();
    if(selectedplayer.grp1disparait){

    } else {
      $('.sigrp1-1').remove();
    }
    selectedplayer.fin1 = 6;
    saveGame();

    $('.finchapitre').off().on('click', function(){
      // finchapitre2
      selectedplayer.chapitrenum = 'chapitre2';
      selectedplayer.chapitre = 'Chap.2 - La source de toute magie';
      selectedplayer.numero = 1;
      saveGame();
      $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
        binding();
        $( "body, .num, main, html" ).scrollTop( 0 );
        $( window ).scrollTop( 0 );
        $( document ).scrollTop( 0 );
      });
    });

  }
  if(selectedplayer.numero == 162){

    nbcap = 0;
    if(selectedplayer.objet.magevent != "capture"){
      $('.mageventcapture').remove();
    } else {
      nbcap++;
    }
    if(selectedplayer.objet.magefeu != "capture"){
      $('.magefeucapture').remove();
    } else {
      nbcap++;
    }

    if(nbcap == 0){
      $('.capture').remove();
    }

  }
  //** FIN NUMEROS ***/
} else if(selectedplayer.chapitrenum == 'chapitre2'){
  goActionChapter2();
} else if(selectedplayer.chapitrenum == 'chapitre3'){
  goActionChapter3();
}
  // FIN CHAPITRE 1

  if(selectedplayer.debloqueinfo != undefined && selectedplayer.debloqueinfo != null){
    if(selectedplayer.debloqueinfo == 'vrai'){
      $('footer .infos').show();
    }
  }
}
