var traduction;
var lang = 'fr';

function trad(lang) {
  $.getJSON( "lang/"+lang+".json", function( data ) {
    traduction = data;
    traduire();
  });
  $('body').addClass('lang-'+lang);
}

function traduire(){
  $('[data-trad]').each(function(){
    var temp_trad = $(this).data('trad');
    $(this).html(traduction[temp_trad]);
  });
}
