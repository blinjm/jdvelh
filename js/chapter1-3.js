var groupe_chap3 = [];
var vote = [];
var chefazano = 'humain';
var tourdetruite = 'non';
var persosmort = [];
var epilogue = [];
var quiquidonc;


function goActionChapter3(){
  getInfoChap2();
  if(selectedplayer.numero > 1){
    getInfoChap3();
  }
  getGroupeMission();
  getMagesGroupe();

  if(selectedplayer.numero == 1){
    var x = document.getElementById("myAudio");
     x.play();

    setTimeout(function(){
      $('.newchapter, .newchapter3').fadeOut(2000);
      $('.choix2, .resume').fadeIn(2000);
    }, 10000);
  }
  if(selectedplayer.fin1 > 0){
    $('.fin1').not('.fin1-'+selectedplayer.fin1).remove();
  }
  if(selectedplayer.fin2 > 0){
    $('.fin').not('.fin-'+selectedplayer.fin2).remove();
  }
  if(selectedplayer.numero == 249){
    selectedplayer.fin3 = 1;
    saveGame();
  }
  if(selectedplayer.numero == 258){
    selectedplayer.fin3 = 2;
    saveGame();
  }
  if(selectedplayer.numero == 288){
    selectedplayer.fin3 = 3;
    saveGame();
  }
  if(selectedplayer.numero == 312){
    selectedplayer.fin3 = 4;
    saveGame();
  }
  if(selectedplayer.numero == 315){
    selectedplayer.fin3 = 5;
    saveGame();
  }
  if(selectedplayer.numero == 335){
    selectedplayer.fin3 = 6;
    saveGame();
  }
  if(selectedplayer.numero == 338){
    selectedplayer.fin3 = 7;
    saveGame();
  }
  if(selectedplayer.numero == 345){
    selectedplayer.fin3 = 8;
    saveGame();
  }
  if(selectedplayer.numero == 360){
    selectedplayer.fin3 = 9;
    saveGame();
  }
  if(selectedplayer.numero == 362){
    selectedplayer.fin3 = 10;
    saveGame();
  }
  if(selectedplayer.numero == 364){
    selectedplayer.fin3 = 11;
    saveGame();
  }
  if(selectedplayer.numero == 366){
    selectedplayer.fin3 = 12;
    saveGame();
  }
  if(selectedplayer.numero == 371){
    selectedplayer.fin3 = 13;
    saveGame();
  }
  if(selectedplayer.numero == 376){
    selectedplayer.fin3 = 14;
    saveGame();
  }
  if(selectedplayer.numero == 378){
    selectedplayer.fin3 = 15;
    saveGame();
  }
  if(selectedplayer.numero == 388){
    selectedplayer.fin3 = 16;
    saveGame();
  }
  if(selectedplayer.numero == 390){
    selectedplayer.fin3 = 17;
    saveGame();
  }
  if(selectedplayer.numero == 392){
    selectedplayer.fin3 = 18;
    saveGame();
  }
  if(selectedplayer.numero == 395){
    selectedplayer.fin3 = 19;
    saveGame();
  }
  if(selectedplayer.numero == 397){
    selectedplayer.fin3 = 20;
    saveGame();
  }
  if(selectedplayer.numero == 400){
    selectedplayer.fin3 = 21;
    saveGame();
  }
  if(selectedplayer.numero == 402){
    selectedplayer.fin3 = 22;
    saveGame();
  }
  if(selectedplayer.numero == 409){
    selectedplayer.fin3 = 23;
    saveGame();
  }
  if(selectedplayer.numero == 421){
    selectedplayer.fin3 = 24;
    saveGame();
  }
  if(selectedplayer.numero == 430){
    selectedplayer.fin3 = 25;
    saveGame();
  }
  if(selectedplayer.numero == 456){
    selectedplayer.fin3 = 26;
    saveGame();
  }
  if(selectedplayer.numero == 469){
    selectedplayer.fin3 = 27;
    saveGame();
  }
  if(selectedplayer.numero == 478){
    selectedplayer.fin3 = 28;
    saveGame();
  }
  if(selectedplayer.numero == 15 || selectedplayer.numero == 59 || selectedplayer.numero == 65 || selectedplayer.numero == 92 || selectedplayer.numero == 109 || selectedplayer.numero == 110 || selectedplayer.numero == 120 || selectedplayer.numero == 135 || selectedplayer.numero == 143 || selectedplayer.numero == 152 || selectedplayer.numero == 177 || selectedplayer.numero == 190 || selectedplayer.numero == 199 || selectedplayer.numero == 200 || selectedplayer.numero == 202){
    $('.redochap3').on('click', function(){
      resetgame3();
    });
  }

  var listemort = persosmort.join('-');
  if(listemort.search('Méonie') > -1){
    $('.méonie-morte').show();
    $('.méonie-vivante').hide();
  } else {
    $('.méonie-morte').hide();
    $('.méonie-vivante').show();
  }
  if(listemort.search('Mullihane') > -1){
    $('.mullihane-morte').show();
    $('.mullihane-vivante').hide();
  } else {
    $('.mullihane-morte').hide();
    $('.mullihane-vivante').show();
  }
  if(listemort.search('Kannan') > -1){
    $('.kannan-morte').show();
    $('.kannan-vivante').hide();
  } else {
    $('.kannan-morte').hide();
    $('.kannan-vivante').show();
  }
  if(listemort.search('Espéride') > -1){
    $('.espéride-morte').show();
    $('.espéride-vivante').hide();
  } else {
    $('.espéride-morte').hide();
    $('.espéride-vivante').show();
  }
  if(listemort.search('Haziko') > -1){
    $('.haziko-morte').show();
    $('.haziko-vivante').hide();
  } else {
    $('.haziko-morte').hide();
    $('.haziko-vivante').show();
  }
  if(listemort.search('Mac Lumy') > -1){
    $('.mac-mort').show();
    $('.mac-vivant').hide();
  } else {
    $('.mac-mort').hide();
    $('.mac-vivant').show();
  }
  if(listemort.search('Haziko') > -1){
    $('.haziko-morte').show();
    $('.haziko-vivante').hide();
  } else {
    $('.haziko-morte').hide();
    $('.haziko-vivante').show();
  }
  if(listemort.search('Panacle') > -1){
    $('.panacle-mort').show();
    $('.panacle-vivant').hide();
  } else {
    $('.panacle-mort').hide();
    $('.panacle-vivant').show();
  }
  if(listemort.search('Anselm') > -1){
    $('.anselm-mort').show();
    $('.anselm-vivant').hide();
  } else {
    $('.anselm-mort').hide();
    $('.anselm-vivant').show();
  }
  if(listemort.search('Lasseni') > -1){
    $('.lasseni-morte').show();
    $('.lasseni-vivante').hide();
  } else {
    $('.lasseni-morte').hide();
    $('.lasseni-vivante').show();
  }

  if(selectedplayer.grp1) {
    quiquidonc = selectpersonnages.groupe.mission[1];
    if(quiquidonc != "" && quiquidonc != undefined) {

    } else {
      quiquidonc = "Kannan";
    }
    $('.partiavecusaki').html('quiquidonc');
  }

  if(rem){
    $('.naorem').html('Rem');
  } else {
    $('.naorem').html('Nao');
  }

  if(selectedplayer.numero == 1){
    insertNew(selectperso, 'Méonie');
    insertNew(selectperso, 'Arak');
    saveGame();
  }
  if(selectedplayer.numero == 24){
    tourdetruite = 'oui';
  }

  if(selectedplayer.numero == 52){
    // seulement Académie et Lac peuvent voter
    $('.vote1-1').on('click', function(){
      vote[0] = '1';
    });
    // tout le monde peut voter
    $('.vote1-2').on('click', function(){
      vote[0] = '2';
    });
  }
  if(selectedplayer.numero == 53){
    // no limites
    $('.vote2-1').on('click', function(){
      vote[1] = '1';
    });
    // pas de mages renégats
    $('.vote2-2').on('click', function(){
      vote[1] = '2';
    });
    // pas de juniors
    $('.vote3-2').on('click', function(){
      vote[1] = '3';
    });
  }
  if(selectedplayer.numero == 54){
    // 4 mages
    $('.vote3-1').on('click', function(){
      vote[2] = '1';
    });
    // 6mages
    $('.vote3-2').on('click', function(){
      vote[2] = '2';
    });
  }
  if(selectedplayer.numero == 55){
    if(parseInt(vote[2]) == 1){
      $('.vote3-2').remove();
    } else {
      $('.vote3-1').remove();
    }
  }
  if(selectedplayer.numero == 72){
    chefazano = 'mixte';
    epilogue.push('Village commun');
  }
  if(selectedplayer.numero == 94){
    persosmort.push('Haziko');
    persosmort.push('Panacle');
    persosmort.push('Anselm');
    persosmort.push('MacLumy');
    persosmort.push('Kannan');
    persosmort.push('Espéride');
    epilogue.push('La catastrophe');
  }
  if(selectedplayer.numero == 96){
    epilogue.push('Guerre des villages');
  }
  if(selectedplayer.numero == 97){
    epilogue.push('Village blessé');
  }
  if(selectedplayer.numero == 98){
    epilogue.push('Village dans la forteresse');
  }
  if(selectedplayer.numero == 108){
    persosmort.push('Espéride2');
    epilogue.push('Espéride morte');
  }
  if(selectedplayer.numero == 113){
    if(quiquidonc.toLowerCase() == "haziko"){
      $('.haziko').hide();
    }
    if(quiquidonc.toLowerCase() == "panacle"){
      $('.panacle').hide();
    }
    if(quiquidonc.toLowerCase() == "anselm"){
      $('.anselm').hide();
    }
  }
  if(selectedplayer.numero == 128){
    epilogue.push('Déception légendaire');
  }
  if(selectedplayer.numero == 131){
    epilogue.push('Déception légendaire');
  }
  if(selectedplayer.numero == 144){
    persosmort.push('Mullihane');
    persosmort.push('Kannan2');
  }
  if(selectedplayer.numero == 145){
    persosmort.push('Méonie');
  }
  if(selectedplayer.numero == 166){
    persosmort.push('MacLumy2');
    persosmort.push('Kannan3');
    persosmort.push('Panacle2');
    persosmort.push('Mullihane2');
  }
  if(selectedplayer.numero == 168){
    epilogue.push('Mort triste de Méonie');
    persosmort.push('Méonie2');
  }
  if(selectedplayer.numero == 176){
    persosmort.push('Mullihane3');
    persosmort.push('MacLumy3');
    persosmort.push('Kannan4');
    persosmort.push('Panacle3');
    persosmort.push('Haziko2');
    persosmort.push('Anselm2');
    persosmort.push('Espéride3');
  }
  if(selectedplayer.numero == 183){
    epilogue.push('Usaki bras en moins');
  }
  if(selectedplayer.numero == 198){
    epilogue.push('Ymir bras en moins');
  }
  if(selectedplayer.numero == 203){
    persosmort.push('MacLumy4');
  }
  if(selectedplayer.numero == 205){
    epilogue.push('Lasseni et Fandrean résistance');
  }
  if(selectedplayer.numero == 208){
    persosmort.push('Mullihane4');
    persosmort.push('Méonie3');
    epilogue.push('Kannan aveugle');
  }
  if(selectedplayer.numero == 209){
    epilogue.push('Méonie jambes coupées');
    epilogue.push('Kannan aveugle');
  }
  if(selectedplayer.numero == 210){
    epilogue.push('Méonie jambes coupées');
    epilogue.push('Kannan aveugle');
  }
  if(selectedplayer.numero == 219){
    epilogue.push('Usaki bras en moins');
  }
  if(selectedplayer.numero == 232){
    epilogue.push('Méonie sauvée par ton pouvoir');
  }
  if(selectedplayer.numero == 233){
    epilogue.push('Méonie morte');
  }
  if(selectedplayer.numero == 236){
    persosmort.push('Soyos2');
    persosmort.push('Andaelor');
    persosmort.push('Pasha');
  }
  if(selectedplayer.numero == 237){
    persosmort.push('Soyos2');
    persosmort.push('Andaelor');
    persosmort.push('Pasha');
  }
  if(selectedplayer.numero == 239){
    epilogue.push('Tour détruite');
  }
  if(selectedplayer.numero == 242){
    persosmort.push('MacLumy5');
  }
  if(selectedplayer.numero == 244){
    persosmort.push('Panacle4');
  }
  if(selectedplayer.numero == 246){
    persosmort.push('Espéride4');
    persosmort.push('Panacle4');
    persosmort.push('Anselm3');
    persosmort.push('Kannan5');
  }
  if(selectedplayer.numero == 250){
    persosmort.push('Espéride5');
    persosmort.push('Panacle5');
    persosmort.push('Anselm4');
    persosmort.push('Kannan6');
  }
  if(selectedplayer.numero == 251){
    persosmort.push('Espéride6');
  }
  if(selectedplayer.numero == 263){
    persosmort.push('Pasha2');
    persosmort.push('Soyos3');
    persosmort.push('Andaelor2');
  }
  if(selectedplayer.numero == 266){
    persosmort.push('Pasha3');
    persosmort.push('Andaelor3');
  }
  if(selectedplayer.numero == 272){
    persosmort.push('Pasha4');
  }
  if(selectedplayer.numero == 274){
    persosmort.push('Pasha2');
    persosmort.push('Soyos3');
  }
  if(selectedplayer.numero == 276){
    epilogue.push('Tu es aveugle');
  }
  if(selectedplayer.numero == 277){
    persosmort.push('Méonie');
    if(rem){
      persosmort.push('Rem');
    } else {
      persosmort.push('Nao');
    }
  }
  if(selectedplayer.numero == 280){
    //persosmort.push('Pasha');
  }
  if(selectedplayer.numero == 283){
    persosmort.push('Espéride7');
  }
  if(selectedplayer.numero == 285){
    persosmort.push('Usaki');
    persosmort.push('Ymir');
    persosmort.push('Zaïra');
  }
  if(selectedplayer.numero == 293){
    persosmort.push('Zaïra2');
  }
  if(selectedplayer.numero == 297){
    persosmort.push('Pasha5');
  }
  if(selectedplayer.numero == 311){
    persosmort.push('Ymir2');
    persosmort.push('Zaïra2');
  }
  if(selectedplayer.numero == 317){
    persosmort.push('Soyos3');
    persosmort.push('Andaelor2');
  }
  if(selectedplayer.numero == 321){
    persosmort.push('Pasha');
  }
  if(selectedplayer.numero == 322){
    persosmort.push('Pasha6');
  }
  if(selectedplayer.numero == 324){
    persosmort.push('Pasha');
    persosmort.push('Usaki2');
  }
  if(selectedplayer.numero == 325){
    persosmort.push('Usaki3');
  }
  if(selectedplayer.numero == 331){
    persosmort.push('Méonie5');
  }
  if(selectedplayer.numero == 334){
    persosmort.push('Usaki4');
  }
  if(selectedplayer.numero == 340){
    persosmort.push('Méonie5');
  }
  if(selectedplayer.numero == 348){
    persosmort.push('MacLumy6');
  }
  if(selectedplayer.numero == 391){
    persosmort.push('Panacle6');
    persosmort.push('Haziko3');
    persosmort.push('Kannan7');
    persosmort.push('Anselm5');
    persosmort.push('Espéride8');
    persosmort.push('Usaki5');
    persosmort.push('Méonie6');
    if(rem){
      persosmort.push('Rem2');
    } else {
      persosmort.push('Nao2');
    }
  }
  if(selectedplayer.numero == 392){
    persosmort.push('Lasseni');
    persosmort.push('Fandrean');
  }
  if(selectedplayer.numero == 393){
    persosmort.push('Esteban');
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 396){
    persosmort.push('Usaki');
  }
  if(selectedplayer.numero == 398){
    if(rem){
      persosmort.push('Rem3');
    } else {
      persosmort.push('Nao3');
    }
  }
  if(selectedplayer.numero == 402){
    persosmort.push('Méonie7');
  }
  if(selectedplayer.numero == 404){
    if(rem){
      persosmort.push('Rem2');
    } else {
      persosmort.push('Nao2');
    }
  }
  if(selectedplayer.numero == 410){
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 411){
    persosmort.push('Usaki6');
  }
  if(selectedplayer.numero == 412){
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 417){
    if(rem){
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }
  if(selectedplayer.numero == 420){
    persosmort.push('Ymir2');
    persosmort.push('Usaki7');
    if(rem){
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }
  if(selectedplayer.numero == 420){
    persosmort.push('Ymir2');
    persosmort.push('Zaïra2');
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 429){
    if(rem){
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }
  if(selectedplayer.numero == 433){
    persosmort.push('Ymir2');
    persosmort.push('Zaïra2');
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 436){
    if(rem){
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }
  if(selectedplayer.numero == 438){
    persosmort.push('Pasha7');
  }
  if(selectedplayer.numero == 443){
    if(rem){
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }
  if(selectedplayer.numero == 446){
    persosmort.push('Ymir2');
    persosmort.push('Usaki7');
    if(rem){
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
  }
  if(selectedplayer.numero == 452){
    persosmort.push('Andaelor2');
    persosmort.push('Soyos3');
  }
  if(selectedplayer.numero == 455){
    persosmort.push('Usaki7');
  }
  if(selectedplayer.numero == 456){
    if(rem){
      persosmort.push('Rem4');
    } else {
      persosmort.push('Nao4');
    }
    persosmort.push('Usaki7');
  }
  if(selectedplayer.numero == 461){
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 465){
    if(rem){
      persosmort.push('Rem5');
    } else {
      persosmort.push('Nao5');
    }
    persosmort.push('Usaki8');
  }
  if(selectedplayer.numero == 467){
    persosmort.push('Usaki9');
  }
  if(selectedplayer.numero == 470){
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 471){
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 473){
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 475){
    persosmort.push('Méonie6');
  }
  if(selectedplayer.numero == 477){
    if(rem){
      persosmort.push('Rem5');
    } else {
      persosmort.push('Nao5');
    }
    persosmort.push('Usaki7');
  }
  if(selectedplayer.numero == 479){
    persosmort.push('Méonie6');
    persosmort.push('Haziko3');
  }

  saveInfoChap3();
}

function getInfoChap3(){
  chefazano = JSON.parse(selectedplayer.chefazano);
  vote = JSON.parse(selectedplayer.vote);
  tourdetruite = JSON.parse(selectedplayer.tourdetruite);
  persosmort = JSON.parse(selectedplayer.persosmort);
  epilogue = JSON.parse(selectedplayer.epilogue);
}
function saveInfoChap3(){
  selectedplayer.tourdetruite = JSON.stringify(tourdetruite);
  selectedplayer.vote = JSON.stringify(vote);
  selectedplayer.chefazano = JSON.stringify(chefazano);
  selectedplayer.persosmort = JSON.stringify(persosmort);
  selectedplayer.epilogue = JSON.stringify(epilogue);
  saveGame();
}

function resetgame3(){

}
