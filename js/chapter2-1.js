var heros = 0; // 1 = Armin - 2 = Yasha
var pouvoir = 0; // 1 = CROTALE / CRAPAUD - 2 = FENNEC / LIEVRE

function goActionLivre2_1(){

  // AFFICHAGE EN FONCTION DU HEROS
  if(heros == 1){
    $('.h-1').show();
    $('.h-2').remove();
  } else {
    $('.h-1').remove();
    $('.h-2').show();
  }
  // AFFICHAGE EN FONCTION DU POUVOIR
  if(pouvoir == 1){
    $('.p-1').show();
    $('.p-2').remove();
  } else {
    $('.p-1').remove();
    $('.p-2').show();
  }

  // CHOIX DU HEROS
  if(selectedplayer.numero == 1){
    $('.ch').on('click', function(){
      if($(this).hasClass('heros-1')){
        heros = 1;
        selectedplayer.heros = 1;
      } else {
        heros = 2;
        selectedplayer.heros = 2;
      }
    });
    saveGame();
  }
  // CHOIX DU POUVOIR
  if(selectedplayer.numero == 2){
    $('.ch').on('click', function(){
      if($(this).hasClass('pouvoir-1')){
        pouvoir = 1;
        selectedplayer.pouvoir = 1;
      } else {
        pouvoir = 2;
        selectedplayer.pouvoir = 2;
      }
    });
    saveGame();
  }
  if(selectedplayer.numero == 7){
    selectedplayer.depart = 1; // Insulte Marez
    saveGame();
  }
  if(selectedplayer.numero == 9){
    selectedplayer.depart = 2; // cliente raciste
    saveGame();
  }

}


function getInfoChap2_1(){
  heros = selectedplayer.heros;
  pouvoir = selectedplayer.pouvoir;
  depart = selectedplayer.depart;
}
function saveInfoChap2_1(){
  selectedplayer.depart = depart;
  selectedplayer.heros = heros;
  selectedplayer.pouvoir = pouvoir;
  saveGame();
}
