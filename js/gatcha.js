function initNew(){

    trad(lang);


    $('.livres').on('click', function(){
      if(!$(this).hasClass('locked')){
        $('.livres').not($(this)).hide();
        $('.chapitre[data-livre="'+$(this).attr('data-livre')+'"]').show();

        // console.log(selectedplayer);
        if(selectedplayer != null){
          if(selectedplayer.livrenum == "") {
            $('.continue, .retour').hide();
            $('.new').show();
          } else if($(this).attr('data-livre') == selectedplayer.livrenum){
            $('.new, .retour').hide();
            $('.continue').show();
          } else {
            $('.new, .continue').hide();
            $('.retour').show();
          }
          // if((selectedplayer.livrenum) == ($(this).attr('data-livre'))){
            if((selectedplayer.chapitrenum) == "chapitre1"){
              $('.titre__chapitre').removeClass('selected done');
              $('.chapitre-1 [data-chap="chapitre1"]').addClass('selected');
            }
            if((selectedplayer.chapitrenum) == "chapitre2"){
              $('.titre__chapitre').removeClass('selected done');
              $('.chapitre-1 [data-chap="chapitre1"]').addClass('done');
              $('.chapitre-1 [data-chap="chapitre2"]').addClass('selected');
            }
            if((selectedplayer.chapitrenum) == "chapitre3"){
              $('.titre__chapitre').removeClass('selected done');
              $('.chapitre-1 [data-chap="chapitre1"]').addClass('done');
              $('.chapitre-1 [data-chap="chapitre2"]').addClass('done');
              $('.chapitre-1 [data-chap="chapitre3"]').addClass('selected');
            }
            if((selectedplayer.chapitrenum) == "chapitre21"){
              $('.titre__chapitre').removeClass('selected done');
              $('.chapitre-1 [data-chap="chapitre1"]').addClass('done');
              $('.chapitre-1 [data-chap="chapitre2"]').addClass('done');
              $('.chapitre-1 [data-chap="chapitre3"]').addClass('done');
              $('.chapitre-2 [data-chap="chapitre1"]').addClass('selected');
            }
          // }
        }
      }
    });

    $('.continue').on('click', function(){
        $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
          binding();
        });
    });
    $('.retour').on('click', function(){
        $('.livres').show();
        $('.chapitre').hide();
    });

    $('.new').on('click', function(){
      selectedplayer.livrenum = $(this).parent().attr('data-livre');
      selectedplayer.livre = $(this).parent().parent().find('.livres .titre').html();
      selectedplayer.chapitrenum = $(this).parent().find('.titre__chapitre.selected').attr('data-chap');
      selectedplayer.chapitre = $(this).parent().find('.titre__chapitre.selected').html();
      selectedplayer.numero = 1;
      saveGame();

      $( "main" ).load( "template/"+selectedplayer.livrenum+"/"+selectedplayer.chapitrenum+"/"+selectedplayer.numero+".html", function() {
        binding();
      });
    });

    if(selectedplayer == null){

      if(joueur_saved!= null && joueur_saved.joueur1 != null){
        $('.nom').html(joueur_saved.joueur1.nom);
        selectedplayer = joueur_saved.joueur1;

        if(selectedplayer.chapitrenum == 'chapitre2'){
          $('.livres[data-livre="livre1"]').addClass('chapitre2');
          $('.titre__chapitre').removeClass('selected');
          $('.titre__chapitre[data-chap="chapitre2"]').addClass('selected');
          for(var keys in joueur_saved){
            if(selectedplayer.nom == joueur_saved[keys].nom){
              selectperso       = persos[keys];
              selectlieux       = lieux[keys];
              selectcreature    = creatures[keys];
              selectpersonnages = personnages[keys];
              selectsorts       = sorts[keys];
            }
          }
        }
      } else {
        $('.nom').html('Créer un personnage d\'abord');
        $('.new').hide();
      }

    } else {
      $('.nom').html(selectedplayer.nom);

      if(selectedplayer.chapitrenum == 'chapitre2'){
        $('.livres[data-livre="livre1"]').addClass('chapitre2');
        $('.titre__chapitre').removeClass('selected');
        $('.titre__chapitre[data-chap="chapitre2"]').addClass('selected');
        for(var keyss in joueur_saved){
          if(selectedplayer.nom == joueur_saved[keyss].nom){
            selectperso       = persos[keyss];
            selectlieux       = lieux[keyss];
            selectcreature    = creatures[keyss];
            selectpersonnages = personnages[keyss];
            selectsorts       = sorts[keyss];
          }
        }
      }
    }

    if(selectedplayer != null){
      if(parseInt(selectedplayer.fin3) > 0 && selectedplayer.livrenum == "livre1"){
        selectedplayer.livrenum = "livre2";
        selectedplayer.chapitrenum = "chapitre21";
        selectedplayer.livre = "Le monde dévasté";
        selectedplayer.chapitre = "Chap.1 - Soleil brûlant";
        selectedplayer.numero = 1;
        saveGame();
      }
      if(selectedplayer.livrenum == "livre2"){
        $('.livres[data-livre="livre2"]').removeClass('locked');
      }
    }

}
